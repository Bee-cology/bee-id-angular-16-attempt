import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { LocationService } from '../../services/location.service';
import { Marker } from '../../models/marker.model';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss']
})
export class GoogleMapComponent implements OnInit, OnChanges {
  @Input() initLat: number;
  @Input() initLng: number;
  lat: number;
  lng: number;
  @Input() selectedId;
  private _selectedId;
  @Input() zoom = 15;
  markers: Array<Marker>;

  @Output() markerClicked = new EventEmitter<number>();
  @Output() markerAdded = new EventEmitter<Marker>();

  constructor(private locationService: LocationService) { }

  ngOnInit() {
    this.lat = this.initLat;
    this.lng = this.initLng;
    this.markers = this.locationService.markers;
    this.selectedId = this.markers[0];
  }

  ngOnChanges(changes: SimpleChanges) {
    this.markers = this.locationService.markers;
    const marker = this.markers[changes.selectedId.currentValue];
    if (marker != null) { // if change is not caused by adding a new marker
      this._selectedId = changes.selectedId.currentValue;
      this.lat = this.markers[this._selectedId].lat;
      this.lng = this.markers[this._selectedId].lng;
      this.highlightMarker(this._selectedId);
    }
  }

  highlightMarker(id) {
    this.markers.forEach(marker => {
      if (marker.id === id) {
        marker.opacity = 1;
      } else {
        marker.opacity = 0.5;
      }
    });
  }

  clickedMarker(index) {
    this.markerClicked.emit(index);
  }

  mapClicked(event: any) {
    console.log(event);
    this.locationService.addMarker(event.coords.lat, event.coords.lng)
      .then(markers => {
        this.markers = markers;
        this.markerAdded.emit(this.markers[this.markers.length - 1]);
      })
      .catch(e => {
        console.log(e);
      });
  }

  markerDragEnd(m: Marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
}
