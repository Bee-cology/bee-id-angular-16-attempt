import {
  Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';
import { NguCarousel, NguCarouselConfig, NguCarouselStore } from '@ngu/carousel';
// import { NguCarouselService } from '@ngu/carousel/src/ngu-carousel.service';
// import { NguCarouselStore } from '@ngu/carousel/src/ngu-carousel/ngu-carousel.interface';

declare var Flickity: any;

@Component({
  selector: 'app-flickity',
  templateUrl: './app-flickity.component.html',
  styleUrls: ['./app-flickity.component.scss']
})
export class AppFlickityComponent implements OnInit, OnChanges {
  // @ViewChild('carousel', { read: ElementRef }) carousel: ElementRef;
  @Input() name;
  @Input() source;
  @Input() base = null;
  @Input() abdomen = null;
  @Input() thorus = null;
  @Input() head = null;
  @Input() data;
  @Input() imageSrc;
  @Output() selected = new EventEmitter();
  selectedIndex = -1;
  length = 0;
  selectionText = null;
  public carouselOne: NguCarouselConfig;
  private carouselToken: string;


  constructor(private carousel: NguCarousel<any>) { }

  ngOnInit() {
    if (this.name === 'Base') {
      this.selectionText = ['MOSTLY BLACK', 'HALF & HALF', 'MOSTLY NON-BLACK'];
      this.selectedIndex = this.base;
    } else {
      this.selectedIndex = 0;
      this.selected.emit({ name: this.name, selectedId: this.selectedIndex, init: true });
    }
    this.carouselOne = {
      grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
      slide: 1,
      speed: 400,
      point: {
        visible: false
      },
      load: 2,
      touch: false,
      loop: true,
      custom: 'banner'
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data && changes.data.currentValue) {
      this.data = changes.data.currentValue.map(img => `assets/offline-assets/bee-cartoons/${this.source}/${img}.png`);
      this.length = this.data.length;
      this.resetFn();
    }
  }

  initDataFn(key) {
    this.carouselToken = key.token;
  }

  resetFn() {
	// ERROR
    // this.carousel.reset(this.carouselToken);
    this.carousel.reset(true);
  }

  onmoveFn($event) {
    this.selectedIndex = $event.currentSlide;
    this.selected.emit({ name: this.name, selectedId: $event.currentSlide, init: false });
  }

  select(index) {
    this.selectedIndex = index;
    this.selected.emit({name: this.name, selectedId: index, init: false})
  }
}
