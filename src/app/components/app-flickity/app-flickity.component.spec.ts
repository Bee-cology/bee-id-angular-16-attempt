import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppFlickityComponent } from './app-flickity.component';

describe('AppFlickityComponent', () => {
  let component: AppFlickityComponent;
  let fixture: ComponentFixture<AppFlickityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppFlickityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppFlickityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
