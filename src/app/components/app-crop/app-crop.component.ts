import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
  selector: 'app-app-crop',
  templateUrl: './app-crop.component.html',
  styleUrls: ['./app-crop.component.scss']
})
export class AppCropComponent implements OnInit {
  @Input() inputEvent;
  @Input() inputBase64;
  @Input() coordinates;
  @Output() croppedImageEmitter = new EventEmitter();
  @Output() backEmitter = new EventEmitter();
  @ViewChild('cropper', {static: false}) cropper: AppCropComponent;

  imageChangedEvent;
  image;
  initCoordinates = null;

  constructor() { }

  ngOnInit() {
    if (this.inputEvent != null) this.fileChangeEvent(this.inputEvent);
  }

  back() {
    this.backEmitter.emit();
  }

  crop() {
    this.croppedImageEmitter.emit({'image': this.image, 'coordinates': this.coordinates});
  }

  fileChangeEvent(event): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event) {
    if (this.initCoordinates == null) {
      this.initCoordinates = JSON.parse(JSON.stringify(event.cropperPosition));
    }
    if (JSON.stringify(this.initCoordinates) === JSON.stringify(event.cropperPosition) && this.coordinates != null) {
      this.cropper.cropper = this.coordinates;
      return;
    }
    this.coordinates = event.cropperPosition;
    this.image = event.base64;
  }

  cropperReady() {
    if (this.coordinates != null) {
      this.cropper.cropper = this.coordinates;
      this.cropper.crop();
    }
  }

  imageLoaded() {
  }
}
