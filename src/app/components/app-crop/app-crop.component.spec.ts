import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCropComponent } from './app-crop.component';

describe('AppCropComponent', () => {
  let component: AppCropComponent;
  let fixture: ComponentFixture<AppCropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppCropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
