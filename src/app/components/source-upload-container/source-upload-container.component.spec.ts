import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceUploadContainerComponent } from './source-upload-container.component';

describe('SourceUploadContainerComponent', () => {
  let component: SourceUploadContainerComponent;
  let fixture: ComponentFixture<SourceUploadContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceUploadContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceUploadContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
