import { AfterViewInit, Component, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import fixOrientation from 'fix-orientation';
import {DialogButterflyUploadComponent, DialogVideoPreferenceComponent} from '../../pages/app-home/app-home.component';
import { MatDialog } from '@angular/material/dialog';
import { SnackBarInputBasic } from '../../models/app-snackbars';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-source-upload-container',
  templateUrl: './source-upload-container.component.html',
  styleUrls: ['./source-upload-container.component.scss']
})
export class SourceUploadContainerComponent implements AfterViewInit {
  @Output() inputVideoEmitter = new EventEmitter<any>();
  @Output() inputImageEmitter = new EventEmitter<any>();
  @Input() sourceType: string;
  @Input() specimenType: string;
  maxVideoSize = 40; // MB

  constructor(private element: ElementRef,
              private focusMonitor: FocusMonitor,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngAfterViewInit() {
    if (this.sourceType === 'image') {
      this.focusMonitor.stopMonitoring(document.getElementById('button')); // disable grey highlighting after warning click
    }
  }

  imageUploadEvent(event) {
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.readAsDataURL(file); // read image to base64
    reader.onload = () => {
      fixOrientation(reader.result, {image: true}, fixed => {
        this.inputImageEmitter.emit({'file': file, 'base64': fixed, 'specimen': this.specimenType});
      });
    };
  }

  imageWarningEvent() {
    const dialogRef = this.dialog.open(DialogVideoPreferenceComponent, { autoFocus: false });
    document.body.classList.add('dialog-open');
    dialogRef.afterClosed().subscribe(event => {
      document.body.classList.remove('dialog-open');
      if (event) {
        this.imageUploadEvent(event);
      }
    });
  }

  /*
  butterflyUploadEvent() {
    const dialogRef = this.dialog.open(DialogButterflyUploadComponent, { autoFocus: false });
    document.body.classList.add('dialog-open');
    dialogRef.afterClosed().subscribe(event => {
      document.body.classList.remove('dialog-open');
      if (event) {
        this.videoUploadEvent(event);
      }
    });
    this.videoUploadEvent(event);
  }
   */

  videoUploadEvent(event) {
    const video = event.target.files[0];
    if (this.specimenType === 'butterfly' && video.size > this.maxVideoSize * 1024 * 1024) {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: { 'message': 'Your video is larger than the limit (' + this.maxVideoSize + ' MB)', 'icon': 'error' },
        panelClass: ['sb-error']
      });
    } else {
      this.inputVideoEmitter.emit({'file': video, 'specimen': this.specimenType});
    }
  }
}
