import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';
import { FileUploadModule } from 'ng2-file-upload';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { environment } from '../environments/environment';
import { NavComponent } from './components/nav/nav.component';
import { MaterialModule } from './material.module';
// import { GestureConfig } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppPageComponent } from './pages/app-page/app-page.component';

import {
  AppHomeComponent,
  DialogBeeBehaviorDialogComponent,
  DialogButterflyBehaviorDialogComponent,
  DialogGuidedBeeIdentificationComponent,
  DialogGuidedFlowerIdentificationComponent,
  DialogGuidedButterflyIdentificationComponent,
  DialogVideoPreferenceComponent,
  DialogVideoUploadComponent,
  DialogTailsGuideComponent,
  DialogAntennaeGuideComponent,
  DialogPatternGuideComponent,
  DialogShapeGuideComponent,
  DialogVideoTooLargeComponent,
  DialogButterflyUploadComponent
} from './pages/app-home/app-home.component';
import { SnackBarInputBasic } from './models/app-snackbars';
import { AppLogSummaryComponent } from './pages/app-log-summary/app-log-summary.component';
import { AppLogsComponent } from './pages/app-logs/app-logs.component';
import { AppBeedexComponent } from './pages/app-beedex/app-beedex.component';
import { AppTutorialComponent } from './pages/app-tutorial/app-tutorial.component';
import { FrameCaptureService } from './services/frame-capture.service';
import { BeelogService } from './services/beelog.service';
import { LocationService } from './services/location.service';
import { HttpClientModule } from '@angular/common/http';
import { AppBeeDetailComponent } from './pages/app-bee-detail/app-bee-detail.component';
import { AppButterflyDetailComponent } from './pages/app-butterfly-detail/app-butterfly-detail.component';
import { AgmCoreModule } from '@agm/core';
import { GoogleMapComponent } from './components/google-map/google-map.component';
import { AppCaptureComponent } from './pages/app-capture/app-capture.component';
import { AppCropComponent } from './components/app-crop/app-crop.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppFlickityComponent } from './components/app-flickity/app-flickity.component';
import { CookieService } from 'ngx-cookie-service';
import { NguCarouselModule } from '@ngu/carousel';
import { ImageCropperModule } from 'ngx-image-cropper';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { CreateAccountComponent } from './pages/create-account/create-account.component';
import { BeecologyAuthService } from './services/authentication/beecology-auth.service';
import { DatabaseService } from './services/database.service';
import { FooterComponent } from './components/footer/footer.component';
import { AppResetpasswordComponent } from './pages/app-resetpassword/app-resetpassword.component';
import { NetworkStatusService } from './services/network-status.service';
import { AppHomeSourceUploadComponent } from './pages/app-home/app-home-source-upload/app-home-source-upload.component';
import { AppHomeBeeIdExpertComponent } from './pages/app-home/app-home-bee-id-expert/app-home-bee-id-expert.component';
import { AppHomeBeeIdGuidedComponent } from './pages/app-home/app-home-bee-id-guided/app-home-bee-id-guided.component';
import { AppHomeFlowerIdExpertComponent } from './pages/app-home/app-home-flower-id-expert/app-home-flower-id-expert.component';
import { AppFlowerSpeciesSelectorComponent } from './pages/app-flower-species-selector/app-flower-species-selector.component';
import { AppButterflySpeciesSelectorComponent } from './pages/app-butterfly-species-selector/app-butterfly-species-selector.component';

import { AppHomeSummaryComponent } from './pages/app-home/app-home-summary/app-home-summary.component';
import { AppHomeUserCommentComponent } from './pages/app-home/app-home-user-comment/app-home-user-comment.component';
import {
  AppLocationSelectorComponent,
  DialogDateAndLocationComponent
} from './pages/app-location-selector/app-location-selector.component';
import {
  AppLandingPageComponent,
  DialogLandingPageUpdateComponent
} from './pages/app-landing-page/app-landing-page.component';
import { AppHomeDateLocationComponent } from './pages/app-home/app-home-date-location/app-home-date-location.component';
import { ApiModule, Configuration } from './api-module';
import { SpeciesNamePipe } from './pipes/species-name.pipe';
import { TitleCasePipe } from '@angular/common';
import { SourceUploadContainerComponent } from './components/source-upload-container/source-upload-container.component';
import { DialogAppUpdateComponent } from './services/check-update.service';
import { AppHomeButterflyIdManualComponent } from './pages/app-home/app-home-butterfly-id-manual/app-home-butterfly-id-manual.component';
import {DragDropModule} from '@angular/cdk/drag-drop';


export function ApiModuleConfigurationFactory() {
  return new Configuration({basePath: environment.basePath});
}

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AppPageComponent,
    AppHomeComponent,
    AppLogSummaryComponent,
    AppLogsComponent,
    AppBeedexComponent,
    AppTutorialComponent,
    AppBeeDetailComponent,
    GoogleMapComponent,
    AppCaptureComponent,
    AppCropComponent,
    AppFlickityComponent,
    LoginPageComponent,
    CreateAccountComponent,
    DialogBeeBehaviorDialogComponent,
    DialogGuidedBeeIdentificationComponent,
    DialogGuidedFlowerIdentificationComponent,
    DialogDateAndLocationComponent,
    DialogVideoPreferenceComponent,
    DialogVideoUploadComponent,
    DialogVideoTooLargeComponent,
    DialogButterflyUploadComponent,
    FooterComponent,
    AppResetpasswordComponent,
    SnackBarInputBasic,
    AppHomeSourceUploadComponent,
    AppHomeBeeIdExpertComponent,
    AppHomeBeeIdGuidedComponent,
    AppHomeFlowerIdExpertComponent,
    AppFlowerSpeciesSelectorComponent,
    AppHomeSummaryComponent,
    AppHomeUserCommentComponent,
    AppLocationSelectorComponent,
    AppLandingPageComponent,
    DialogLandingPageUpdateComponent,
    AppHomeDateLocationComponent,
    SpeciesNamePipe,
    SourceUploadContainerComponent,
    DialogAppUpdateComponent,
    AppButterflySpeciesSelectorComponent,
    AppHomeButterflyIdManualComponent,
    AppButterflyDetailComponent,
    DialogButterflyBehaviorDialogComponent,
    DialogTailsGuideComponent,
    DialogShapeGuideComponent,
    DialogAntennaeGuideComponent,
    DialogPatternGuideComponent,
    DialogGuidedButterflyIdentificationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('/webapp/ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    FileUploadModule,
	// ERROR: @agm/core does not support angular ivy, need to replace this
    // AgmCoreModule.forRoot({
    //   apiKey: environment.googleMaps.key
    // }),
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
    AngularFireAuthModule,
    NguCarouselModule,
    ImageCropperModule,
    NgxWebstorageModule.forRoot(),
    ApiModule.forRoot(ApiModuleConfigurationFactory),
    DragDropModule,
  ],
//   entryComponents: [DialogBeeBehaviorDialogComponent, DialogButterflyBehaviorDialogComponent, DialogGuidedBeeIdentificationComponent,
//     DialogDateAndLocationComponent, DialogGuidedFlowerIdentificationComponent, DialogGuidedButterflyIdentificationComponent,
//     DialogVideoUploadComponent, DialogLandingPageUpdateComponent, DialogVideoPreferenceComponent, SnackBarInputBasic,
//     DialogAppUpdateComponent, DialogVideoTooLargeComponent, DialogButterflyUploadComponent,
//     DialogVideoTooLargeComponent, DialogTailsGuideComponent, DialogAntennaeGuideComponent, DialogPatternGuideComponent,
//     DialogShapeGuideComponent],
  providers: [
    FrameCaptureService,
    BeecologyAuthService,
    LocationService,
    BeelogService,
    CookieService,
    DatabaseService,
    NetworkStatusService,
    TitleCasePipe,
    SpeciesNamePipe,
    { provide: HAMMER_GESTURE_CONFIG, useClass: HammerGestureConfig /*GestureConfig*/ }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
