import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppPageComponent } from './pages/app-page/app-page.component';
import { AppHomeComponent} from './pages/app-home/app-home.component';
import { AppLogsComponent} from './pages/app-logs/app-logs.component';
import { AppBeedexComponent} from './pages/app-beedex/app-beedex.component';
import { AppTutorialComponent} from './pages/app-tutorial/app-tutorial.component';
import { AppBeeDetailComponent} from './pages/app-bee-detail/app-bee-detail.component';
import { AppButterflyDetailComponent} from './pages/app-butterfly-detail/app-butterfly-detail.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { CreateAccountComponent } from './pages/create-account/create-account.component';
import { AppResetpasswordComponent } from './pages/app-resetpassword/app-resetpassword.component';
import { AppLandingPageComponent } from './pages/app-landing-page/app-landing-page.component';


const routes: Routes = [
  { path: '', redirectTo: 'app', pathMatch: 'full' },

  {
    path: 'app',
    component: AppPageComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: AppLandingPageComponent },
      { path: 'record', component: AppHomeComponent },
      { path: 'logs', component: AppLogsComponent },
      { path: 'logs/:id', component: AppLogsComponent },
      { path: 'beedex', component: AppBeedexComponent },
      { path: 'tutorial', component: AppTutorialComponent },
      { path: 'detail/:id', component: AppBeedexComponent},
      { path: 'login', component: LoginPageComponent },
      { path: 'createaccount', component: CreateAccountComponent },
      { path: 'resetpassword', component: AppResetpasswordComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
