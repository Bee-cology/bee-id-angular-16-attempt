import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppButterflyDetailComponent } from './app-butterfly-detail.component';

describe('AppButterflyDetailComponent', () => {
  let component: AppButterflyDetailComponent;
  let fixture: ComponentFixture<AppButterflyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppButterflyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppButterflyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
