import { Component, OnInit, Input } from '@angular/core';

import { ButterflySpecies, ReferenceService } from '../../api-module';

@Component({
  selector: 'app-app-butterfly-detail',
  templateUrl: './app-butterfly-detail.component.html',
  styleUrls: ['./app-butterfly-detail.component.scss']
})
export class AppButterflyDetailComponent implements OnInit {
  @Input() butterfly: ButterflySpecies;
  @Input() butterflydexId: string;
  @Input() detailsEnabled = true;

  constructor(private butterflyService: ReferenceService) {
  }

  async ngOnInit() {
    if (!this.butterfly) {
      this.butterfly = await this.butterflyService.getButterfly(this.butterflydexId).toPromise();
    }
  }
}
