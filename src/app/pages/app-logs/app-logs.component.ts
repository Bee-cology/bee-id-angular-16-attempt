import { Component, OnInit, ViewChild } from '@angular/core';
import { BeecologyAuthService } from '../../services/authentication/beecology-auth.service';
import { DatabaseService } from '../../services/database.service';
// import { MatSelectionList, MatSnackBar } from '@angular/material';
import { MatSelectionList } from '@angular/material/list';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as _ from 'lodash';
import { BeeLogRecord } from '../../models/bee-log-record.model';
import { ButterflyLogRecord } from '../../models/butterfly-log-record.model';
import { LocationService } from '../../services/location.service';
import { CheckUpdateService } from '../../services/check-update.service';
import { SavedLocationsService } from '../../services/saved-locations.service';
import { SnackBarInputBasic } from '../../models/app-snackbars';
import { BeeRecord, ButterflyRecord, DefaultService } from '../../api-module';
import sizeof from 'object-sizeof';
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-app-logs',
  templateUrl: './app-logs.component.html',
  styleUrls: ['./app-logs.component.scss']
})
export class AppLogsComponent implements OnInit {
  @ViewChild('logs', { static: false }) logsList: MatSelectionList;
  @ViewChild('locationSelector', { static: false }) locationSelector;
  @ViewChild('flowerSpeciesSelector', { static: false }) flowerSpeciesSelector;
  logs: PouchDB.Core.ExistingDocument<BeeLogRecord>[];
  editLogs: PouchDB.Core.ExistingDocument<BeeLogRecord>[];
  submittedLogs: BeeRecord[];
  logsButterfly: PouchDB.Core.ExistingDocument<ButterflyLogRecord>[];
  editLogsButterfly: PouchDB.Core.ExistingDocument<ButterflyLogRecord>[];
  submittedLogsButterfly: ButterflyRecord[];
  submittedLogsChanged = false;
  currentLog;
  currentSubmitLog: any;
  currentSubmitLogButterfly: ButterflyRecord;
  listSection = true;
  detailSection = false;
  editSection = false;
  submittedSection = false;
  detailSubmittedSection = false;
  loading = {
    'logs': true,
    'submittedLogs': true,
    'editLogs': true
  };
  checkLoading = false;
  operationInProgress = false;
  modifyLocationSection = false;
  selectedOptions = [];
  isLoggedIn: boolean;
  selectAll = false;
  selectAllButterflies = false;
  modifyFlowerSection = false;
  beeIcon = 'assets/offline-assets/icons/beeIconLarge.png';
  butterflyIcon = 'assets/offline-assets/icons/butterflyIconLarge.png';

  constructor(
    private auth: BeecologyAuthService,
    private db: DatabaseService,
    private locationService: LocationService,
    public snackBar: MatSnackBar,
    private savedLocationsService: SavedLocationsService,
    private checkUpdateService: CheckUpdateService,
    private apiService: DefaultService,
    private activatedRoute: ActivatedRoute,
    private location: Location) {
    this.auth.$isLoggedIn.subscribe(bool => {
      this.isLoggedIn = bool;
      this.getSubmittedLogs().then();
    });
    this.isLoggedIn = this.auth.isLoggedIn();

    this.activatedRoute.paramMap.subscribe(async (params: Params) => {
      const id = params.get('id');
      if (id) {
        this.currentSubmitLog = await this.getSubmittedLog(id);
        console.log('Current Sumbit log after get submittedLog')
        console.log(this.currentSubmitLog);
        if (!this.currentSubmitLog) {
          this.currentSubmitLog = await this.getSubmittedLogButterfly(id);
        }
        this.listSection = false;
        this.detailSubmittedSection = true;
      }
    });
  }

  ngOnInit() {
    this.checkUpdateService.checkForUpdate();
    this.getLogs();
    this.getSubmittedLogs().then();
  }

  getLogs() {

    this.db.getLocalLogsButterfly()
      .then((rows) => {
        this.logsButterfly = rows;

        // this.loading['logs'] = false;

        this.logsButterfly.filter(log => log.butterflyid);
        this.editLogsButterfly = _.reverse(_.sortBy(this.logsButterfly, log => log._id));
        // this.loading['edit Logs'] = false;
      })
      .catch(e => console.log(e)); // TODO: better error handling
      this.db.logSourceButterfly$.subscribe(source => this.editLogsButterfly = source);

    this.db.getLocalLogs()
      .then((rows) => {
        this.logs = rows;
        this.loading['logs'] = false;

        this.logs.filter(log => log.chead);
        this.editLogs = _.reverse(_.sortBy(this.logs, log => log._id));
        this.loading['editLogs'] = false;
      })
      .catch(e => console.log(e)); // TODO: better error handling



      this.db.logSource$.subscribe(source => this.editLogs = source);
  }//

  async getSubmittedLogs() {
    this.loading['submittedLogs'] = true;
    if (this.isLoggedIn) {
      console.log('Getting submitted logs...');
      const logs: BeeRecord[] = await this.apiService.getRecords(this.auth.getUserID()).toPromise();
      this.submittedLogs = _.reverse(_.sortBy(logs, log => log.time));
      // remove videos from submitted log media
      for (const log of this.submittedLogs) {
        if (log.media) {
          log.media = log.media.filter(x => x.split('png').length > 1 || x.split('jpeg').length > 1);
        }
      }
      this.submittedLogsChanged = false;
    }
    //
    if (this.isLoggedIn) {
      console.log('Getting submitted logs...');
      // tslint:disable-next-line:max-line-length
      const logs: ButterflyRecord[] = await this.apiService.getButterflyRecords(this.auth.getUserID()).toPromise(); // requires an update to autogenerated files
      this.submittedLogsButterfly = _.reverse(_.sortBy(logs, log => log.time));
      // remove videos from submitted log media
      for (const log of this.submittedLogsButterfly) {
        if (log.media) {
          log.media = log.media.filter(x => x.split('png').length > 1 || x.split('jpeg').length > 1);
        }
      }
      this.submittedLogsChanged = false;
    }
    //
    this.loading['submittedLogs'] = false;
  }//

  async getSubmittedLog(id): Promise<BeeRecord> {
    return new Promise(async (resolve) => {
      const log: BeeRecord = await this.apiService.getRecord(id).toPromise();
      if (log.media) {
        log.media = log.media.filter(x => x.split('png').length > 1 || x.split('jpeg').length > 1);
      }
      resolve(log);
    });
  }

  async getSubmittedLogButterfly(id): Promise<ButterflyRecord> {
    return new Promise(async (resolve) => {
      const log: ButterflyRecord = await this.apiService.getButterflyRecord(id).toPromise();
      if (log.media) {
        log.media = log.media.filter(x => x.split('png').length > 1 || x.split('jpeg').length > 1);
      }
      resolve(log);
    });
  }

  showDetail(log) {
    this.selectAll = false;
    this.selectedOptions = [];
    this.currentLog = log;
    this.detailSection = true;
    this.listSection = false;
  }

  modifyLocation() {
    this.modifyLocationSection = true;
  }

  modifyFlower() {
    this.modifyFlowerSection = true;
  }

  async confirmLocation() {
    if (this.locationSelector.validate()) {
      const cityname = this.locationSelector.locationAddress;
      const hasLocation = this.locationSelector.hasLocation;
      const loc = this.locationService.coordsToString(this.locationSelector.lat, this.locationSelector.lng);
      if (!hasLocation) {
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 2000,
          data: { 'messageKey': 'error-log-location-missing-confirmation', 'icon': 'error' },
          panelClass: ['sb-error']
        });
      } else {
        if (this.currentLog.chead) { // check if bee or butterfly log
          await this.db.updateLocation(this.currentLog, hasLocation, loc, cityname).then(
            (response) => {
              this.currentLog.hasLocation = hasLocation;
              this.currentLog.loc = loc;
              this.currentLog.cityname = cityname;
              this.currentLog._rev = response.rev;
              const snackBarRef = this.snackBar.openFromComponent(SnackBarInputBasic, {
                duration: 2000,
                data: { 'messageKey': 'success-log-location-update', 'icon': 'success' },
                panelClass: ['sb-success']
              });
              snackBarRef.afterDismissed().subscribe(() => {
                this.snackBar.openFromComponent(SnackBarInputBasic, {
                  duration: 2000,
                  data: { 'messageKey': 'info-log-location-saved', 'icon': 'info' },
                  panelClass: ['sb-info']
                });
              });
            }).catch((err) => {
            console.log(err);
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 2000,
              data: { 'messageKey': 'error-log-location-update', 'icon': 'error' },
              panelClass: ['sb-error']
            });
          });
          this.modifyLocationSection = false;
          if (this.locationSelector.newLocation) {
            this.savedLocationsService.saveLocation(this.locationSelector.lat, this.locationSelector.lng);
          }
        } else {
          console.log('Hit correct Side');
          await this.db.updateLocationButterfly(this.currentLog, hasLocation, loc, cityname).then(
            (response) => {
              this.currentLog.hasLocation = hasLocation;
              this.currentLog.loc = loc;
              this.currentLog.cityname = cityname;
              this.currentLog._rev = response.rev;
              const snackBarRef = this.snackBar.openFromComponent(SnackBarInputBasic, {
                duration: 2000,
                data: { 'messageKey': 'success-log-location-update', 'icon': 'success' },
                panelClass: ['sb-success']
              });
              snackBarRef.afterDismissed().subscribe(() => {
                this.snackBar.openFromComponent(SnackBarInputBasic, {
                  duration: 2000,
                  data: { 'messageKey': 'info-log-location-saved', 'icon': 'info' },
                  panelClass: ['sb-info']
                });
              });
            }).catch((err) => {
            console.log(err);
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 2000,
              data: { 'messageKey': 'error-log-location-update', 'icon': 'error' },
              panelClass: ['sb-error']
            });
          });
          this.modifyLocationSection = false;
          if (this.locationSelector.newLocation) {
            this.savedLocationsService.saveLocation(this.locationSelector.lat, this.locationSelector.lng);
          }
        }

      }
    }
  }

  async confirmFlower() {
    if (this.flowerSpeciesSelector.validate()) {
      const data = this.flowerSpeciesSelector.data;
      if (this.currentLog.chead) { // checks if bee or butterfly
        await this.db.updateFlower(this.currentLog, data.flowerName, data.flowerFamilyName,
          data.flowerCommonName, data.flowerShape, data.flowerLifeCycle, data.flowerColor).then(
          (response) => {
            this.currentLog.fname = data.flowerName;
            this.currentLog.fcname = data.flowerCommonName;
            this.currentLog.fshape = data.flowerShape;
            this.currentLog.fcolor = data.flowerColor;
            this.currentLog._rev = response.rev;
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 2000,
              data: { 'messageKey': 'success-log-flower-update', 'icon': 'success' },
              panelClass: ['sb-success']
            });
          }).catch((err) => {
          console.log(err);
          this.snackBar.openFromComponent(SnackBarInputBasic, {
            duration: 2000,
            data: { 'messageKey': 'error-log-flower-update', 'icon': 'error' },
            panelClass: ['sb-error']
          });
        });
        this.modifyFlowerSection = false;
      } else {
        await this.db.updateFlowerButterfly(this.currentLog, data.flowerName, data.flowerFamilyName,
          data.flowerCommonName, data.flowerShape, data.flowerLifeCycle, data.flowerColor).then(
          (response) => {
            this.currentLog.fname = data.flowerName;
            this.currentLog.fcname = data.flowerCommonName;
            this.currentLog.fshape = data.flowerShape;
            this.currentLog.fcolor = data.flowerColor;
            this.currentLog._rev = response.rev;
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 2000,
              data: { 'messageKey': 'success-log-flower-update', 'icon': 'success' },
              panelClass: ['sb-success']
            });
          }).catch((err) => {
          console.log(err);
          this.snackBar.openFromComponent(SnackBarInputBasic, {
            duration: 2000,
            data: { 'messageKey': 'error-log-flower-update', 'icon': 'error' },
            panelClass: ['sb-error']
          });
        });
        this.modifyFlowerSection = false;
      }
    }
  }

  showDetailSubmit(log) {
    console.log('Current Submit log')
    console.log(log)
    // if (log.beeCharacteristic) {
      this.currentSubmitLog = log;
      console.log('currentSubmitLog')
      console.log(this.currentSubmitLog)
    // } else if (log.butterflyCharacteristic) {// likely an error here
    //   this.currentSubmitLogButterfly = log;
    //   console.log('currentSubmitLogButterfly')
    //   console.log(this.currentSubmitLogButterfly)
    // }


    this.location.go('/app/logs/' + log.id);
    this.submittedSection = false;
    this.detailSubmittedSection = true;
  }

  openEdit() {
    this.editSection = true;

    this.listSection = false;
  }

  openSubmitted() {
    this.submittedSection = true;
    this.listSection = false;
    if (this.isLoggedIn && (this.submittedLogsChanged || !(this.submittedLogs || this.submittedLogsButterfly))) {
      this.getSubmittedLogs().then();
      /*
      const logs: BeeRecordUserResponse = await this.auth.getAllLogs();
      this.loading['submittedLogs'] = false;
      this.submittedLogs = logs.data;
      this.submittedLogs = _.reverse(_.sortBy(this.submittedLogs, log => log.date));
      */
    }
  }

  back() {
    if (this.modifyLocationSection || this.modifyFlowerSection) {
      this.modifyLocationSection = false;
      this.modifyFlowerSection = false;
      return;
    }
    this.detailSection = false;
    this.editSection = false;
    this.submittedSection = false;
    this.listSection = true;
    this.db.getLocalLogs()
      .then((rows) => {
        this.loading['logs'] = false;
        this.logs = rows;
        this.logs.filter(log => log.chead);
        this.editLogs = _.reverse(_.sortBy(this.logs, log => log.time));
      })
      .catch(e => console.log(e)); // TODO: better error handling
      this.db.getLocalLogsButterfly()
      .then((rows) => {
        this.loading['logs'] = false;
        this.logsButterfly = rows;
        this.logsButterfly.filter(log => log.butterflyname);
        this.editLogsButterfly = _.reverse(_.sortBy(this.logsButterfly, log => log.time));
      })
      .catch(e => console.log(e)); // TODO: better error handling
  }

  backSubmit() {
    this.detailSubmittedSection = false;
    this.currentSubmitLog = null;
    this.currentSubmitLogButterfly = null;
    this.location.go('/app/logs');
    if (this.isLoggedIn) {
      this.submittedSection = true;
    } else {
      this.listSection = true;
    }
  }

  redirect(options) {
    this.editSection = false;
    this.showDetail(options[0]);
  }

  submitChecked() {
    if (this.selectedOptions.length > 0) {
      if (this.selectedOptions.filter(log => !log.hasLocation).length > 0) {
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 3000,
          data: { 'messageKey': 'error-log-location-missing-submission', 'icon': 'error' },
          panelClass: ['sb-error']
        });
        this.redirect(this.selectedOptions.filter(log => !log.hasLocation));
      } else if (this.selectedOptions.filter(log => log.fname === 'SET LATER').length > 0) {
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 3000,
          data: { 'messageKey': 'error-log-flower-missing-submission', 'icon': 'error' },
          panelClass: ['sb-error']
        });
        this.redirect(this.selectedOptions.filter(log => log.fname === 'SET LATER'));
      } else {
        const count = this.selectedOptions.length;
        this.operationInProgress = true;
        this.checkLoading = true;

        // need to sort out here
        const selectedOptionsBees = [];
        const selectedOptionsButterflies = [];
        let selectedBees = false;
        let selectedButterflies = false;
        for (let i = 0; i < this.selectedOptions.length; i++) {
          if (this.selectedOptions[i].chead) {// is bee
            console.log('bees');
          selectedOptionsBees.push(this.selectedOptions[i]);
          selectedBees = true;
          } else {
            console.log('butter');
            selectedOptionsButterflies.push(this.selectedOptions[i]);
            selectedButterflies = true;
          }
        }
        if (selectedButterflies) {
          this.db.postLogsButterfly(selectedOptionsButterflies).then(res => {

            this.operationInProgress = false;
            this.checkLoading = false;
            this.submittedLogsChanged = true;
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 3000,
              data: { 'message': count, 'messageKey': 'success-log-submission', 'icon': 'success' },
              panelClass: ['sb-success']
            });
          }).catch(err => {
            this.operationInProgress = false;
            this.checkLoading = false;
            console.log(err);
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 2000,
              data: { 'messageKey': 'error-log-submission', 'icon': 'error' },
              panelClass: ['sb-error']
            });
          });
        }

        if (selectedBees) {
          this.db.postLogs(selectedOptionsBees).then(res => {

            this.operationInProgress = false;
            this.checkLoading = false;
            this.submittedLogsChanged = true;
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 3000,
              data: { 'message': count, 'messageKey': 'success-log-submission', 'icon': 'success' },
              panelClass: ['sb-success']
            });
          }).catch(err => {
            this.operationInProgress = false;
            this.checkLoading = false;
            console.log(err);
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 2000,
              data: { 'messageKey': 'error-log-submission', 'icon': 'error' },
              panelClass: ['sb-error']
            });
          });
        }
      }
    } else {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: { 'messageKey': 'info-select-logs-submission', 'icon': 'info' },
        panelClass: ['sb-info']
      });
    }
    this.selectedOptions = [];
    this.selectAll = false;

    //this.back();//backinghere doesnt work for some reason
  }
//
  dbDelete(log) {
    if (log.chead) {
      return this.db.deleteLog(log);
    } else {
      return this.db.deleteLogButterfly(log);
    }


  }

  deleteChecked() {
    console.log(this.editLogsButterfly);
    if (this.selectedOptions.length > 0) {
      // tslint:disable-next-line:max-line-length
      const deletingAll = this.selectedOptions.length === (this.editLogs.length + this.editLogsButterfly.length); // not sure this is correct
      const count = this.selectedOptions.length;
      this.operationInProgress = true;


      Promise.all(this.selectedOptions.map(log => this.dbDelete(log))).then(reses => {//
        this.operationInProgress = false;
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 2000,
          data: { 'message': count, 'messageKey': 'success-log-deletion', 'icon': 'success' },
          panelClass: ['sb-success']
        });
        if (deletingAll
          //|| !deletingAll//temporarily want to back out anything
          ) {
          this.back();
        }
      }).catch(err => {
        this.operationInProgress = false;
        console.log(err);
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 2000,
          data: { 'messageKey': 'error-log-deletion', 'icon': 'error' },
          panelClass: ['sb-error']
        });
      });





    } else {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: { 'messageKey': 'info-select-logs-deletion', 'icon': 'info' },
        panelClass: ['sb-info']
      });
    }
    console.log(this.editLogsButterfly)

    this.selectedOptions = [];
    this.selectAll = false;
    this.selectAllButterflies = false;
    console.log(this.editLogsButterfly)
  }
//
  onSelectedOptionsChange() {
    console.log("runBees");
    const selectedOptionsBees = [];
    for (let i = 0; i < this.selectedOptions.length; i++) {
      if (this.selectedOptions[i].chead) {// is bee

        selectedOptionsBees.push(this.selectedOptions[i]);
      }
    }
    this.selectedOptions = selectedOptionsBees;
    this.selectAllButterflies = false;
    this.selectAll = ((selectedOptionsBees.length === (this.logs.length)) && !this.selectAll);

    if(selectedOptionsBees.length > 0){
      this.selectAllButterflies = false;
      console.log("selectedOptionsBees");
    }

  }

  onSelectedOptionsChangeButterfly(){
    console.log("runButterflies");
    const selectedOptionsButterflies = [];
    for (let i = 0; i < this.selectedOptions.length; i++) {
      if (!this.selectedOptions[i].chead) {// is bee

        selectedOptionsButterflies.push(this.selectedOptions[i]);
      }
    }
    this.selectedOptions = selectedOptionsButterflies;
    this.selectAll = false;
    this.selectAllButterflies = ((selectedOptionsButterflies.length === (this.logsButterfly.length)) && !this.selectAllButterflies );
    // if(this.selectAllButterflies){
      this.selectAll = false;
      console.log("selectedOptionsButterflies");
    // }
  }

  getSize(log: object) {
    const kilobytes = sizeof(log) / 1000;
    if (kilobytes < 1000) {
      return Math.round(kilobytes) + ' KB';
    } else {
      return Math.round(kilobytes / 1000) + ' MB';
    }
  }

}
