import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ButterflySpecies, BeeSpecies, ReferenceService } from '../../api-module';

interface ButterflyFamily {
  name: string;
  imgLoc: string;
  subheading: string;
}

const butterflyFamilies: ButterflyFamily[] = [
  {
    name: 'Nymphalidae',
    imgLoc: 'assets/butterflyImages/nymphalidae.jpg',
    subheading: 'The Brushfoot Family'
  },
  {
    name: 'Pieridae',
    imgLoc: 'assets/butterflyImages/pieridae.jpeg',
    subheading: 'The Whites and Sulphur Family'
  },
  {
    name: 'Papilionidae',
    imgLoc: 'assets/butterflyImages/papilionidae.jpg',
    subheading: 'The Swallowtail Family'
  },
  {
    name: 'Lycaenidae',
    imgLoc: 'assets/butterflyImages/lycaenidae.jpeg',
    subheading: 'The Hairstreak, Blues, and Gossamar Family'
  },
  {
    name: 'Hesperiidae',
    imgLoc: 'assets/butterflyImages/hesperiidae.jpg',
    subheading: 'The Skippers Family'
  }
];

@Component({
  selector: 'app-app-beedex',
  templateUrl: './app-beedex.component.html',
  styleUrls: ['./app-beedex.component.scss']
})
export class AppBeedexComponent implements OnInit {
  bees: BeeSpecies[];
  selectedBee: BeeSpecies;
  butterflies: ButterflySpecies[];
  butterfliesNymph: ButterflySpecies[];
  butterfliesPieridae: ButterflySpecies[];
  butterfliesPap: ButterflySpecies[];
  butterfliesLyc: ButterflySpecies[];
  butterfliesHesp: ButterflySpecies[];
  selectedButterfly: ButterflySpecies;
  families: ButterflyFamily[] = butterflyFamilies;
  selectedFamily: string;
  butterflyFormat = false;

  constructor(private beedexService: ReferenceService,
              private butterflydexService: ReferenceService,
              private route: ActivatedRoute) {
  }

  async ngOnInit() {
    await this.getBeedex();
    await this.getButterflydex();
    this.butterflyFormat = false;
  }

  async getBeedex() {
    this.bees = await this.beedexService.getBees().toPromise();
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.selectedBee = this.bees.find(bee => bee.id === id);
    }
  }
  async getButterflydex() {
    this.butterflies = await this.butterflydexService.getButterflies().toPromise();
    this.butterfliesNymph = await this.butterflydexService.getButterflies(null, ['Nymphalidae']).toPromise();
    this.butterfliesHesp = await this.butterflydexService.getButterflies(null, ['Hesperiidae']).toPromise();
    this.butterfliesLyc = await this.butterflydexService.getButterflies(null, ['Lycaenidae']).toPromise();
    this.butterfliesPap = await this.butterflydexService.getButterflies(null, ['Papilionidae']).toPromise();
    this.butterfliesPieridae = await this.butterflydexService.getButterflies(null, ['Pieridae']).toPromise();
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.selectedButterfly = this.butterflies.find(butterfly => butterfly.id === id);
    }
  }
}
