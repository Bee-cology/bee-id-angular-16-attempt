import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppBeedexComponent } from './app-beedex.component';

describe('AppBeedexComponent', () => {
  let component: AppBeedexComponent;
  let fixture: ComponentFixture<AppBeedexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppBeedexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppBeedexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
