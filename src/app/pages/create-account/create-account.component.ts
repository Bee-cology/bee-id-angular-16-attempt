import { Component } from '@angular/core';
import { BeecologyAuthService, Method } from '../../services/authentication/beecology-auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarInputBasic } from '../../models/app-snackbars';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent {

  password = '';
  emailAddress = '';

  constructor(private auth: BeecologyAuthService,
              private router: Router,
              public snackBar: MatSnackBar) { }

  private createAccount(email: string, password: string) {
    return this.auth.createAccount(email, password);
  }

  async submit() {
    if (this.password !== '' && this.emailAddress !== '') {
      try {
        await this.createAccount(this.emailAddress, this.password);
        await this.auth.signInWith(Method.Email, { email: this.emailAddress, password: this.password });
        this.snackBar.openFromComponent(SnackBarInputBasic, { duration: 2000, data: {'messageKey': 'success-account-creation', 'icon': 'success'}, panelClass: ['sb-success']});
        this.router.navigate(['/app']).then();
      } catch (err) {
        let messageKey;
        if (err.code === 'auth/invalid-email') {
          messageKey = 'error-account-email-invalid';
        } else if (err.code === 'auth/weak-password') {
          messageKey = 'error-account-password-weak';
        } else if (err.code === 'auth/email-already-in-use') {
          messageKey = 'error-account-email-taken';
        }
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 2000,
          data: {'messageKey': messageKey, 'icon': 'error'},
          panelClass: ['sb-error']});
      }
    } else {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: {'messageKey': 'error-account-field-input', 'icon': 'error'},
        panelClass: ['sb-error']});
    }
  }

}
