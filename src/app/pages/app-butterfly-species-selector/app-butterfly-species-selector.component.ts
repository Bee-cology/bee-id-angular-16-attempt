import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SnackBarInputBasic } from '../../models/app-snackbars';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ButterflySpecies, ReferenceService } from '../../api-module';
import { SpeciesNamePipe } from '../../pipes/species-name.pipe';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-app-butterfly-species-selector',
  templateUrl: './app-butterfly-species-selector.component.html',
  styleUrls: ['./app-butterfly-species-selector.component.scss']
})
export class AppButterflySpeciesSelectorComponent implements OnInit {
  @Input() canSkip = false; // whether  species can be set later
  disabled = false; // variable to replace butterflyControl.enabled which doesn't update properly

  butterflyControl = new FormControl();
  butterflyList: ButterflySpecies[];
  filteredButterflyList: Observable<ButterflySpecies[]>;
  data: any;
  includeUnknown = true;

  constructor(private butterflyListService: ReferenceService,
              private speciesNamePipe: SpeciesNamePipe,
              public titleCasePipe: TitleCasePipe,
              public snackBar: MatSnackBar) {
  }

  async ngOnInit() {
  this.butterflyList = await this.butterflyListService.getButterflies().toPromise();
  this.butterflyList.sort(function(a, b) { return (a.species > b.species) ? 1 : -1; });
  this.filteredButterflyList = this.butterflyControl.valueChanges.pipe(
    startWith(''),
    map(value => this.butterflyFilter(value))
  );
  }

  // filters butterflyList based on butterflyControl.value
  private butterflyFilter(value: string): ButterflySpecies[] {
    if (value !== null) {
      value = value.toLowerCase() as string;
      this.includeUnknown = 'unknown'.includes(value);
      return this.butterflyList.filter(butterfly => {
        return butterfly.species.toLowerCase().includes(value) ||
          butterfly.common_names.join(' ').toLowerCase().includes(value);
      });
    }
  }

  // toggles set later option, called by SET LATER and SET NOW button
  disable() {
    this.disabled = !this.disabled;
    if (this.disabled) {
      this.butterflyControl.disable();
      this.butterflyControl.setValue('');
    } else {
      this.butterflyControl.enable();
    }
  }

  validate(): boolean {
    if (this.disabled) {
      this.data = {
        'butterflyname': 'SET LATER',
        'butterflyfamily': 'SET LATER'
        // 'butterflyid': 'SET LATER'
        // TODO 'flowerCommonName': matched.common_name//this.titleCasePipe.transform(matched.alt_names.join(', ')),
      };
    } else if (this.butterflyControl.value) {
      let matched: any;
      if (this.butterflyList.find(x => x.species.toLowerCase() === this.butterflyControl.value.toLowerCase())) {
        matched = this.butterflyList.find(x => x.species.toLowerCase() === this.butterflyControl.value.toLowerCase());
      } else {
        matched = this.butterflyList.find(x => x.common_names[0].toLowerCase() === this.butterflyControl.value.toLowerCase());
      }
      // const matched = this.butterflyList.find(x => x.common_names[0] === this.butterflyControl.value.toLowerCase());
      // const matched = this.butterflyList.find(x => x.species.toLowerCase() === this.butterflyControl.value.toLowerCase());
      if (matched) {
        this.data = {
          'butterflyname': matched.species,
          'butterflyfamily': matched.family
          // 'butterflyname': this.speciesNamePipe.transform(matched),
          // 'butterflyid': matched.id,
         // TODO 'flowerCommonName': matched.common_name//this.titleCasePipe.transform(matched.alt_names.join(', ')),
        };
      } else if (this.butterflyControl.value === 'Unknown') {
        this.data = {
          'butterflyname': null,
          'butterflyfamily': null
          // 'butterflyid': null

        };
      } else { // invalid species value
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 2000,
          data: { 'messageKey': 'error-butterfly-name', 'icon': 'error' },
          panelClass: ['sb-error']
        });
        return false;
      }
    } else { // no  value
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: { 'messageKey': 'error-field-input', 'icon': 'error' },
        panelClass: ['sb-error']
      });
      return false;
    }
    return true;
  }

  setValue(value) {
    this.butterflyControl.setValue(value);
  }

  getValue() {
    return this.butterflyControl.value;
  }
}

