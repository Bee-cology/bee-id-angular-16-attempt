import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppButterflySpeciesSelectorComponent } from './app-butterfly-species-selector.component';

describe('AppButterflySpeciesSelectorComponent', () => {
  let component: AppButterflySpeciesSelectorComponent;
  let fixture: ComponentFixture<AppButterflySpeciesSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppButterflySpeciesSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppButterflySpeciesSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
