import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BeecologyAuthService, Method } from '../../services/authentication/beecology-auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarInputBasic } from '../../models/app-snackbars';
import { DatabaseService } from '../../services/database.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  isSignedIn;
  persist = false;
  emailAddress = '';
  password = '';

  constructor(private authService: BeecologyAuthService,
              private router: Router,
              public snackBar: MatSnackBar,
              private databaseService: DatabaseService) { }

  ngOnInit() {
    this.authService.$isLoggedIn.subscribe(b => {
      this.isSignedIn = b;
    });
    this.isSignedIn = this.authService.isLoggedIn();
  }

  signInWithEmail(persist, email, password) {
    this.authService.signInWith(Method.Email, { persist: persist, email: email, password: password})
      .then((res) => {
        this.router.navigate(['/app']).then();
      })
      .catch((err) => {
        let messageKey;
        if (err.code === 'auth/invalid-email') {
          messageKey = 'error-account-email-invalid';
        } else if (err.code === 'auth/wrong-password') {
          messageKey = 'error-account-password-incorrect';
        } else if (err.code === 'auth/user-not-found') {
          messageKey = 'error-account-email-unknown';
        } else if (err.code === 'auth/too-many-requests') {
          messageKey = 'error-account-too-many-attempts';
        }
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 2000,
          data: {'messageKey': messageKey, 'icon': 'error'},
          panelClass: ['sb-error']});
      });
  }

  signInWithFacebook() {
    this.authService.signInWith(Method.Facebook, {persist: this.persist}).then((res) => {
      this.router.navigate(['/app']).then();
    })
      .catch((err) => console.log('error: ' + err));
  }

  signInWithGoogle() {
    this.authService.signInWith(Method.Google, { persist: this.persist }).then((res) => {
      this.router.navigate(['/app']).then();
    })
      .catch((err) => console.log('error: ' + err));
  }

  submit() {
    if (this.password !== '' && this.emailAddress !== '') {
      this.signInWithEmail(this.persist, this.emailAddress, this.password);
    } else {
      this.snackBar.openFromComponent(SnackBarInputBasic, { duration: 2000, data: {'messageKey': 'error-account-field-input', 'icon': 'error'}, panelClass: ['sb-error']});
    }
  }

  getUsername = () => this.authService.getUsername();

  getEmail = () => this.authService.getEmail();

  signOut = () => this.authService.logout();
}
