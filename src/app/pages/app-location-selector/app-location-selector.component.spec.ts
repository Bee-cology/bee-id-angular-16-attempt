import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLocationSelectorComponent } from './app-location-selector.component';

describe('AppLocationSelectorComponent', () => {
  let component: AppLocationSelectorComponent;
  let fixture: ComponentFixture<AppLocationSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLocationSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLocationSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
