import { Component, OnInit, Inject, Output, EventEmitter, Input } from '@angular/core';
import { zip } from 'rxjs';
import { Marker } from '../../models/marker.model';
import { LocationService } from '../../services/location.service';
import { NetworkStatusService } from '../../services/network-status.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SavedLocationsService } from '../../services/saved-locations.service';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

enum LocationType { RECENT, AUTO, MANUAL, SET_LATER}

@Component({
  selector: 'app-app-location-selector',
  templateUrl: './app-location-selector.component.html',
  styleUrls: ['./app-location-selector.component.scss']
})
export class AppLocationSelectorComponent implements OnInit {
  @Input() canSkip = true;
  @Output() skipEmitter = new EventEmitter<any>();
  // GPS
  isOnline = true;
  gpsTimeout = true;
  gpsTimer;
  gpsSubscription;

  // LOCATION
  chosenLocationType: LocationType = LocationType.SET_LATER;
  locationAddress: string = null;
  hasLocation = false;
  initLat = 42.3601;
  initLng = -71.057083;
  lat;
  lng;
  currentLocationId = -1;
  autoLocationId = -1;
  recentLocationIds = Array<number>();
  manualMarkers = [];
  newLocation;
  searchString;

  constructor(
    public locationService: LocationService,
    private networkStatus: NetworkStatusService,
    private savedLocationsService: SavedLocationsService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog) { }

  ngOnInit() {
    // Check initial network status
    this.networkStatus.checkOnline().then((isOnline) => {
      this.isOnline = isOnline;
    });
    // Set up network status monitoring
    this.networkStatus.isOnline.subscribe(() => {
      console.log('now online');
      this.isOnline = true;
    });
    this.networkStatus.isOffline.subscribe(() => {
      console.log('now offline');
      this.isOnline = false;
    });

    this.initiateMap();
  }

  skip() {
    this.currentLocationId = -1;
    this.skipEmitter.emit();
  }

  initiateMap() {
    console.log('Initiating map...')
    this.removeMarkers(); // clear map
    this.getAutoLocation(); // get GPS location
    this.getSavedLocations(); // get recent locations
    this.getManualLocations();
  }

  removeMarkers() {
    this.currentLocationId = -1;
    this.locationService.clearMarker();
  }

  removeMarker() {
    if (this.getLocationType() === 'MANUAL') {
      const index = this.manualMarkers.findIndex(manualMarkers => manualMarkers.id === this.lat.toString() + this.lng.toString());
      this.manualMarkers.splice(index, 1);
      this.initiateMap();
    } else if (this.getLocationType() === 'RECENT') {
      const marker = this.locationService.markers[this.currentLocationId];
      this.savedLocationsService.removeLocation(marker.lat, marker.lng).then((res) => {
        if (res != null) {
          this.initiateMap();
        } else {
          console.log('An error occurred while trying to remove that marker.');
        }
      });
    }
  }

  setLocationType(type) { this.chosenLocationType = type; }

  updateLocationType() {
    const id = this.currentLocationId;
    if (id === -1) {
      this.setLocationType(LocationType.SET_LATER);
    } else if (id === this.autoLocationId) {
      this.setLocationType(LocationType.AUTO);
    } else if (this.recentLocationIds.indexOf(id) !== -1) {
      this.setLocationType(LocationType.RECENT);
    } else {
      this.setLocationType(LocationType.MANUAL);
    }
  }

  getLocationType() { return LocationType[this.chosenLocationType];}

  getAutoLocation() {
    this.autoLocationId = -1;
    this.hasLocation = false;
    this.gpsTimeout = false;
    // Theoretically we shouldn't need to check network status here, but it's probably better to, just in case.
    this.gpsTimer = setTimeout(() => { this.gpsSubscription.unsubscribe(); if (!this.gpsTimeout) { this.gpsTimeout = true; } }, 15000);
    this.gpsSubscription = zip(this.networkStatus.checkOnline(), this.locationService.getCurrentLocation()).subscribe((statusLocation) => {
      if (!this.gpsTimeout) {
        this.hasLocation = true;
        this.setLocationType(LocationType.AUTO);
        this.gpsTimeout = true;
        clearTimeout(this.gpsTimer);
        const networkStatus: boolean = statusLocation[0];
        const location: GeolocationPosition = statusLocation[1];
        this.isOnline = networkStatus;
        this.lat = location.coords.latitude;
        this.lng = location.coords.longitude;
        const promise = this.locationService.addMarker(this.lat, this.lng, '', false, 0.5,
          'assets/offline-assets/icons/marker_primary.png');
        promise.then(res => {
          const id = res[res.length - 1]['id'];
          this.currentLocationId = id;
          this.autoLocationId = id;
          this.setLocationType(LocationType.AUTO);
          this.getLocationAddress();
        });
      }
    });
  }

  getSavedLocations() {
    this.recentLocationIds = [];
    this.savedLocationsService.getLocations().then((savedLocations) => {
      if (savedLocations.length > 0) {
        this.hasLocation = true;
        for (const location of savedLocations) {
          const promise = this.locationService.addMarker(location['lat'], location['lng'], '', false, 0.5,
            'assets/offline-assets/icons/marker_accent.png');
          promise.then(markers => {
            const id = markers[markers.length - 1]['id'];
            this.recentLocationIds.push(id);
          });
        }
      }
    });
  }

  getManualLocations() {
    for (const location of this.manualMarkers) {
      this.locationService.addMarker(location['lat'], location['lng']).then();
    }
  }

  getLocationAddress() {
    if (this.isOnline) {
      this.locationService.getLocationAddress(this.lat, this.lng).subscribe((addresses) => {
        this.locationAddress = addresses[0].formatted_address;
        return this.locationAddress;
      });
    }
    return null;
  }

  onMarkerClicked(id) {
    const clickedMarker = this.locationService.markers[id];
    this.currentLocationId = id;
    this.updateLocationType();
    this.locationAddress = clickedMarker.address;
    this.lat = clickedMarker.lat;
    this.lng = clickedMarker.lng;
    this.hasLocation = true;
  }

  onMarkerAdded(marker: Marker) {
    this.onMarkerClicked(marker.id);
    this.manualMarkers.push({'id': marker.lat.toString() + marker.lng.toString(), 'lat': marker.lat, 'lng': marker.lng});
  }

  isLocationNew() {
    this.newLocation = true;
    const markers = this.locationService.markers;
    for (const id of this.recentLocationIds) {
      const marker = markers[id];
      if (marker.lat === this.lat && marker.lng === this.lng) {
        return this.newLocation = false;
      }
    }
  }

  validate() {
    this.updateLocationType();
    console.log('Location Type: ' + this.getLocationType());
    if (this.getLocationType() === 'SET_LATER') {
      this.locationAddress = null;
      this.lat = null;
      this.lng = null;
      this.hasLocation = false;
      this.newLocation = false;
    } else {
      this.isLocationNew();
    }
    return true;
  }

  searchLocation() {
    this.locationService.getLocationLatLng(this.searchString).subscribe((result) => {
      const res = result[0];
      this.lat = res.geometry.location.lat;
      this.lng = res.geometry.location.lng;
      this.locationAddress = res.formatted_address;
      const promise = this.locationService.addMarker(this.lat, this.lng, '', false, 0.5,
        'assets/offline-assets/icons/marker_red.png');
      promise.then(res => {
        const id = res[res.length - 1]['id'];
        this.currentLocationId = id;
        this.manualMarkers.push({'id': this.lat.toString() + this.lng.toString(), 'lat': this.lat, 'lng': this.lng});
        this.setLocationType(LocationType.MANUAL);
      });
    });
  }

  toggleDialog(gps = null) {
    const dialogRef = this.dialog.open(DialogDateAndLocationComponent, {
    data: { GPS: gps } }
    );
    document.body.classList.add('dialog-open');
    dialogRef.afterClosed().subscribe(result => {
      document.body.classList.remove('dialog-open');
      if (result) {
        this.initiateMap();
      }
    });
  }

}

@Component({
  selector: 'app-dialog-date-and-location',
  templateUrl: 'dialog-date-and-location.html',
})

export class DialogDateAndLocationComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogDateAndLocationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

