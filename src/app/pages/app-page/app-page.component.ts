import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-page',
  templateUrl: './app-page.component.html',
  styleUrls: ['./app-page.component.css']
})
export class AppPageComponent implements OnInit {
  navLinks = [
    { path: '/app/home', label: 'Home' },
    { path: '/app/logs', label: 'Logs' },
    { path: '/app/beedex', label: 'B-dex' },
    { path: '/app/tutorial', label: 'Tutorial' },
    { path: '/app/login', label: 'Account' },
  ];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  changeTab(goToPath) {
    const currentPath = this.router.url;
    for(let link of this.navLinks) {
      if(link.path === currentPath) {
        this.router.navigateByUrl(goToPath);
        return;
      }
    }
    if (window.confirm('If you switch to another page, your progress will be lost. Are you sure you want to switch to another page?')) {
      this.router.navigateByUrl(goToPath);
    }
  }
}
