import { Component, OnInit, Inject } from '@angular/core';
import { CheckUpdateService } from '../../services/check-update.service';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-app-landing-page',
  templateUrl: './app-landing-page.component.html',
  styleUrls: ['./app-landing-page.component.scss']
})
export class AppLandingPageComponent implements OnInit {

  constructor(private checkUpdateService: CheckUpdateService,
              private router: Router,
              public dialog: MatDialog,
              private cookieService: CookieService) { }

  ngOnInit() {
    this.checkUpdateService.checkForUpdate();
    this.toggleUpdateDialog();
  }

  getStarted() {
    this.router.navigateByUrl('/app/record').then();
  }

  viewTutorial() {
    this.router.navigateByUrl('/app/tutorial').then();
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView({behavior: 'smooth'});
  }

  toggleUpdateDialog() {
    const cookie_name = 'updatePopup5.0.0';
    const cookie = this.cookieService.get(cookie_name);
    let button;
    if (!cookie) {
      button = 'VIEW TUTORIAL';
    } else if (cookie === 'displayed') {
      button = 'DON\'T SHOW AGAIN';
    } else {
      return;
    }
    const dialogRef = this.dialog.open(DialogLandingPageUpdateComponent, { data: {'button': button}, autoFocus: false });
    document.body.classList.add('dialog-open');
    dialogRef.afterClosed().subscribe(result => {
      document.body.classList.remove('dialog-open');
      const expires = new Date();
      expires.setTime(expires.getTime() + (10 * 365 * 24 * 60 * 60 * 1000));
      if (result) {
        this.cookieService.set(cookie_name, 'stop', expires);
        if (!cookie) {
          this.viewTutorial();
        }
      } else if (!cookie) {
        this.cookieService.set(cookie_name, 'displayed', expires);
      }
    });
  }
}

@Component({
  selector: 'app-dialog-landing-page-update',
  templateUrl: 'dialog-landing-page-update.html',
})

export class DialogLandingPageUpdateComponent {
   constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
 }
