import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCaptureComponent } from './app-capture.component';

describe('AppCaptureComponent', () => {
  let component: AppCaptureComponent;
  let fixture: ComponentFixture<AppCaptureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppCaptureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppCaptureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
