import { Component, OnInit, OnDestroy, ElementRef, ViewChild, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FrameCaptureService } from '../../services/frame-capture.service';
import { Subscription } from 'rxjs';
import { MatSnackBar } from "@angular/material/snack-bar";
import { SnackBarInputBasic } from '../../models/app-snackbars';

declare var Hammer: any;

@Component({
  selector: 'app-app-capture',
  templateUrl: './app-capture.component.html',
  styleUrls: ['./app-capture.component.scss'],
})
export class AppCaptureComponent implements OnInit, OnDestroy, OnChanges {
  @Input() video: File;
  @Input() step;
  @Input() receivePause;
  @Input() captureEnabled = false;
  hasFrame = false;
  subscription: Subscription;
  mediaIcon = 'play_arrow';
  speedIcon = 'speed';
  duration = 1000000;
  currentTime: number = 0;
  currentSpeed = 100;
  checkboxValue: boolean;
  rotationMode = 0; // try to fix rotation and distortion
  frameAppear = false;

  wrapperWidth: number;
  wrapperHeight: number;
  videoWidth: number;
  videoHeight: number;
  v: HTMLVideoElement;
  @ViewChild('controller', {static: false})
  controller: ElementRef;
  verticalVideo = false;
  rotate = 0;

  adjustDeltaX = 0;
  adjustDeltaY = 0;
  distance: number;

  // Codepen
  minScale = 1;
  maxScale = 4;
  displayImageX = 0;
  displayImageY = 0;
  displayImageScale = 1;

  displayDefaultWidth: number;
  displayDefaultHeight: number;

  rangeX = 0;
  rangeMaxX = 0;
  rangeMinX = 0;

  rangeY = 0;
  rangeMaxY = 0;
  rangeMinY = 0;

  displayImageRangeY = 0;

  displayImageCurrentX = 0;
  displayImageCurrentY = 0;
  displayImageCurrentScale = 1;

  videoLoaded = false;
  image = {'initial': null, 'distorted': [null, null], 'chosen': null};
  currentTransform = null;
  checked = false;
  @Output() videoFrameEmitter = new EventEmitter();
  @Output() backEmitter = new EventEmitter();
  @Output() playStatusEmitter = new EventEmitter();

  constructor(private route: ActivatedRoute,
              private frameCaptureService: FrameCaptureService,
              private element: ElementRef,
              public snackBar: MatSnackBar) { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.video) {
      this.v = this.element.nativeElement.querySelector('#player');
      window.onorientationchange = this.onOrientationChange;
      const container = this.element.nativeElement.querySelector('#video-wrapper');
      const ham = new Hammer(container, {
        domEvents: true
      });
      this.v.src = URL.createObjectURL(this.video);

      container.addEventListener('wheel', e => {
        this.displayImageScale = this.displayImageCurrentScale = this.clampScale(this.displayImageScale + (e.wheelDelta / 800));
        this.updateRange();
        this.displayImageCurrentX = this.clamp(this.displayImageCurrentX, this.rangeMinX, this.rangeMaxX);
        this.displayImageCurrentY = this.clamp(this.displayImageCurrentY, this.rangeMinY, this.rangeMaxY);
        this.updateDisplayImage(this.displayImageCurrentX, this.displayImageCurrentY, this.displayImageScale);
      }, false);

      ham.get('pinch').set({ enable: true });
      ham.get('pan').set({ enable: true });

      ham.on('pan', ev => {
        this.displayImageCurrentX = this.clamp(this.displayImageX + ev.deltaX, this.rangeMinX, this.rangeMaxX);
        this.displayImageCurrentY = this.clamp(this.displayImageY + ev.deltaY, this.rangeMinY, this.rangeMaxY);
        this.updateDisplayImage(this.displayImageCurrentX, this.displayImageCurrentY, this.displayImageScale);
      });

      ham.on('pinch pinchmove', ev => {
        this.displayImageCurrentScale = this.clampScale(ev.scale * this.displayImageScale);
        this.updateRange();
        this.updateRange();
        this.displayImageCurrentX = this.clamp(this.displayImageX + ev.deltaX, this.rangeMinX, this.rangeMaxX);
        this.displayImageCurrentY = this.clamp(this.displayImageY + ev.deltaY, this.rangeMinY, this.rangeMaxY);
        this.updateDisplayImage(this.displayImageCurrentX, this.displayImageCurrentY, this.displayImageCurrentScale);
      });

      ham.on('panend pancancel pinchend pinchcancel', () => {
        this.displayImageScale = this.displayImageCurrentScale;
        this.displayImageX = this.displayImageCurrentX;
        this.displayImageY = this.displayImageCurrentY;
      });
    }
  }

  getFrame() {
    this.snackBar.openFromComponent(SnackBarInputBasic, { duration: 800, data: {'message': "Video frame captured.", 'icon': 'info'}, panelClass: ['sb-info']});
    this.capture();
    this.currentTransform = {'scale': this.displayImageCurrentScale, 'x': this.displayImageCurrentX, 'y': this.displayImageCurrentY, 'time': this.v.currentTime};
    this.sendVideoFrame();
  }

  private clamp(value, min, max) {
    return Math.min(Math.max(min, value), max);
  }

  private clampScale(newScale) {
    return this.clamp(newScale, this.minScale, this.maxScale);
  }

  private updateDisplayImage(x, y, scale) {
    const transform = 'translateX(' + x + 'px) translateY(' + y + 'px) scale(' + scale + ',' + scale + ')';
    this.v.style.transform = transform;
    this.v.style.webkitTransform = transform;
  }

  private updateScale(scale) {
    const transform = 'scale(' + scale + ',' + scale + ')';
    this.v.style.transform = transform;
    this.v.style.webkitTransform = transform;
  }

  // calculates range that a user should be able to move a scaled image
  private updateRange() {
    this.rangeX = Math.max(0, Math.round(this.displayDefaultWidth * this.displayImageCurrentScale) - this.wrapperWidth);
    this.rangeY = Math.max(0, Math.round(this.displayDefaultHeight * this.displayImageCurrentScale) - this.wrapperHeight);

    this.rangeMaxX = Math.round(this.rangeX / 2);
    this.rangeMinX = 0 - this.rangeMaxX;

    this.rangeMaxY = Math.round(this.rangeY / 2);
    this.rangeMinY = 0 - this.rangeMaxY;
  }

  toggleVideo() {
    this.duration = this.v.duration;
    this.currentTime = this.v.currentTime;
    if (this.v.paused) {
      this.v.play();
      this.mediaIcon = 'pause';
      this.playStatusEmitter.emit(true);
    } else {
      this.v.pause();
      this.mediaIcon = 'play_arrow';
      this.playStatusEmitter.emit(false);
    }
  }

  updateTime() {
    if (this.duration === 1000000) {
      this.duration = this.v.duration;
      this.currentTime = this.v.currentTime = (this.currentTime * this.duration) / 1000000;
    }
    this.currentTime = this.v.currentTime;
    if (this.v.paused || this.v.ended) this.mediaIcon = 'play_arrow';
    else this.mediaIcon = 'pause';
  }

  private onOrientationChange = (event: Event) => {
    if (this.videoLoaded === false) {
      return;
    }
    this.v.style['transform'] = '';
    const aspectr = this.v.videoWidth / this.v.videoHeight;
    const ratio = this.v.clientWidth / this.v.videoWidth;
    this.wrapperWidth = this.controller.nativeElement.clientWidth;
    this.wrapperHeight = this.wrapperWidth / aspectr;

    const whratio = this.wrapperWidth / this.wrapperHeight;
    if (aspectr < 1) {
      this.verticalVideo = true;
      this.wrapperHeight = 400;
      this.wrapperWidth = 400 * whratio;
    }
    this.videoWidth = this.v.videoWidth;
    this.videoHeight = this.v.videoHeight;
    this.displayDefaultWidth = this.wrapperWidth;
    this.displayDefaultHeight = this.wrapperHeight;
    this.rangeX = Math.max(0, this.displayDefaultWidth - this.wrapperWidth);
    this.rangeY = Math.max(0, this.displayDefaultHeight - this.wrapperHeight);
    return 0;
  }

  updateDimension(event) {
    this.videoLoaded = true;
    this.v.style['transform'] = '';
    const aspectr = event.target.videoWidth / event.target.videoHeight;
    const ratio = event.target.clientWidth / event.target.videoWidth;
    this.wrapperWidth = event.target.clientWidth;
    this.wrapperHeight = this.wrapperWidth / aspectr;

    /*
    const whratio = this.wrapperWidth / this.wrapperHeight;
    if (aspectr < 1) {
      this.verticalVideo = true;
      this.wrapperHeight = 400;
      this.wrapperWidth = 400 * whratio;
    }
    */
    this.videoWidth = event.target.videoWidth;
    this.videoHeight = event.target.videoHeight;
    this.displayDefaultWidth = this.wrapperWidth;
    this.displayDefaultHeight = this.wrapperHeight;
    this.rangeX = Math.max(0, this.displayDefaultWidth - this.wrapperWidth);
    this.rangeY = Math.max(0, this.displayDefaultHeight - this.wrapperHeight);
  }

  zoomIn() {
    this.displayImageScale = this.displayImageCurrentScale = this.clampScale(this.displayImageScale + 0.15);
    this.updateRange();
    this.displayImageCurrentX = this.clamp(this.displayImageCurrentX, this.rangeMinX, this.rangeMaxX);
    this.displayImageCurrentY = this.clamp(this.displayImageCurrentY, this.rangeMinY, this.rangeMaxY);
    this.updateDisplayImage(this.displayImageCurrentX, this.displayImageCurrentY, this.displayImageScale);
  }

  zoomOut() {
    if (this.displayImageScale <= 1.15) this.displayImageScale = this.displayImageCurrentScale = 1;
    else this.displayImageScale = this.displayImageCurrentScale -= 0.15;
    this.updateRange();
    this.displayImageCurrentX = this.clamp(this.displayImageCurrentX, this.rangeMinX, this.rangeMaxX);
    this.displayImageCurrentY = this.clamp(this.displayImageCurrentY, this.rangeMinY, this.rangeMaxY);
    this.updateDisplayImage(this.displayImageCurrentX, this.displayImageCurrentY, this.displayImageScale);
  }

  frameForward() {
    this.v.currentTime += 0.04;
  }

  frameBackward() {
    this.v.currentTime -= 0.04;
  }

  onInputChange(event) {
    this.pauseVideo();
    this.currentTime = this.v.currentTime = event.value;
  }

  onSpeedChange(event) {
    this.v.playbackRate = event.value;
  }

  formatLabel(value: number) {
    return value + 'x';
  }

  incrementAndCapture() {
    if (this.rotationMode == 2) {
      this.image['chosen'] = this.image['initial'];
      this.rotationMode = 0;
    }
    else {
      this.image['chosen'] = this.image['distorted'][this.rotationMode];
      this.rotationMode++;
    }
  }

  pauseVideo() {
    if (!this.v.paused) this.toggleVideo();
    this.playStatusEmitter.emit(false);
  }

  capture() {
    this.pauseVideo();
    this.frameAppear = true;
    const iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    function iOSversion() {
      if (/iP(hone|od|ad)/.test(navigator.platform)) {
        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
        const v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || '0', 10)];
      }
    }

    this.hasFrame = true;
    const canvas: HTMLCanvasElement = this.element.nativeElement.querySelector('#canvas');
    const context = canvas.getContext('2d');

    context.canvas.width = this.v.videoWidth;
    context.canvas.height = this.v.videoHeight;
    const ratio = this.v.videoHeight / this.v.clientHeight;
    context.save();

    context.transform(1, 0, 0, 1, this.displayImageX * ratio, this.displayImageY * ratio);
    context.transform(1, 0, 0, 1, this.v.videoWidth / 2, this.v.videoHeight / 2);
    context.transform(this.displayImageScale, 0, 0, this.displayImageScale, 0, 0);
    context.transform(1, 0, 0, 1, - this.v.videoWidth / 2, - this.v.videoHeight / 2);
    const r = this.v.videoHeight / this.v.videoWidth;
    if (this.verticalVideo && iOS && iOSversion()[0] >= 11) {
      context.translate(this.v.videoWidth, 0);
      context.rotate(90 * Math.PI / 180);
      context.scale(r, 1);
    }

    const isSamsungBrowser = navigator.userAgent.match(/SamsungBrowser/i);
    const isFirefoxBrowser = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

    if (this.verticalVideo && (isFirefoxBrowser || isSamsungBrowser)) {
      context.translate(this.v.videoWidth, 0);
      context.rotate(90 * Math.PI / 180);
      context.scale(r, 1 / r);
    }

    context.drawImage(this.v, 0, 0, this.v.videoWidth, this.v.videoHeight);
    context.restore();
    this.image['chosen'] = canvas.toDataURL();
    /*
    this.image['initial'] = canvas.toDataURL();
    this.image['chosen'] = this.image['initial'];

    context.translate(this.v.videoWidth, 0);
    context.rotate(90 * Math.PI / 180);

    context.scale(r, 1 / r);
    context.drawImage(this.v, 0, 0, this.v.videoWidth, this.v.videoHeight);
    context.restore();
    this.image['distorted'][0] = canvas.toDataURL();

    context.scale(r, 1);
    context.drawImage(this.v, 0, 0, this.v.videoWidth, this.v.videoHeight);
    context.restore();
    this.image['distorted'][1] = canvas.toDataURL();
    */
  }

  removeCapture() {
    const canvas: HTMLCanvasElement = this.element.nativeElement.querySelector('#canvas');
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    this.frameAppear = false;
    this.image = {'initial': null, 'distorted': [null, null], 'chosen': null};
    this.rotationMode = 0;
  }

  refreshCapture() {
    this.displayImageScale = this.displayImageCurrentScale = 1;
    this.displayImageX = this.displayImageCurrentX = this.displayImageY = this.displayImageCurrentY = 0;
    this.zoomOut();
  }

  back() {
    if (this.frameAppear) this.removeCapture();
    else {
      this.removeCapture();
      this.refreshCapture();
      this.backEmitter.emit();
      this.pauseVideo();
    }
  }

  sendVideoFrame() {
    this.videoFrameEmitter.emit({'image': this.image['chosen'], 'transform': this.currentTransform});
    //this.removeCapture();
    //this.refreshCapture();
  }

  complete() {
    const canvas: HTMLCanvasElement = this.element.nativeElement.querySelector('#canvas');
    const imageBlob = dataURItoBlob(canvas.toDataURL());
    const imageFile = new File([imageBlob], 'bee-frame');

    const reader = new FileReader();
    const imageEl = this.element.nativeElement.querySelector('#demo');
    reader.onload = (e: any) => {
      const src = e.target.result;
      imageEl.src = src;
      imageEl.onload = () => {
        const finalBlob = dataURItoBlob(canvas.toDataURL());
        const finalFile = new File([finalBlob], 'bee-frame');

        this.frameCaptureService.getImageSource(finalFile);
      };
    };

    reader.readAsDataURL(imageFile);

  }

  ngOnDestroy() {
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

}

function dataURItoBlob(dataURI) {
  // convert base64/URLEncoded data component to raw binary data held in a string
  let byteString;

  if (dataURI.split(',')[0].indexOf('base64') >= 0) {
    byteString = atob(dataURI.split(',')[1]);
  } else {
    byteString = dataURI.split(',')[1];
  }
  // separate out the mime component
  const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  // write the bytes of the string to a typed array
  const ia = new Uint8Array(byteString.length);
  for (let i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ia], { type: mimeString });
}
