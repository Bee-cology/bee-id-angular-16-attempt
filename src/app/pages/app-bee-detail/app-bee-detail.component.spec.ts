import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppBeeDetailComponent } from './app-bee-detail.component';

describe('AppBeeDetailComponent', () => {
  let component: AppBeeDetailComponent;
  let fixture: ComponentFixture<AppBeeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppBeeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppBeeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
