import { Component, OnInit, Input } from '@angular/core';

import { BeeSpecies, ReferenceService } from '../../api-module';

@Component({
  selector: 'app-app-bee-detail',
  templateUrl: './app-bee-detail.component.html',
  styleUrls: ['./app-bee-detail.component.scss']
})
export class AppBeeDetailComponent implements OnInit {
  @Input() bee: BeeSpecies;
  @Input() beedexId: string;
  @Input() detailsEnabled = true;

  constructor(private beeService: ReferenceService) {
  }

  async ngOnInit() {
    if (!this.bee) {
      this.bee = await this.beeService.getBee(this.beedexId).toPromise();
    }
  }
}
