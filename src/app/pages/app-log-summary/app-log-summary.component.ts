import { Component, OnChanges, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { BeeRecord, ReferenceService, ButterflyRecord, ButterflySpecies} from '../../api-module';
import { SpeciesNamePipe } from '../../pipes/species-name.pipe';

enum logType { PRE, POST, DB }

@Component({
  selector: 'app-app-log-summary',
  templateUrl: './app-log-summary.component.html',
  styleUrls: ['./app-log-summary.component.scss']
})
export class AppLogSummaryComponent implements OnChanges {
  @Output() editEmitter = new EventEmitter();
  @Input() beeLogPre; // bee log form before saving
  @Input() beeLogPost; // bee log form after saving
  @Input() beeLogDB: BeeRecord; // bee log form after submitting to the database

  @Input() butterLogPre; // copies for butterflies
  @Input() butterLogPost;
  @Input() butterLogDB: ButterflyRecord;

  @Input() images: string[];
  @Input() uploadVideo = false;
  @Input() canEditUploadVideo = false;
  @Input() sourceType = 'image';
  @Input() fileTooLarge = false;

  vowels = ['a', 'e', 'i', 'o', 'u'];

  beeName;
  beeGender;
  beeBehavior;
  beeBehaviors = ['Collecting nectar', 'Collecting pollen', 'Unknown'];
  beeBehaviorsDB = { 'nectar': 'Collecting nectar', 'pollen': 'Collecting pollen', 'unknown': 'Unknown' };

  // butterfly stuff
  butterflyFromDB: ButterflySpecies[];
  butterflyName;
  butterflyCommonName;
  butterflyFamily;
  butterflyBehavior;
  butterflyBehaviors = ['Nectaring', 'Puddling', 'Patrolling', 'Perching', 'Ovipositing', 'Mating', 'Unknown'];
  butterflyBehaviorsDB = {'nectar': 'Nectaring', 'puddling': 'Puddling', 'perching': 'Perching' , 'ovipositing': 'Ovipositing', 'mating': 'Mating', 'unknown': 'Unknown'};
  butterflySize;
  butterflyDorsalColors;
  butterflyVentralColors;
  butterflyPattern;
  butterflyTails;
  butterflyAntennae;
  butterflyShape;

  flowerName;
  flowerCommonName;
  flowerFamilyName;
  flowerShape;
  flowerLifeCycle;
  flowerColors;
  date;
  coordinates;
  address;
  userComments;

  constructor(private flowerListService: ReferenceService,
              private butterflyService: ReferenceService,
              private speciesNamePipe: SpeciesNamePipe) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.beeLogPre) {
      this.parseLog(changes.beeLogPre.currentValue, logType.PRE).then();
    } else if (changes.beeLogPost) {
      this.parseLog(changes.beeLogPost.currentValue, logType.POST).then();
    } else if (changes.beeLogDB) {
      this.parseLog(changes.beeLogDB.currentValue, logType.DB).then();
    }
    // else if (changes.butterLogPost) {
    //   this.parseLog(changes.butterLogPost.currentValue, logType.POST).then();
    // } else if (changes.butterLogDB) {
    //   this.parseLog(changes.butterLogDB.currentValue, logType.DB).then();
    // } else if (changes.butterLogPost) {
    //   this.parseLog(changes.butterLogPost.currentValue, logType.POST).then();
    // }
  }

  async getButterfly(value) {

    if (value !== null) {
      this.butterflyFromDB = await this.butterflyService.getButterflies([value]).toPromise();
    }

  }

  async parseLog(log, type: logType) {
    // shared
    if (type === logType.PRE) {
      this.flowerName = log.flowerName;
      this.flowerCommonName = log.flowerCommonName;
      this.flowerFamilyName = log.flowerFamilyName;
      this.flowerShape = log.flowerShape;
      this.flowerLifeCycle = log.flowerLifeCycle;
      this.flowerColors = log.flowerColor;
      this.date = log.date;
      this.coordinates = log.location;
      this.address = log.city;
      this.userComments = log.userComments; // added but may not want this here
      //this.userComments = log.userComments;

      if (log.beeCharacteristic) {
        this.beeName = log.beeCharacteristic.beeName;
        this.beeGender = log.gender;
        this.beeBehavior = this.beeBehaviors[log.beeBehavior];
      } else if (log.butterflyCharacteristic) {
        // useCase end of submission
        this.beeName = 'Butterfly'; // legacy
        this.butterflyName = log.butterflyCharacteristic.butterflyName;
        this.butterflyBehavior = this.butterflyBehaviors[log.butterflyBehavior];
        await this.getButterfly(this.butterflyName);
        // this.butterflyBehavior = log.butterflybehavior; // idk
        // console.log(this.butterflyBehavior);

        // old pre log fills with values from log
        /*this.butterflySize = log.wingSize; // working here
        this.butterflyFamily = log.butterflyCharacteristic.butterflyFamily;
        this.butterflyDorsalColors = (log.dorsalPrimaryColor === undefined) ? null : log.dorsalPrimaryColor.toString();
        // this.butterflyDorsalColors = log.dorsalPrimaryColor + log.dorsalSecondaryColor;
        this.butterflyVentralColors = (log.ventralPrimaryColor === undefined) ? null : log.ventralPrimaryColor.toString();
        // this.butterflyVentralColors = log.ventralPrimaryColor + log.ventralSecondaryColor;
        this.butterflyPattern = (log.dorsalPattern === undefined) ? null : log.dorsalPattern.toString();
        // this.butterflyPattern = log.dorsalPattern + log.ventralPattern;
        this.butterflyTails = log.tails;
        this.butterflyAntennae = log.antennae;
        this.butterflyShape = log.wingShape;*/

        // new log fills with values from db about species
        this.butterflyCommonName = this.butterflyFromDB[0].common_names[0];
        this.butterflySize = this.butterflyFromDB[0].wing_size; // working here
        this.butterflyFamily = log.butterflyCharacteristic.butterflyFamily;
        this.butterflyDorsalColors = this.butterflyFromDB[0].dorsalPrimaryColor.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyDorsalColors = this.butterflyFromDB[0].dorsalPrimaryColor.join(', ') + ', ' + this.butterflyFromDB[0].dorsalSecondaryColor.join(', ');
        this.butterflyVentralColors = this.butterflyFromDB[0].ventralPrimaryColor.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyVentralColors = this.butterflyFromDB[0].ventralPrimaryColor.join(', ') + ', ' + this.butterflyFromDB[0].ventralSecondaryColor.join(', ');
        this.butterflyPattern = this.butterflyFromDB[0].dorsalPattern.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyPattern = this.butterflyFromDB[0].dorsalPattern.join(', ') + ', ' + this.butterflyFromDB[0].ventralPattern.join(', ');
        this.butterflyTails = this.butterflyFromDB[0].tails;
        this.butterflyAntennae = this.butterflyFromDB[0].antennae;
        this.butterflyShape = this.butterflyFromDB[0].wing_shape;
      }

    } else if (type === logType.POST) {
      this.flowerName = log.fname;
      this.flowerCommonName = log.fcname;
      this.flowerFamilyName = log.ffname;
      this.flowerShape = log.fshape;
      this.flowerLifeCycle = log.lifecycle;
      this.flowerColors = log.fcolor;
      this.date = log.time;
      this.coordinates = log.loc;
      this.address = log.cityname;
      this.userComments = log.userComments; // added but may not want this here

      if (log.gender) { // this appears to be our bee/ butterfly differentiator

        this.beeName = log.beename;
        this.beeGender = log.gender;
        this.beeBehavior = this.beeBehaviors[log.beebehavior];
      } else { // if(log.butterflyName){
        this.beeName = 'Butterfly'; // legacy, but used to change log type
        this.butterflyName = log.butterflyname;
        await this.getButterfly(log.butterflyname);
        this.butterflyBehavior = this.butterflyBehaviors[log.butterflybehavior];
        this.butterflyCommonName = this.butterflyFromDB[0].common_names[0];
        this.butterflySize = this.butterflyFromDB[0].wing_size; // working here
        this.butterflyFamily = this.butterflyFromDB[0].family;
        this.butterflyDorsalColors = this.butterflyFromDB[0].dorsalPrimaryColor.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyDorsalColors = this.butterflyFromDB[0].dorsalPrimaryColor.join(', ') + ', ' + this.butterflyFromDB[0].dorsalSecondaryColor.join(', ');
        this.butterflyVentralColors = this.butterflyFromDB[0].ventralPrimaryColor.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyVentralColors = this.butterflyFromDB[0].ventralPrimaryColor.join(', ') + ', ' + this.butterflyFromDB[0].ventralSecondaryColor.join(', ');
        this.butterflyPattern = this.butterflyFromDB[0].dorsalPattern.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyPattern = this.butterflyFromDB[0].dorsalPattern.join(', ') + ', ' + this.butterflyFromDB[0].ventralPattern.join(', ');
        this.butterflyTails = this.butterflyFromDB[0].tails;
        this.butterflyAntennae = this.butterflyFromDB[0].antennae;
        this.butterflyShape = this.butterflyFromDB[0].wing_shape;

        // this.butterflySize = log.wingSize; // currently working?
        // this.butterflyFamily = (!log.butterflyfamily) ? null : log.butterflyCharacteristic.butterflyfamily;
        // this.butterflyDorsalColors = (log.dorsalPrimaryColor === undefined) ? null : log.dorsalPrimaryColor.join(', ');
        // this.butterflyDorsalColors = log.dorsalPrimaryColor + log.dorsalSecondaryColor;
        // this.butterflyVentralColors = (log.ventralPrimaryColor === undefined) ? null : log.ventralPrimaryColor.join(', ');
        // this.butterflyPattern = (log.dorsalPattern === undefined) ? null : log.dorsalPattern.join(', ');
        // this.butterflyPattern = log.dorsalPattern + log.ventralPattern;
        // this.butterflyTails = log.tails;
        // this.butterflyAntennae = log.antennae;
        // this.butterflyShape = log.wingShape;
      }


    } else if (type === logType.DB) {

      this.images = this.images.map(x => 'https://beecology.wpi.edu/' + x); // confirms media is image and adds url

      this.flowerName = log.flower_species ? this.speciesNamePipe.transform(log.flower_species) : null;
      this.flowerFamilyName = log.flower_species ? log.flower_species.family : null;
      this.flowerShape = log.flower_species ? log.flower_species.shape : null;
      this.flowerLifeCycle = log.flower_species ? log.flower_species.life_cycle : null;
      this.flowerColors = log.flower_species ? log.flower_species.colors : null;
      this.flowerCommonName = log.flower_species && log.flower_species.alt_names ? log.flower_species.alt_names.join(', ') : null;
      this.date = log.time;
      this.coordinates = `${log.location.latitude}, ${log.location.longitude}`;
      this.address = log.closest_city;
      this.userComments = log.user_comments;

      if (log.bee_species) { // another differentiator
        const r = log as BeeRecord;
        this.beeName = this.speciesNamePipe.transform(r.bee_species);
        this.beeGender = r.gender;
        this.beeBehavior = this.beeBehaviorsDB[r.behavior];
      } else {
        // useCase db
        // const rButterfly = log as ButterflyRecord;
        this.butterflyName = this.speciesNamePipe.transform(log.butterfly_species);
        this.butterflyCommonName = log.butterfly_species.common_names[0];
        this.beeName = 'Butterfly'; // legacy, but used to change log type
        this.butterflyBehavior = this.butterflyBehaviorsDB[log.butterfly_behavior];

        this.butterflySize = log.butterfly_species.wing_size; // working here
        this.butterflyFamily = log.butterfly_species.family;
        this.butterflyDorsalColors = log.butterfly_species.dorsalPrimaryColor.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyDorsalColors = this.butterflyFromDB[0].dorsalPrimaryColor.join(', ') + ', ' + this.butterflyFromDB[0].dorsalSecondaryColor.join(', ');
        this.butterflyVentralColors = log.butterfly_species.ventralPrimaryColor.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyVentralColors = this.butterflyFromDB[0].ventralPrimaryColor.join(', ') + ', ' + this.butterflyFromDB[0].ventralSecondaryColor.join(', ');
        this.butterflyPattern = log.butterfly_species.dorsalPattern.join(', ');
        // tslint:disable-next-line:max-line-length
        // this.butterflyPattern = this.butterflyFromDB[0].dorsalPattern.join(', ') + ', ' + this.butterflyFromDB[0].ventralPattern.join(', ');
        this.butterflyTails = log.butterfly_species.tails;
        this.butterflyAntennae = log.butterfly_species.antennae;
        this.butterflyShape = log.butterfly_species.wing_shape;
        // this.butterflySize = log.wing_size; // currently working?
        // this.butterflyDorsalColors = (log.dorsalPrimaryColor === undefined) ? null : log.dorsalPrimaryColor.join(', ');
        // this.butterflyVentralColors = (log.ventralPrimaryColor === undefined) ? null : log.ventralPrimaryColor.join(', ');
        // this.butterflyPattern = (log.dorsalPattern === undefined) ? null : log.dorsalPattern.join(', ');
        // this.butterflyTails = log.tails;
        // this.butterflyAntennae = log.antennae;
        // this.butterflyShape = log.wing_shape;
        // wondering if need to do same thing for all enum?
        // what about other fields also not sure where this code is run anyway
        // this.butterflySize = this.butterflySize[log.wing_size];
      }
    }

    if (this.flowerColors && this.flowerColors !== 'SET LATER') {
      this.flowerColors = this.flowerColors.join(', ');
    }
    if (this.uploadVideo == null) {
      this.uploadVideo = false;
    }

  }

  // checks if the flower name starts with a vowel
  flowerVowel() {
    return this.vowels.indexOf(this.flowerName.charAt(0).toLowerCase());
  }

  isInvalid(value: String) {
    if (value) {
      // value = value.toLowerCase();
      return value === 'null' || value === 'set later' || value === 'unknown' || value === 'SET LATER' || value === 'Unknown';
    } else {
      return true;
    }
  }
  capitilize(value: String) {
    if (value) {
      value = value.charAt(0).toUpperCase() + value.slice(1);
    }
    return value;
  }
  isUnknown(value: String) {
    if (value) {
      return value.toLowerCase() === 'unknown';
    }
    return true;
  }

  editUploadVideo() {
    this.editEmitter.emit();
  }
}
