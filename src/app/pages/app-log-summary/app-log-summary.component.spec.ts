import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLogSummaryComponent } from './app-log-summary.component';

describe('AppLogSummaryComponent', () => {
  let component: AppLogSummaryComponent;
  let fixture: ComponentFixture<AppLogSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLogSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLogSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
