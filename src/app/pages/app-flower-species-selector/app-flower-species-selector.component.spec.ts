import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppFlowerSpeciesSelectorComponent } from './app-flower-species-selector.component';

describe('AppFlowerSpeciesSelectorComponent', () => {
  let component: AppFlowerSpeciesSelectorComponent;
  let fixture: ComponentFixture<AppFlowerSpeciesSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppFlowerSpeciesSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppFlowerSpeciesSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
