import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SnackBarInputBasic } from '../../models/app-snackbars';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FlowerSpecies, ReferenceService } from '../../api-module';
import { SpeciesNamePipe } from '../../pipes/species-name.pipe';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-app-flower-species-selector',
  templateUrl: './app-flower-species-selector.component.html',
  styleUrls: ['./app-flower-species-selector.component.scss']
})
export class AppFlowerSpeciesSelectorComponent implements OnInit {
  @Input() canSkip = true; // whether flower species can be set later
  disabled = false; // variable to replace flowerControl.enabled which doesn't update properly

  flowerControl = new FormControl();
  flowerList: FlowerSpecies[];
  filteredFlowerList: Observable<FlowerSpecies[]>;
  data: any;
  includeUnknown = true;

  constructor(private flowerListService: ReferenceService,
              private speciesNamePipe: SpeciesNamePipe,
              public titleCasePipe: TitleCasePipe,
              public snackBar: MatSnackBar) {
  }

  async ngOnInit() {
    this.flowerList = await this.flowerListService.getFlowers().toPromise();
    this.flowerList.sort(function(a, b) { return (a.common_name > b.common_name) ? 1 : -1; });
    this.filteredFlowerList = this.flowerControl.valueChanges.pipe(
      startWith(''),
      map(value => this.flowerFilter(value))
    );
  }

  async refreshFlowerList(){
       this.flowerList = await this.flowerListService.getFlowers().toPromise();
    this.flowerList.sort(function(a, b) { return (a.common_name > b.common_name) ? 1 : -1; });
    this.filteredFlowerList = this.flowerControl.valueChanges.pipe(
      startWith(''),
      map(value => this.flowerFilter(value))
    );

  }

  // filters flowerList based on flowerControl.value
  private flowerFilter(value: string): FlowerSpecies[] {
    if (value !== null) {
      value = value.toLowerCase() as string;
      this.includeUnknown = 'unknown'.includes(value);
      return this.flowerList.filter(flower => {
        return this.speciesNamePipe.transform(flower).toLowerCase().includes(value) ||
          flower.alt_names.join(' ').toLowerCase().includes(value);
      });
    }
  }

  // toggles set later option, called by SET LATER and SET NOW button
  disable() {
    this.disabled = !this.disabled;
    if (this.disabled) {
      this.flowerControl.disable();
      this.flowerControl.setValue('');
    } else {
      this.flowerControl.enable();
    }
  }

  validate(): boolean {
    if (this.disabled) {
      this.data = {
        'flowerName': 'SET LATER',
        'flowerFamilyName': 'SET LATER',
        'flowerCommonName': 'SET LATER',
        'flowerId': 'SET LATER',
        'flowerColor': 'SET LATER',
        'flowerShape': 'SET LATER',
        'flowerLifeCycle': 'SET LATER'
      };
    } else if (this.flowerControl.value) {
      const matched = this.flowerList.find(x => x.common_name === this.flowerControl.value.toLowerCase());
      if (matched) {
        this.data = {
          'flowerName': this.speciesNamePipe.transform(matched),
          'flowerFamilyName': matched.family,
          'flowerCommonName': this.titleCasePipe.transform(matched.alt_names.join(', ')),
          'flowerColor': matched.colors,
          'flowerShape': matched.shape,
          'flowerLifeCycle': matched.life_cycle
        };
      } else if (this.flowerControl.value === 'Unknown') {
        this.data = {
          'flowerName': null,
          'flowerFamilyName': null,
          'flowerCommonName': null,
          'flowerColor': null,
          'flowerShape': null,
          'flowerLifeCycle': null
        };
      } else { // invalid flower value
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 2000,
          data: { 'messageKey': 'error-flower-name', 'icon': 'error' },
          panelClass: ['sb-error']
        });
        return false;
      }
    } else { // no flower value
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: { 'messageKey': 'error-field-input', 'icon': 'error' },
        panelClass: ['sb-error']
      });
      return false;
    }
    return true;
  }

  setValue(value) {
    this.flowerControl.setValue(value);
  }

  getValue() {
    return this.flowerControl.value;
  }

}
