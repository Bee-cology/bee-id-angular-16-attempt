import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppResetpasswordComponent } from './app-resetpassword.component';

describe('AppResetpasswordComponent', () => {
  let component: AppResetpasswordComponent;
  let fixture: ComponentFixture<AppResetpasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppResetpasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppResetpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
