import { Component, OnInit } from '@angular/core';
import { BeecologyAuthService } from '../../services/authentication/beecology-auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarInputBasic } from '../../models/app-snackbars';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-resetpassword',
  templateUrl: './app-resetpassword.component.html',
  styleUrls: ['./app-resetpassword.component.css']
})
export class AppResetpasswordComponent implements OnInit {

  emailAddress = '';

  constructor(private auth: BeecologyAuthService,
              private snackBar: MatSnackBar,
              private router: Router) { }

  ngOnInit() {
  }

  submit() {
    this.auth.resetPassword(this.emailAddress).then(res => {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: {'messageKey': 'success-password-reset', 'icon': 'success'},
        panelClass: ['sb-success']});
      this.router.navigate(['/app/login']).then();
    }, err => {
      let messageKey;
      if (err.code === 'auth/invalid-email') {
        messageKey = 'error-account-email-invalid';
      } else if (err.code === 'auth/user-not-found') {
        messageKey = 'error-account-email-unknown';
      }
      this.snackBar.openFromComponent(SnackBarInputBasic, { duration: 2000, data: {'messageKey': messageKey, 'icon': 'error'}, panelClass: ['sb-error']});
    });
  }

}
