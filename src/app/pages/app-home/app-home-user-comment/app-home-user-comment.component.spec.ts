import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHomeUserCommentComponent } from './app-home-user-comment.component';

describe('AppHomeUserCommentComponent', () => {
  let component: AppHomeUserCommentComponent;
  let fixture: ComponentFixture<AppHomeUserCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHomeUserCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHomeUserCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {//
    expect(component).toBeTruthy();
  });
});
