import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BeeLog } from '../../../models/bee-log.model';
import { ButterflyLog } from '../../../models/butterfly-log.model';
import { BeelogService } from '../../../services/beelog.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-app-home-user-comment',
  templateUrl: './app-home-user-comment.component.html',
  styleUrls: ['./app-home-user-comment.component.css']
})
export class AppHomeUserCommentComponent implements OnInit {
  @Input() source;
  @Output() nextEmitter = new EventEmitter<any>();
  @Output() backEmitter = new EventEmitter<any>();
  @ViewChild('videoPlayer', {static: false}) videoPlayer;
  @ViewChild('flowerSpeciesSelector', {static: false}) flowerSpeciesSelector;

  beeLog: BeeLog;
  butterflyLog: ButterflyLog;
  beeBehaviors = ['Collecting nectar', 'Collecting pollen', 'Unknown'];
  playStatus;
  imageData;
  commentControl = new FormControl();

  constructor(
    private beelogService: BeelogService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog) {
    this.beeLog = beelogService.beeLog;
    this.butterflyLog = beelogService.butterflyLog;
  }

  async ngOnInit() {
    this.commentControl.setValue(this.beeLog.userComments);
  }

  next() {


    //this.beeLog.userComments = this.commentControl.value;
    this.butterflyLog.userComments = this.beeLog.userComments;
    this.nextEmitter.emit(null);
  }

  back() {
    this.backEmitter.emit();
  }
}
