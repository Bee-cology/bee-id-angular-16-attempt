import { Component, Output, EventEmitter } from '@angular/core';
import { BeeLog } from '../../../models/bee-log.model';
import { ButterflyLog } from '../../../models/butterfly-log.model';
import { BeelogService } from '../../../services/beelog.service';

@Component({
  selector: 'app-app-home-source-upload',
  templateUrl: './app-home-source-upload.component.html',
  styleUrls: ['./app-home-source-upload.component.scss']
})

export class AppHomeSourceUploadComponent {
  @Output() nextEmitter = new EventEmitter<any>();
  beeLog: BeeLog;
  butterflyLog: ButterflyLog;

  constructor(private beelogService: BeelogService) {
    this.beeLog = beelogService.beeLog;
    this.butterflyLog = beelogService.butterflyLog;
  }

  next(type: string, file: File, specimen: string) {
    if (type == null) {
      this.nextEmitter.emit();
    } else {
      this.nextEmitter.emit({'type': type, 'file': file, 'specimen': specimen});
    }
  }

  inputVideoHandler(video) {
    this.next('video', video.file, video.specimen);
  }

  inputImageHandler(image) {
    this.next('image', image.base64, image.specimen);
  }
}
