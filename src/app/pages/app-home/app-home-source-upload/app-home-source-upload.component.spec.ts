import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHomeBasicInfoComponent } from './app-home-image-upload.component';

describe('AppHomeBasicInfoComponent', () => {
  let component: AppHomeBasicInfoComponent;
  let fixture: ComponentFixture<AppHomeBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHomeBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHomeBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
