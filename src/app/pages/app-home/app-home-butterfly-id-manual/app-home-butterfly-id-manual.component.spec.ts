import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {AppHomeButterflyIdManualComponent} from './app-home-butterfly-id-manual.component';

describe('AppHomeButterflyIdManualComponent', () => {
  let component: AppHomeButterflyIdManualComponent;
  let fixture: ComponentFixture<AppHomeButterflyIdManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHomeButterflyIdManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHomeButterflyIdManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
