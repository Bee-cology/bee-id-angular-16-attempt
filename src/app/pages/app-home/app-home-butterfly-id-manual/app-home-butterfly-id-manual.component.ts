import { ButterflyLog } from './../../../models/butterfly-log.model';
import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  OnInit,
  HostListener
} from '@angular/core';
import { BeelogService } from '../../../services/beelog.service';
// tslint:disable-next-line:max-line-length
import {DialogGuidedButterflyIdentificationComponent, DialogTailsGuideComponent, DialogAntennaeGuideComponent, DialogPatternGuideComponent, DialogShapeGuideComponent} from '../app-home.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarInputBasic } from '../../../models/app-snackbars';
import { ButterflySpecies, ReferenceService } from '../../../api-module';
import { NetworkStatusService } from '../../../services/network-status.service';
import { PageEvent } from '@angular/material/paginator';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-app-home-butterfly-id-manual',
  templateUrl: './app-home-butterfly-id-manual.component.html',
  styleUrls: ['./app-home-butterfly-id-manual.component.scss']
})
export class AppHomeButterflyIdManualComponent implements OnInit {

  constructor(
    private butterflylogService: BeelogService,
    private butterflyService: ReferenceService,
    private networkStatus: NetworkStatusService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    public titleCasePipe: TitleCasePipe) {
    this.butterflyLog = butterflylogService.butterflyLog;
  }
  @Input() source;
  @Output() nextEmitter = new EventEmitter<any>();
  @Output() backEmitter = new EventEmitter<any>();
  @ViewChild('videoPlayer', {static: false}) videoPlayer;
  @ViewChild('butterflySpeciesSelector', {static: false}) butterflySpeciesSelector;
  butterflyLog: ButterflyLog;
  butterflyBehaviors = ['Puddling', 'Nectaring'];
  playStatus;
  imageData;
  imageData2;
  butterflyManualStepId = 0;
  lowerButterflyName: string;
  responseData;
  resizedImage;
  suggestedText;
  butterflies: ButterflySpecies[];
  allButterflies: ButterflySpecies[];
  tempButterflies = [];
  haveAllButt = false;
  butterfliesLimited: ButterflySpecies[];
  butterfliesDisplay: ButterflySpecies[];
  selectedWingShape = null;
  wingShapes = [
    {value: null, viewValue: 'Unknown'},
    {value: 'Common', viewValue: 'Common'},
    {value: 'Hairstreak', viewValue: 'Hairstreak'},
    {value: 'Skipper', viewValue: 'Skipper'},
    {value: 'Longtail', viewValue: 'Swallowtail'}, // oops
    {value: 'Unique', viewValue: 'Unique'}
  ];
  selectedDorsalPColor = '';
  selectedVentralPColor = '';
  listColors = [
    {value: 'Orange', viewValue: 'Orange'},
    {value: 'Black', viewValue: 'Black'},
    {value: 'Brown', viewValue: 'Brown'},
    {value: 'Yellow', viewValue: 'Yellow'},
    {value: 'Green', viewValue: 'Green'},
    {value: 'Red', viewValue: 'Red'},
    {value: 'Purple', viewValue: 'Purple'},
    {value: 'Grey', viewValue: 'Grey'},
    {value: 'Copper', viewValue: 'Copper'},
    {value: 'White', viewValue: 'White'},
    {value: 'Blue', viewValue: 'Blue'}
  ];
  listColorsV = [
    {value: 'Orange-h', viewValue: 'Orange'},
    {value: 'Black-h', viewValue: 'Black'},
    {value: 'Brown-h', viewValue: 'Brown'},
    {value: 'Yellow-h', viewValue: 'Yellow'},
    {value: 'Green-h', viewValue: 'Green'},
    {value: 'Red-h', viewValue: 'Red'},
    {value: 'Purple-h', viewValue: 'Purple'},
    {value: 'Grey-h', viewValue: 'Grey'},
    {value: 'Copper-h', viewValue: 'Copper'},
    {value: 'White-h', viewValue: 'White'},
    {value: 'Blue-h', viewValue: 'Blue'}
  ];
  selectedTails = null;
  tails = [
    {value: null, viewValue: 'Unknown'},
    {value: 'Yes', viewValue: 'Yes'},
    {value: 'No', viewValue: 'No'}
  ];
  selectedAntennae = null;
  antennaes = [
    {value: null, viewValue: 'Unknown'},
    {value: 'clubbed', viewValue: 'Clubbed'},
    {value: 'hooked', viewValue: 'Hooked'},
    {value: 'combed', viewValue: 'Combed'}
  ];
  selectedSize = null;
  sizes = [
    {value: null, viewValue: 'Unknown'},
    {value: 'Small', viewValue: 'Small (Less than 2.5 in)'},
    {value: 'Large', viewValue: 'Large (Greater than 2.5 in)'}
  ];
  selectedPattern = '';
  selectedPatternV = '';
  patterns = [
    {value: null, viewValue: 'Unknown'},
    {value: 'Dark Spots', viewValue: 'Dark Spots'},
    {value: 'Bright Spots', viewValue: 'Bright Spots'},
    {value: 'Striped', viewValue: 'Striped'},
    {value: 'Eyespots', viewValue: 'Eyespots'},
    {value: 'Mottled', viewValue: 'Mottled'},
    {value: 'Veins', viewValue: 'Veins'},
    {value: 'Checkered', viewValue: 'Checkered'},
    {value: 'Iridescent', viewValue: 'Iridescent'},
    {value: 'Camoflauge', viewValue: 'Camoflauge'},
    {value: 'Frosted', viewValue: 'Frosted'}
  ];
  colorList: string[];
  speciesArray = null;
  familyArray = null;
  dorsalPatternArray = null;
  ventralPatternArray = null;
  dorsalPrimaryColorArray = null;
  dorsalSecondaryColorArray = null;
  ventralPrimaryColorArray = null;
  ventralSecondaryColorArray = null;
  numButterflies = 0;
  belowVideo = false;
  stopFlicker = false;
  getSuggestSwitch = false;
  numFilters = 0; // number of filters applied
  pageNumber = 0; // page number
  iNaturalistRoute = false; // marker to know if user chose iNaturalist or not
  iNaturalistSearchLimit = 0; // not the limit but the counter for search limit
  searchLimitTriggered = false; // checks to see if the search limit has been triggered
  iNatReturned = false; // used with globaliNatReturned to check if iNaturalist call has been returned before we throw errors
  butterfliesPerPage = 4;
  butterflyPages;
  isOnline = true;
  ccClicked = true;
  imgOwner;
  copyRightType;
  copyRightLink;
  totalPages = 20; // need to set on init / resize and again in get butterflies
  butterfliesResponseDB = [];
  dataIsLoaded = false;
  overlayButterfly: ButterflySpecies;





  /**
   * init... sets online status, listener for butterfliesPerPage and calls get butterflies
   */
  async ngOnInit() {

    // Check initial network status
    this.networkStatus.checkOnline().then((isOnline) => {
      this.isOnline = isOnline;
    });
    // Set up network status monitoring
    this.networkStatus.isOnline.subscribe(() => {
      console.log('now online');
      this.isOnline = true;
    });
    this.networkStatus.isOffline.subscribe(() => {
      console.log('now offline');
      this.isOnline = false;
    });
    await this.getButterflies();
    this.colorList = new Array<string>();

    window.addEventListener('resize', () => {
      const windowWidth = window.innerWidth - 30; // need to change padding not sure how to find that number
      let bfp = 1;
      while ((bfp + (bfp - 1) * (12 + 150)) < windowWidth) {
        bfp++;
      }
      if (((bfp - 2) * 2) >= 6) {
        this.butterfliesPerPage = 6;
      } else {
        this.butterfliesPerPage = (bfp - 2) * 2;
      }

      if (this.butterflies) {
        this.butterflyPages = this.butterflies.filter((_b, i) => i % this.butterfliesPerPage === 0);
      }
      this.totalPages =  this.butterflies.length / this.butterfliesPerPage;
      this.pageFunc(0);
    });

    this.totalPages =  this.butterflies.length / this.butterfliesPerPage;

  }


  /**
   * Gets all butterflies from db
   */
  async getButterflies() {
    this.numFilters = 0;
    if (this.dorsalPatternArray) { this.numFilters++; }
    if (this.ventralPatternArray) { this.numFilters++; }
    if (this.dorsalPrimaryColorArray) { this.numFilters++; }
    if (this.ventralPrimaryColorArray) { this.numFilters++; }
    if (this.selectedAntennae) { this.numFilters++; }
    if (this.selectedWingShape) {this.numFilters++; }
    if (this.selectedSize) { this.numFilters++; }
    if (this.selectedTails) {this.numFilters++; }
    // tslint:disable-next-line:max-line-length
    this.butterflies = await this.butterflyService.getButterflies(this.speciesArray, this.familyArray, this.dorsalPatternArray, this.ventralPatternArray, this.dorsalPrimaryColorArray, this.dorsalSecondaryColorArray, this.ventralPrimaryColorArray, this.ventralSecondaryColorArray, this.selectedWingShape, this.selectedSize, this.selectedTails, this.selectedAntennae).toPromise();
    console.log('Butterflies returned from search are: ');
    console.log(this.butterflies);
    this.numButterflies = this.butterflies.length;
    this.butterfliesDisplay = this.butterflies.slice(0, this.butterfliesPerPage);
    this.butterflyPages = this.butterflies.filter((_b, i) => i % this.butterfliesPerPage === 0);
    if (!this.haveAllButt) {
      this.allButterflies = this.butterflies;
      butterfliesOutside = this.butterflies;
      this.haveAllButt = true;
    }
    this.butterfliesLimited = this.butterflies.slice(0, 10);

  }

  /**
   * Goes to straight to iNaturalist page... checks if online first
   */
  goToiNaturalist() {
    if (this.isOnline) {
      this.butterflyManualStepId = 2;
      this.iNaturalistRoute = true;

    } else {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 3000,
        data: {'messageKey': 'not-connected-to-internet', 'icon': 'error'},
        panelClass: ['sb-error']});
    }

  }


  /**
   * Goes to Manual id Page
   */
  async goToManualId () {
    this.butterflyManualStepId = 1;
    if (this.butterflies.length !== 0) {
      this.overlayButterfly = this.butterflies[0];
    }
    // sets butterfly per page # before manual id is loaded
    const windowWidth = window.innerWidth - 30; // need to change padding not sure how to find that number
    let bfp = 1;
    while ((bfp + (bfp - 1) * (12 + 150)) < windowWidth) {
      bfp++;
    }
    if (((bfp - 2) * 2) >= 6) {
      this.butterfliesPerPage = 6;
    } else {
      this.butterfliesPerPage = (bfp - 2) * 2;
    }

    if (this.butterflies) {
      this.butterflyPages = this.butterflies.filter((_b, i) => i % this.butterfliesPerPage === 0);
    }
    this.totalPages =  this.butterflies.length / this.butterfliesPerPage;
    this.pageFunc(0);

  }

  /**
   * Handles next button
   */
  next() {
    console.log('next called');
    if (!this.playing() && !(this.source.type === 'video' && this.hasFrame()) && this.validate()) {

      if (this.source.type === 'video' &&
      ((this.butterflySpeciesSelector.data.butterflyname == null) || (this.butterflySpeciesSelector.data.butterflyname === 'SET LATER'))
        && this.butterflyManualStepId === 0) {

        this.butterflyManualStepId = 1;
      } else if (this.butterflyManualStepId === 1) {
        // this.butterflyManualStepId = 2;
        this.nextEmitter.emit(this.imageData);
      } else {
        this.nextEmitter.emit(this.imageData);
      }
    }
  }



  /**
   * Handles back button
   */
  back() {
    this.overlayButterfly = null;
    if (this.butterflyManualStepId === 1) {
      this.butterflyManualStepId = 0;
    } else if (this.butterflyManualStepId === 2) {
      this.butterflyManualStepId = 0;
    } else {
      this.backEmitter.emit();
    }
  }

  toggleDialog(auto) {
    this.dialog.open(DialogGuidedButterflyIdentificationComponent, {
      data: { video: this.source.type === 'video',
              auto: auto } }
      );
  }

  playing() {
    if (this.playStatus) {
      this.toggleDialog(true);
      this.videoPlayer.pauseVideo();
      return true;
    }
    return false;
  }

  /**
   * Checks if user has captured an image
   */
  hasFrame() {
      if (this.imageData == null) {
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 3000,
          data: {'messageKey': 'error-butterfly-frame', 'icon': 'error'},
          panelClass: ['sb-error']});
        return true;
      }
      return false;
    }

  saveFrame(value) {
    this.imageData = value;
  }

  validate() {

    // this checks if filter values have been selected and adds them to the log
    if (this.selectedSize) {
      this.butterflyLog.wingSize = this.selectedSize;
    }

    if (this.selectedWingShape) {
      this.butterflyLog.wingShape = this.selectedWingShape;
    }

    if (this.selectedTails) {
      this.butterflyLog.tails = this.selectedTails;
    }

    if (this.selectedAntennae) {
      this.butterflyLog.antennae = this.selectedAntennae;
    }

    if (this.dorsalPrimaryColorArray) {
        this.butterflyLog.dorsalPrimaryColor = this.dorsalPrimaryColorArray;
    }

    if (this.ventralPrimaryColorArray) {
      this.butterflyLog.ventralPrimaryColor = this.dorsalPrimaryColorArray;
    }

    if (this.dorsalPatternArray) {
      this.butterflyLog.dorsalPattern = this.dorsalPatternArray;
    }

    if (this.butterfliesResponseDB.length > 0) {
      this.butterflyLog.speciesGuessArray = [];
      for (let i = 0; i < this.butterfliesResponseDB.length; i++) {
        this.butterflyLog.speciesGuessArray.push(this.butterfliesResponseDB[i]);
      }
      this.butterflyLog.speciesTopGuess = this.butterfliesResponseDB[0];
    }


    if (this.butterflySpeciesSelector.validate()) {

      const data = this.butterflySpeciesSelector.data;
      console.log(data);
      this.butterflyLog.butterflyCharacteristic.butterflyName = data.butterflyname;
      this.butterflyLog.butterflyCharacteristic.butterflyFamily = data.butterflyfamily;

      return true;
    } else {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 3000,
        data: {'messageKey': 'error-field-input', 'icon': 'error'},
        panelClass: ['sb-error']});
      return false;
    }
  }

  overlayOn(butterfly) {
    this.overlayButterfly = butterfly;
    const elementTarget = document.getElementById('overlay');
    window.scrollTo({top: elementTarget.offsetTop, behavior: 'smooth'});
  }


  // on scroll listening for passing videoplayer
  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    let offsetSize = 525;
    if (window.innerWidth >= 600) {
      offsetSize = 700;
    } else {
      offsetSize = 700;
    }
    if (this.butterflyManualStepId === 1 || this.butterflyManualStepId === 2) {
      const elementTarget = document.getElementById('videoPlayer');
      // if (window.scrollY > (elementTarget.offsetTop + elementTarget.offsetHeight)) {
      if (window.scrollY >= (elementTarget.offsetTop + offsetSize)) {
        if (!this.stopFlicker) {
          // capture image/state of video to be used in top right
          if (this.hasFrame()) {
            this.snackBar.openFromComponent(SnackBarInputBasic, {
              duration: 3000,
              data: {'messageKey': 'capture-image-for-fxn', 'icon': 'error'},
              panelClass: ['sb-error']
            });
          } else {
            this.stickyImg();
          }

          this.stopFlicker = true;
        }
      }
      if (window.scrollY < (elementTarget.offsetTop + offsetSize)) {
        if (this.stopFlicker && this.imageData) {
          this.stopFlicker = false;
          document.getElementById('stickyDiv').style.display = 'none';
        }
      }
    }
  }

  stickyImg() {
    if (this.imageData) {
      document.getElementById('stickyDiv').style.display = 'block';
      document.getElementById('stickyImg').setAttribute('src', this.imageData.image);
    }
  }

  getSuggest() {

    globaliNatReturned = false;
    this.iNatReturned = false;
    if (this.hasFrame()) {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 3000,
        data: {'messageKey': 'capture-image-for-iNat', 'icon': 'error'},
        panelClass: ['sb-error']});
    } else {

      // checks if you've reached your search limit already
      if (this.iNaturalistSearchLimit < 2) {

        if (this.iNaturalistSearchLimit === 1) {
          this.searchLimitTriggered = true;
        }

        // checks to make sure the image you are submitting is a new image
        if (this.imageData !== this.imageData2) { // checks if iNaturalist has been run on an image

          this.iNaturalistSearchLimit++;
          this.getSuggestSwitch = true;
          const strSuggs = this.resizeSendImage();
          console.log(strSuggs);
          this.suggestedText = strSuggs;

          this.imageData2 = this.imageData;
        } else {
          this.snackBar.openFromComponent(SnackBarInputBasic, {
            duration: 3000,
            data: {'messageKey': 'capture-new-image', 'icon': 'error'},
            panelClass: ['sb-error']});
        }
      } else {

        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 3000,
          data: {'messageKey': 'out-of-iNat-queries', 'icon': 'error'},
          panelClass: ['sb-error']});
      }

    }
  }

  resizeSendImage() {
    const i = new Image();

    i.onload = function() {
      console.log( 'Original image size: ' + i.width + ', ' + i.height );
    };

    // i.src = this.source.file;
    i.src = this.imageData.image; // using image rather than video
    let newWidth = 300;
    let newHeight = 300;

    if (i.width > i.height) {

      newHeight = Math.round(300 * (i.height / i.width));
    } else if (i.width < i.height) {

      newWidth = Math.round(300 * (i.width / i.height));

    }

    console.log( 'Modified image size: ' + newWidth + ', ' + newHeight );

    // compressImage( this.source.file, newWidth, newHeight ).then( (blob:Blob )=> {
    compressImage( this.imageData.image, newWidth, newHeight ).then( (blob: Blob ) => {
      const data = new FormData( );

      data.append( 'image', blob);

      // data.append( 'taxon_id', '47126'); // looking for any plant
      // data.append( 'taxon_id', '47125'); // looking for a flower
      data.append( 'taxon_id', '47157'); // looking for a butterfly
      // tslint:disable-next-line:max-line-length
      // taxon_id can be found at https://www.inaturalist.org/taxa by searching for the taxa of interest. The id is a five digit number in the URL

      const xhr = new XMLHttpRequest();
      xhr.onload = () => this.readResponse(xhr.responseText);

      xhr.open( 'POST', 'https://visionapi.p.rapidapi.com/v1/rapidapi/score_image' );
      xhr.setRequestHeader( 'Access-Control-Allow-Origin', 'http://localhost:4200' );
      xhr.setRequestHeader( 'x-rapidapi-host', 'visionapi.p.rapidapi.com' );
      xhr.setRequestHeader( 'x-rapidapi-key', 'b7ddc20751msh765abd135dc9500p125e28jsnbbcf6a8da233' );

      xhr.send(data); // temp
    });
  }

  makeColorArray(e: any, id: string) {
    if (!this.colorList.includes(id)) {
      this.colorList.push(id);
    } else {
      this.colorList = this.colorList.filter(m => m !== id);
    }
  }

  async filter() {
    this.dorsalPatternArray = [];
    this.ventralPatternArray = [];
    this.dorsalPrimaryColorArray = [];
    this.dorsalSecondaryColorArray = [];
    this.ventralPrimaryColorArray = [];
    this.ventralSecondaryColorArray = [];
    // might need to reset the selected stuff too but hope not...
    if (this.colorList.includes('Orange')) {
      this.dorsalPrimaryColorArray.push('Orange');
    }
    if (this.colorList.includes('White')) {
      this.dorsalPrimaryColorArray.push('White');
    }
    if (this.colorList.includes('Brown')) {
      this.dorsalPrimaryColorArray.push('Brown');
    }
    if (this.colorList.includes('Black')) {
      this.dorsalPrimaryColorArray.push('Black');
    }
    if (this.colorList.includes('Grey')) {
      this.dorsalPrimaryColorArray.push('Grey');
    }
    if (this.colorList.includes('Blue')) {
      this.dorsalPrimaryColorArray.push('Blue');
    }
    if (this.colorList.includes('Green')) {
      this.dorsalPrimaryColorArray.push('Green');
    }
    if (this.colorList.includes('Purple')) {
      this.dorsalPrimaryColorArray.push('Purple');
    }
    if (this.colorList.includes('Red')) {
      this.dorsalPrimaryColorArray.push('Red');
    }
    if (this.colorList.includes('Yellow')) {
      this.dorsalPrimaryColorArray.push('Yellow');
    }
    if (this.colorList.includes('Copper')) {
      this.dorsalPrimaryColorArray.push('Copper');
      // this.dorsalPrimaryColorArray.push('Bronze');
    }
    if (this.colorList.includes('Orange-h')) {
      this.ventralPrimaryColorArray.push('Orange');
    }
    if (this.colorList.includes('White-h')) {
      this.ventralPrimaryColorArray.push('White');
    }
    if (this.colorList.includes('Brown-h')) {
      this.ventralPrimaryColorArray.push('Brown');
    }
    if (this.colorList.includes('Black-h')) {
      this.ventralPrimaryColorArray.push('Black');
    }
    if (this.colorList.includes('Grey-h')) {
      this.ventralPrimaryColorArray.push('Grey');
    }
    if (this.colorList.includes('Blue-h')) {
      this.ventralPrimaryColorArray.push('Blue');
    }
    if (this.colorList.includes('Green-h')) {
      this.ventralPrimaryColorArray.push('Green');
    }
    if (this.colorList.includes('Purple-h')) {
      this.ventralPrimaryColorArray.push('Purple');
    }
    if (this.colorList.includes('Red-h')) {
      this.ventralPrimaryColorArray.push('Red');
    }
    if (this.colorList.includes('Yellow-h')) {
      this.ventralPrimaryColorArray.push('Yellow');
    }
    if (this.colorList.includes('Copper-h')) {
      this.ventralPrimaryColorArray.push('Copper');
      // this.ventralPrimaryColorArray.push('Bronze');
    }
    if (this.selectedPattern === 'Veins') {
      this.dorsalPatternArray.push('Veins');
    }
    if (this.selectedPattern === 'Checkered') {
      this.dorsalPatternArray.push('Checkered');
    }
    if (this.selectedPattern === 'Dark Spots') {
      this.dorsalPatternArray.push('Dark Spots');
    }
    if (this.selectedPattern === 'Bright Spots') {
      this.dorsalPatternArray.push('Bright Spots');
    }
    if (this.selectedPattern === 'Striped') {
      this.dorsalPatternArray.push('Striped');
    }
    if (this.selectedPattern === 'Mottled') {
      this.dorsalPatternArray.push('Mottled');
    }
    if (this.selectedPattern === 'Camoflauge') {
      this.dorsalPatternArray.push('Camoflauge');
    }
    if (this.selectedPattern === 'Iridescent') {
      this.dorsalPatternArray.push('Iridescent');
    }
    if (this.selectedPattern === 'Eyespots') {
      this.dorsalPatternArray.push('Eyespots');
    }
    if (this.selectedPattern === 'Frosted') {
      this.ventralPatternArray.push('Frosted');
    }
    if (this.selectedPatternV === 'Veins') {
      this.ventralPatternArray.push('Veins');
    }
    if (this.selectedPatternV === 'Checkered') {
      this.ventralPatternArray.push('Checkered');
    }
    if (this.selectedPatternV === 'Dark Spots') {
      this.ventralPatternArray.push('Dark Spots');
    }
    if (this.selectedPatternV === 'Bright Spots') {
      this.ventralPatternArray.push('Bright Spots');
    }
    if (this.selectedPatternV === 'Striped') {
      this.ventralPatternArray.push('Striped');
    }
    if (this.selectedPatternV === 'Mottled') {
      this.ventralPatternArray.push('Mottled');
    }
    if (this.selectedPatternV === 'Camoflauge') {
      this.ventralPatternArray.push('Camoflauge');
    }
    if (this.selectedPatternV === 'Iridescent') {
      this.ventralPatternArray.push('Iridescent');
    }
    if (this.selectedPatternV === 'Eyespots') {
      this.ventralPatternArray.push('Eyespots');
    }
    if (this.selectedPatternV === 'Frosted') {
      this.ventralPatternArray.push('Frosted');
    }

    // if (this.familyArray.length === 0) { this.familyArray = null; }
    if (this.dorsalPatternArray.length === 0) { this.dorsalPatternArray = null; }
    if (this.ventralPatternArray.length === 0) { this.ventralPatternArray = null; }
    if (this.dorsalPrimaryColorArray.length === 0) { this.dorsalPrimaryColorArray = null; }
    if (this.dorsalSecondaryColorArray.length === 0) { this.dorsalSecondaryColorArray = null; }
    if (this.ventralPrimaryColorArray.length === 0) { this.ventralPrimaryColorArray = null; }
    if (this.ventralSecondaryColorArray.length === 0) { this.ventralSecondaryColorArray = null; }
    await this.getButterflies();
  }

  /**
   * Opens Tails Dialog
   */
  openTails() {
    this.dialog.open(DialogTailsGuideComponent);
  }

  /**
   * Opens Antennae Dialog
   */
  openAntennae() {
    this.dialog.open(DialogAntennaeGuideComponent);
  }

  /**
   * Opens Shape Dialog
   */
  openShape() {
    this.dialog.open(DialogShapeGuideComponent);
  }

  /**
   * Opens Pattern Dialog
   */
  openPattern() {
    this.dialog.open(DialogPatternGuideComponent);
  }

  /**
   * Moves page left
   */
  pageLeft() {
    // moves page index down one
    if (this.pageNumber !== 0) {
      this.pageNumber--;
      // tslint:disable-next-line:max-line-length
      this.butterfliesDisplay = this.butterflies.slice(this.pageNumber * this.butterfliesPerPage, (this.pageNumber * this.butterfliesPerPage) + this.butterfliesPerPage);
    }
  }

  /**
   * Moves page right
   */
  pageRight() {
    // moves page index down one
    if (this.pageNumber !== (this.butterflies.length / this.butterfliesPerPage) - 1) {
      this.pageNumber++;
      // tslint:disable-next-line:max-line-length
      this.butterfliesDisplay = this.butterflies.slice(this.pageNumber * this.butterfliesPerPage, (this.pageNumber * this.butterfliesPerPage) + this.butterfliesPerPage);
    }
  }

  /**
   * Pagation functions... splits butterflies into groups of this.butterflyPerPage and page fxns
   * @param index
   */
  pageFunc(index) {
    // slice of butterflies per page.

    this.pageNumber = index;
    // tslint:disable-next-line:max-line-length
    this.butterfliesDisplay = this.butterflies.slice(this.pageNumber * this.butterfliesPerPage, (this.pageNumber * this.butterfliesPerPage) + this.butterfliesPerPage);
  }

  /**
   * Handles changing colors to buttons when user types in butterfly species selector
   */
  selectorChange() {
    const newValue = this.butterflySpeciesSelector.getValue();
    console.log('newValue: ' + newValue);
  }

  /**
   * Fills butterfly species selector and handles changing button colors
   * @param value
   */
  fillValue(value) {
    this.butterflySpeciesSelector.setValue(this.titleCasePipe.transform(value));
  }

  /**
   * reads iNaturalist response and puts valid species in butterfliesResponse
   * @param input
   */
  readResponse(input) {
    butterfliesResponse = [];
    const json = JSON.parse(input);

    let results = '';
    this.butterfliesResponseDB = [];

    for (let i = 0; i < json.results.length; i++) { // 1?
      // const counter = i + 1;
      // results = results + counter + '. ' + json.results[i].taxon.preferred_common_name + ' (' + json.results[i].taxon.name + ')\n';

      if (speciesExistsInDB(json.results[i].taxon.name)) {
        // if (speciesExistsInDB(json.results[i].taxon.preferred_common_name)) {
          this.butterfliesResponseDB.push(json.results[i].taxon.name);
        results = results + json.results[i].taxon.preferred_common_name + ' (' + json.results[i].taxon.name + ')\n';
      }
    }
    console.log(results);

    this.tempButterflies = butterfliesResponse.slice(0, Math.max(butterfliesResponse.length, 3));
    if (this.tempButterflies.length === 0) {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 4500,
        data: {'messageKey': 'no-species-returned', 'icon': 'error'},
        panelClass: ['sb-error']
      });
      this.iNatReturned = true;
      return false;
    }

    this.iNatReturned = true;

    return results;
  }
} // end of butterfly component
// global stuff
let butterfliesOutside = null;
let butterfliesResponse = [];
let globaliNatReturned = false;


function compressImage( src, newX, newY ) {
  return new Promise( ( res, rej ) => {
    const img = new Image();
    img.src = src;
    img.onload = ( ) => {
      const elem = document.createElement( 'canvas' );
      elem.width = newX;
      elem.height = newY;
      const ctx = elem.getContext( '2d' );
      ctx.drawImage( img, 0, 0, newX, newY );
      const data = ctx.canvas.toBlob( res );

    };
    img.onerror = error => rej( error );
  } );
}

function speciesExistsInDB(value) {
  for (const butterfly of butterfliesOutside) {

    if (value !== undefined) {
      // if (butterfly.common_names[0] === value.toLowerCase()) {
      if (butterfly.species.toLowerCase() === value.toLowerCase()) {
      // if (butterfly.species === value.toLowerCase()) { // if want to change to species change here and 953
      butterfliesResponse.push(butterfly);
      return true;
      }
    }

  }
  return false;
}
