import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHomeFlowerIdExpertComponent } from './app-home-flower-id-expert.component';

describe('AppHomeFlowerIdComponent', () => {
  let component: AppHomeFlowerIdExpertComponent;
  let fixture: ComponentFixture<AppHomeFlowerIdExpertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHomeFlowerIdExpertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHomeFlowerIdExpertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
