import { ButterflyLog } from './../../../models/butterfly-log.model';
import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ElementRef,
  AfterViewInit,
//   SystemJsNgModuleLoader,
  OnInit,
  HostListener
} from '@angular/core';
import { BeeLog } from '../../../models/bee-log.model';
import { BeelogService } from '../../../services/beelog.service';
import {
  DialogBeeBehaviorDialogComponent,
  DialogGuidedFlowerIdentificationComponent,
  DialogButterflyBehaviorDialogComponent
} from '../app-home.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarInputBasic } from '../../../models/app-snackbars';
import { FlowerSpecies, ReferenceService } from '../../../api-module';
import { NetworkStatusService } from '../../../services/network-status.service';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-app-home-flower-id-expert',
  templateUrl: './app-home-flower-id-expert.component.html',
  styleUrls: ['./app-home-flower-id-expert.component.scss']
})
export class AppHomeFlowerIdExpertComponent implements OnInit {
  @Input() source;
  @Output() nextEmitter = new EventEmitter<any>();
  @Output() backEmitter = new EventEmitter<any>();
  @ViewChild('videoPlayer', {static: false}) videoPlayer;
  @ViewChild('flowerSpeciesSelector', {static: false}) flowerSpeciesSelector;


  beeLog: BeeLog;
  butterflyLog: ButterflyLog;
  beeBehaviors = ['Collecting nectar', 'Collecting pollen', 'Unknown'];
  // tslint:disable-next-line:max-line-length
  butterflyBehaviors = ['Nectaring', 'Puddling', 'Patrolling', 'Perching', 'Ovipositing', 'Mating', 'Unknown']; // order is set in butterflyRecord in models and server db models? actually maybe just app-log-summary.component.ts
  playStatus;
  imageData;
  imageData2;
  flowerStepId = 0;
  lowerFlowerName: string;
  responseData;
  resizedImage;
  suggestedText;
  tempFlowers = [];
  tempGenus = [];
  tempSpecies = [];
  getSuggestSwitch = false;
  iNaturalistSearchLimit = 0; // not the limit but the counter for search limit
  searchLimitTriggered = false; // checks to see if the search limit has been triggered
  iNatReturned = false; // used with globaliNatReturned to check if iNaturalist call has been returned before we throw errors
  isOnline = true;
  iNaturalistRoute = false;
  isButterfly = false;
  pageNumber = 0;
  flowersPerPage = 3;
  flowersDisplay: FlowerSpecies[];
  flowerPages;
  totalPages = 4; // need to set on init / resize and again in get butterflies
  imgOwner;
  copyRightType;
  copyRightLink;
  overlayFlower: FlowerSpecies;

  constructor(
    private beelogService: BeelogService,
    private flowerService: ReferenceService,
    private networkStatus: NetworkStatusService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    public titleCasePipe: TitleCasePipe) {
    this.beeLog = beelogService.beeLog;
    this.butterflyLog = beelogService.butterflyLog;
  }

  async ngOnInit() {
    // Check initial network status
    this.networkStatus.checkOnline().then((isOnline) => {
      this.isOnline = isOnline;
    });
    // Set up network status monitoring
    this.networkStatus.isOnline.subscribe(() => {
      console.log('now online');
      this.isOnline = true;
    });
    this.networkStatus.isOffline.subscribe(() => {
      console.log('now offline');
      this.isOnline = false;
    });

    this.isButterfly = false;
    if (this.beeLog.beeCharacteristic.beeName === 'Butterfly') {
      this.isButterfly = true;
    }
    await this.getFlowers();

    /*if (this.source.type !== 'video') {
      this.imageData = this.source;
    }*/
    /*if (window.innerWidth >= 600) {
      this.flowersPerPage = 5;
    } else if (window.innerWidth <= 350) {
      this.flowersPerPage = 1;
    }
    window.addEventListener('resize', () => {
      const windowWidth = window.innerWidth - 30; // need to change padding not sure how to find that number
      let bfp = 1;
      while ((bfp + (bfp - 1) * (12 + 150)) < windowWidth) {
        bfp++;
      }
      if (((bfp - 2) * 2) >= 6) {
        this.flowersPerPage = 5;
      } else {
        this.flowersPerPage = ((bfp - 2) * 2) - 1;
      }

      if (this.tempFlowers) {
        this.flowerPages = this.tempFlowers.filter((_b, i) => i % this.flowersPerPage === 0);
      }
      this.totalPages =  this.tempFlowers.length / this.flowersPerPage;
      this.pageFunc(0);
    });

    this.totalPages =  this.tempFlowers.length / this.flowersPerPage;*/
  }

  async getFlowers() {
    flowersOutside = await this.flowerService.getFlowers().toPromise();
  }

  next() {
    // console.log('next');
    if (!this.playing() && !(this.source.type === 'video' && this.hasFrame()) && this.validate()) {
      // 'flowerName': 'SET LATER',
      // 'flowerName': 'Unknown' is actually null

      if (this.source.type === 'video' &&
        ((this.flowerSpeciesSelector.data.flowerName == null) || (this.flowerSpeciesSelector.data.flowerName === 'SET LATER'))
        && this.flowerStepId === 0) {

        this.flowerStepId = 1;
      } else if (this.flowerStepId === 1) {
        this.nextEmitter.emit(this.imageData);
      } else {
        this.nextEmitter.emit(this.imageData);
      }
    }
  }

  /**
   * Goes to straight to iNaturalist page... checks if online first
   */
  goToiNaturalist() {
    if (this.isOnline) {
      this.flowerStepId = 2;
      this.iNaturalistRoute = true;
    } else {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 3000,
        data: {'messageKey': 'not-connected-to-internet', 'icon': 'error'},
        panelClass: ['sb-error']});
    }

  }

  /**
   * Goes to Manual id Page
   */
  goToManualId () {
    this.flowerStepId = 1;
  }

  back() {
    this.overlayFlower = null;
    if (this.flowerStepId === 1) {
      this.flowerStepId = 0;
    } else if (this.flowerStepId === 2) {
      this.flowerStepId = 0;
    } else {
      this.backEmitter.emit();
    }
  }

  toggleBehaviorDialog() {
    if (this.isButterfly) {
      this.dialog.open(DialogButterflyBehaviorDialogComponent, { });
    } else {
      this.dialog.open(DialogBeeBehaviorDialogComponent, { });
    }
  }

  toggleDialog(auto) {
    this.dialog.open(DialogGuidedFlowerIdentificationComponent, {
      data: { video: this.source.type === 'video',
              auto: auto } }
      );
  }

  playing() {
    if (this.playStatus) {
      this.toggleDialog(true);
      this.videoPlayer.pauseVideo();
      return true;
    }
    return false;
  }

  hasFrame() {
      if (this.imageData == null) {
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 3000,
          data: {'messageKey': 'error-flower-frame', 'icon': 'error'},
          panelClass: ['sb-error']});
        return true;
      }
      return false;
    }

  saveFrame(value) {
    this.imageData = value;
  }

  validate() {

    if (flowersiNatResponse.length > 0) {
      this.butterflyLog.flowerSpeciesGuessArray = [];
      this.beeLog.flowerSpeciesGuessArray = [];
      for (let i = 0; i < flowersiNatResponse.length; i++) {
        this.butterflyLog.flowerSpeciesGuessArray.push(flowersiNatResponse[i]);
        this.beeLog.flowerSpeciesGuessArray.push(flowersiNatResponse[i]);
      }
    }

    if (this.isButterfly) {
      if (this.butterflyLog.butterflyBehavior != null) {
        if (this.flowerSpeciesSelector.validate()) {
          const data = this.flowerSpeciesSelector.data;

          this.butterflyLog.flowerName = data.flowerName;
          this.butterflyLog.flowerFamilyName = data.flowerFamilyName;
          this.butterflyLog.flowerColor = data.flowerColor;
          this.butterflyLog.flowerShape = data.flowerShape;
          this.butterflyLog.flowerCommonName = data.flowerCommonName;
          this.butterflyLog.flowerLifeCycle = data.flowerLifeCycle;

          return true;
        }
        return false;
      } else {
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 3000,
          data: {'messageKey': 'error-field-input', 'icon': 'error'},
          panelClass: ['sb-error']});
        return false;
      }
    } else {
      if (this.beeLog.beeBehavior != null) {
        if (this.flowerSpeciesSelector.validate()) {
          const data = this.flowerSpeciesSelector.data;
          this.beeLog.flowerName = data.flowerName;
          this.beeLog.flowerFamilyName = data.flowerFamilyName;
          this.beeLog.flowerColor = data.flowerColor;
          this.beeLog.flowerShape = data.flowerShape;
          this.beeLog.flowerCommonName = data.flowerCommonName;
          this.beeLog.flowerLifeCycle = data.flowerLifeCycle;

          return true;
        }
        return false;
      } else {
        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 3000,
          data: {'messageKey': 'error-field-input', 'icon': 'error'},
          panelClass: ['sb-error']});
        return false;
      }
    }

  }


  capitalizeCommonName(flowerName) {
    const words = flowerName.split(' ');
    for (let i = 0; i < words.length; i++) {
      words[i] = words[i][0].toUpperCase() + words[i].substr(1);
    }
    return words.join(' ');
  }

  overlayOn(flower) {

    this.overlayFlower = flower;
    document.getElementById('overlayF').style.display = 'block';
    const elementTarget = document.getElementById('overlay');
    window.scrollTo({top: elementTarget.offsetTop, behavior: 'smooth'});
  }


  getSuggest() {

    globaliNatReturned = false;
    this.iNatReturned = false;
    if (this.hasFrame() && this.source.type === 'video') {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 3000,
        data: {'messageKey': 'capture-image-for-iNat', 'icon': 'error'},
        panelClass: ['sb-error']});
    } else {
      // console.log('herreeee: ');
      // console.log(this.imageData.image);
      // console.log(this.imageData);
      // checks if you've reached your search limit already
      if (this.iNaturalistSearchLimit < 2) {

        if (this.iNaturalistSearchLimit === 1) {
          this.searchLimitTriggered = true;
        }

        // checks to make sure the image you are submitting is a new image
        if (this.imageData !== this.imageData2) { // checks if iNaturalist has been run on an image

          this.iNaturalistSearchLimit++;
          this.getSuggestSwitch = true;
          const strSuggs = this.resizeSendImage();
          console.log(strSuggs);
          this.suggestedText = strSuggs;

          this.imageData2 = this.imageData;
        } else {
          this.snackBar.openFromComponent(SnackBarInputBasic, {
            duration: 3000,
            data: {'messageKey': 'capture-new-image', 'icon': 'error'},
            panelClass: ['sb-error']});
        }
      } else {

        this.snackBar.openFromComponent(SnackBarInputBasic, {
          duration: 3000,
          data: {'messageKey': 'out-of-iNat-queries', 'icon': 'error'},
          panelClass: ['sb-error']});
      }

    }
  }

  resizeSendImage() {
    const i = new Image();

    i.onload = function() {
      console.log( 'Original image size: ' + i.width + ', ' + i.height );
    };

    // i.src = this.source.file;
    if (this.source.type === 'video') {
      i.src = this.imageData.image; // using image rather than video
    } else {
      i.src = this.imageData; // this should have worked
    }

    let newWidth = 300;
    let newHeight = 300;

    if (i.width > i.height) {

      newHeight = Math.round(300 * (i.height / i.width));
    } else if (i.width < i.height) {

      newWidth = Math.round(300 * (i.width / i.height));

    }

    console.log( 'Modified image size: ' + newWidth + ', ' + newHeight );

    // compressImage( this.source.file, newWidth, newHeight ).then( (blob:Blob )=> {
    compressImage( i.src, newWidth, newHeight ).then( (blob: Blob ) => {
      const data = new FormData( );

      data.append( 'image', blob);

      data.append( 'taxon_id', '47126'); // looking for any plant
      // data.append( 'taxon_id', '47125'); // looking for a flower
      // data.append( 'taxon_id', '47157'); // looking for a butterfly
      // tslint:disable-next-line:max-line-length
      // taxon_id can be found at https://www.inaturalist.org/taxa by searching for the taxa of interest. The id is a five digit number in the URL

      const xhr = new XMLHttpRequest();
      xhr.onload = () => this.readResponse(xhr.responseText);

      xhr.open( 'POST', 'https://visionapi.p.rapidapi.com/v1/rapidapi/score_image' );
      xhr.setRequestHeader( 'Access-Control-Allow-Origin', 'http://localhost:4200' );
      xhr.setRequestHeader( 'x-rapidapi-host', 'visionapi.p.rapidapi.com' );
      xhr.setRequestHeader( 'x-rapidapi-key', 'b7ddc20751msh765abd135dc9500p125e28jsnbbcf6a8da233' );

      xhr.send(data); // temp
    });
  }

  /**
   * Handles changing colors to buttons when user types in butterfly species selector
   */
  selectorChange() {
    const newValue = this.flowerSpeciesSelector.getValue();
    console.log('newValue: ' + newValue);

  }

  /**
   * Fills butterfly species selector and handles changing button colors
   * @param value
   */
  fillValue(value) {
    this.flowerSpeciesSelector.setValue(this.titleCasePipe.transform(value));

  }


  /**
   * reads iNaturalist response and puts valid species in butterfliesResponse
   * @param input
   */
  async readResponse(input) {
    flowersResponse = [];
    flowersiNatResponse = [];
    possibleSpecies = [];
    possibleGenus = [];
    checkedTaxons = [];
    const json = JSON.parse(input);
    // console.log('Read Response: '); see iNaturalist responses
    console.log(json);

    for (let i = 0; i < json.results.length; i++) { // 1?
      // actually checks if in db and if its not it adds it to db
      // commented out after realizing normal users dont have post privileges
      if (speciesDoesntExistInDB(json.results[i].taxon.name)) {
        let commonName = '';
        if (json.results[i].taxon.preferred_common_name) {
          commonName = json.results[i].taxon.preferred_common_name;
        } else {
          commonName = json.results[i].taxon.name;
        }
        commonName = commonName.toLowerCase();
        const value = json.results[i].taxon.name.split(' ');
        const genus = value[0].toLowerCase();
        let species = 'spp.';
        if (value.length === 2) {
          species = value[1].toLowerCase();
        }

        const flowerSpecies = {
          genus: genus,
          species: species,
          common_name: commonName,
          alt_names: [commonName],
          main_color: '' // ,
          // image: ['unknownFlowerImg.png']
        };
        console.log(flowerSpecies);
        await this.flowerService.postFlower(flowerSpecies).toPromise();
        await this.flowerSpeciesSelector.refreshFlowerList();
      }
    }

    await this.getFlowers();

    for (let i = 0; i < json.results.length; i++) {
      // adding first three species to flowers response and flowersiNatResponse
      speciesExistsInDB(json.results[i].taxon.name);
    }

    this.tempFlowers = flowersResponse.slice(0 , 3);
    // this.tempFlowers = flowersResponse.slice(0, Math.max(flowersResponse.length, 3));
    // this.tempFlowers = flowersResponse;
    const setGenus = new Set(possibleGenus);
    this.tempGenus = Array.from(setGenus.values());
    const setSpecies = new Set(possibleSpecies);
    this.tempSpecies = Array.from(setSpecies.values());
    if (this.tempFlowers.length === 0) {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 4500,
        data: {'messageKey': 'no-species-returned', 'icon': 'error'},
        panelClass: ['sb-error']
      });
      this.iNatReturned = true;
      return false;
    }
    // set up all of pagination stuff with new tempButterflies
    this.totalPages = this.tempFlowers.length / this.flowersPerPage;
    this.flowersDisplay = this.tempFlowers.slice(0, this.flowersPerPage);
    this.flowerPages = this.tempFlowers.filter((_b, i) => i % this.flowersPerPage === 0);

    console.log(this.tempFlowers);

    this.iNatReturned = true;

    return;
  }
} // end of app-home-flower-id-expert
let flowersOutside = null;
let checkedTaxons = [];
let flowersResponse = [];
let flowersiNatResponse = [];
let possibleGenus = [];
let possibleSpecies = [];
let globaliNatReturned = false;


function compressImage( src, newX, newY ) {
  return new Promise( ( res, rej ) => {
    const img = new Image();
    img.src = src;
    img.onload = ( ) => {
      const elem = document.createElement( 'canvas' );
      elem.width = newX;
      elem.height = newY;
      const ctx = elem.getContext( '2d' );
      ctx.drawImage( img, 0, 0, newX, newY );
      const data = ctx.canvas.toBlob( res );

    };
    img.onerror = error => rej( error );
  } );
}

function speciesExistsInDB(value) {
  if (value !== undefined) {
    const valueSplit = value.split(' ');
    const genus = valueSplit[0];
    let species = 'spp.';
    if (valueSplit.length > 1) {
      species = valueSplit[1];
    }
    possibleGenus.push(genus);
    possibleSpecies.push(species);

    if (!checkedTaxons.includes(genus)) {
      checkedTaxons.push(genus);
      for (const flower of flowersOutside) {

        if (flower.species.toLowerCase() === species.toLowerCase()) {
          // if (flower.species.toLowerCase() === valueSplit[1].toLowerCase()) {
          flowersResponse.push(flower);
          const flowerString = flower.genus + ' ' + flower.species;
          flowersiNatResponse.push(flowerString);
          // }
        }
      }
    }
  }
  return;
}

function speciesDoesntExistInDB(value) {
  const valueSplit = value.split(' ');
  for (const flower of flowersOutside) {

    if (valueSplit[0]) {
      if  (valueSplit[1]) {
        if (flower.species.toLowerCase() === valueSplit[1].toLowerCase()) {
          return false;
        }
      } else {
        if (flower.genus.toLowerCase() === valueSplit[0].toLowerCase() || flower.species.toLowerCase() === 'spp.') {
          return false;
        }
      }
    }

  }
  return true;
}
