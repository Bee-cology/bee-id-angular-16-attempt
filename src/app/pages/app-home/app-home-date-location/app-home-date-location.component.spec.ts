import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHomeDateLocationComponent } from './app-home-date-location.component';

describe('AppHomeDateLocationComponent', () => {
  let component: AppHomeDateLocationComponent;
  let fixture: ComponentFixture<AppHomeDateLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHomeDateLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHomeDateLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
