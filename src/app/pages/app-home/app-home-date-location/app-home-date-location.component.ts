import { Component, EventEmitter, OnInit, Output, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppLocationSelectorComponent } from '../../app-location-selector/app-location-selector.component';
import { BeeLog } from '../../../models/bee-log.model';
import { ButterflyLog } from '../../../models/butterfly-log.model';
import { BeelogService } from '../../../services/beelog.service';
import { LocationService } from '../../../services/location.service';

@Component({
  selector: 'app-app-home-date-location',
  templateUrl: './app-home-date-location.component.html',
  styleUrls: ['./app-home-date-location.component.scss']
})
export class AppHomeDateLocationComponent implements OnInit {
  @ViewChild(AppLocationSelectorComponent, {static: true}) locationSelector;
  @Output() nextEmitter = new EventEmitter<any>();
  @Output() backEmitter = new EventEmitter<any>();
  dateMapFormGroup: FormGroup;
  beeLog: BeeLog;
  butterflyLog: ButterflyLog;
  constructor(
    private beelogService: BeelogService,
    private _formBuilder: FormBuilder,
    private locationService: LocationService) {
    this.beeLog = beelogService.beeLog;
    this.beeLog.date = new Date();
    this.butterflyLog = beelogService.butterflyLog;
    this.butterflyLog.date = new Date();
  }

  ngOnInit() {
    this.dateMapFormGroup = this._formBuilder.group({
      dateCtrl: ['', Validators.required]
    });
  }

  next() {
    if (this.locationSelector.validate()) {
      this.setBeeLogLocation();
      this.setButterflyLogLocation();
      this.nextEmitter.emit(this.locationSelector.newLocation);
    }
  }

  back() {
    this.backEmitter.emit();
  }

  private setBeeLogLocation() {
    this.beeLog.city = this.locationSelector.locationAddress;
    this.beeLog.hasLocation = this.locationSelector.hasLocation;
    const lat = this.locationSelector.lat;
    const lng = this.locationSelector.lng;
    this.beeLog.location = this.locationService.coordsToString(lat, lng);
  }
  private setButterflyLogLocation() {
    this.butterflyLog.city = this.locationSelector.locationAddress;
    this.butterflyLog.hasLocation = this.locationSelector.hasLocation;
    const lat = this.locationSelector.lat;
    const lng = this.locationSelector.lng;
    this.butterflyLog.location = this.locationService.coordsToString(lat, lng);
  }

}
