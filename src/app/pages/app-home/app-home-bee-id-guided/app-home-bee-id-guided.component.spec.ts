import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHomeBeeIdGuidedComponent } from './app-home-bee-id-guided.component';

describe('AppHomeBeeIdGuidedComponent', () => {
  let component: AppHomeBeeIdGuidedComponent;
  let fixture: ComponentFixture<AppHomeBeeIdGuidedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHomeBeeIdGuidedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHomeBeeIdGuidedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
