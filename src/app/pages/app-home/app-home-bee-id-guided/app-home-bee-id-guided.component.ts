import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BeeLog } from '../../../models/bee-log.model';
import { BeelogService } from '../../../services/beelog.service';
import { BeePicturePackage } from '../../../models/bee-picture-package.model';
import { BeePicturePackageData } from '../../../models/bee-picture-package-data.model';
import { beeCharacteristics } from '../../../models/bee-checker.model';
import { MatDialog} from '@angular/material/dialog';
import { DialogGuidedBeeIdentificationComponent } from '../app-home.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';
import { SnackBarInputBasic } from '../../../models/app-snackbars';
import * as _ from 'lodash';

@Component({
  selector: 'app-app-home-bee-id-guided',
  templateUrl: './app-home-bee-id-guided.component.html',
  styleUrls: ['./app-home-bee-id-guided.component.scss']
})

export class AppHomeBeeIdGuidedComponent {
  @Input() source;
  @Output() nextEmitter = new EventEmitter<any>();
  @Output() backEmitter = new EventEmitter<any>();
  @ViewChild('videoPlayer', { static: false }) videoPlayer;

  bases = ['a1', 'a5', 'a8'];
  titleText = ['Abdomen Coloration', 'Abdomen Pattern', 'Thorax Pattern', 'Head Pattern'];
  possibleAbdomens;
  possibleThoruses;
  possibleHeads;
  currentAbdomen;
  currentHead;
  currentThorus;
  chosenBeePartIds = { 'base': null, 'abdomen': null, 'thorax': null, 'head': null };
  beeParts = ['base', 'abdomen', 'thorax', 'head'];

  beePackage: BeePicturePackage;
  beeLog: BeeLog;
  beeStepId = 0;
  playStatus;
  firstArrival = true;
  imageData = [];
  displayImages = [];
  videoTransforms = [];

  // ADDED THIS BECAUSE error TS2339: Property 'imageSrc' does not exist on type 'AppHomeBeeIdGuidedComponent'.
  imageSrc = "";

  constructor(
    private beelogService: BeelogService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private cookieService: CookieService) {
    this.beeLog = beelogService.beeLog;
  }

  next() {
    if (this.source.type === 'video' && this.beeStepId >= 1 && this.beeStepId <= 3) {
      if (this.checkPlayStatus()) {
        return;
      } else if (this.checkFrameStatus()) {
        return;
      }
      if (this.videoTransforms[this.beeStepId]) {
        this.setTransform(this.videoTransforms[this.beeStepId]);
      } // update transform when going forward and transform is available
    }
    switch (this.beeStepId) {
      case 0:
        if (this.chosenBeePartIds['base'] === null) {
          this.snackBar.openFromComponent(SnackBarInputBasic, {
            duration: 2000,
            data: { 'messageKey': 'error-abdomen-base-color', 'icon': 'error' },
            panelClass: ['sb-error']
          });
          return;
        } else {
          this.chooseBase(this.chosenBeePartIds['base']);
        }
        if (this.source.type === 'video') {
          this.videoPlayer.captureEnabled = true;
        }
        break;
      case 1:
        this.currentAbdomen = this.possibleAbdomens[this.chosenBeePartIds['abdomen']];
        this.beelogService.setAbdomen(this.currentAbdomen);
        this.possibleThoruses = this.beelogService.getThorus();
        break;
      case 2:
        this.currentThorus = this.possibleThoruses[this.chosenBeePartIds['thorax']];
        this.beelogService.setThorus(this.currentThorus);
        break;
      case 3:
        if (this.source.type === 'video') {
          this.displayImages = [this.imageData[0].image, this.imageData[1].image, this.imageData[2].image];
        } else {
          this.displayImages = [this.source.file];
        }
        this.currentHead = this.possibleHeads[this.chosenBeePartIds['head']];
        this.beelogService.setHead(this.currentHead);
        this.identifyBee();
        break;
      case 4:
        this.nextEmitter.emit({ 'beePackage': this.beePackage, 'imageData': this.imageData });
        break;
    }
    if (this.beeStepId < 4) {
      this.beeStepId++;
    }
  }

  back() {
    if (this.source.type === 'video' && this.beeStepId >= 2 && this.beeStepId <= 3) {
      if (this.checkPlayStatus()) {
        return;
      }
      this.videoTransforms[this.beeStepId - 1] = this.getCurrentTransform(); // save transform when going back on Thorax or Head
    }
    switch (this.beeStepId) {
      case 0:
        this.backEmitter.emit();
        return;
	  // @ts-ignore
      case 1:
        if (this.source.type === 'video') {
          this.videoPlayer.captureEnabled = false;
        } // continue to case 2 code
      // tslint:disable-next-line:no-switch-case-fall-through
      case 2:
        this.currentAbdomen = null;
        break;
      case 3:
        this.currentThorus = null;
        break;
    }
    this.beeStepId--;
    if (this.source.type === 'video' && this.beeStepId >= 1 && this.beeStepId <= 3) {
      this.setTransform(this.videoTransforms[this.beeStepId - 1]);
    } // update transform when going back
  }

  checkPlayStatus() {
    if (this.playStatus) {
      this.toggleDialog(true, false);
      this.videoPlayer.pauseVideo();
      return true;
    }
    return false;
  }

  checkFrameStatus() {
    if (!this.imageData[this.beeStepId - 1]) {
      this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: { 'message': 'Capture a video frame of your bee\'s ' + this.beeParts[this.beeStepId] + '.', 'icon': 'error' },
        panelClass: ['sb-error']
      });
      return true;
    }
    return false;
  }

  toggleDialog(auto, button) {
    document.body.classList.add('dialog-open');
    const dialogRef = this.dialog.open(DialogGuidedBeeIdentificationComponent, {
        data: {
          step: this.beeStepId,
          video: this.source.type === 'video',
          auto: auto,
          button: button
        }
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      document.body.classList.remove('dialog-open');
      if (auto && button && result) {
        this.cookieService.set('autoPopup', 'stop');
      }
    });
  }

  getCurrentTransform() {
    const v = this.videoPlayer;
    return { 'scale': v.displayImageCurrentScale, 'x': v.displayImageCurrentX, 'y': v.displayImageCurrentY, 'time': v.currentTime };
  }

  setTransform(t) {
    const v = this.videoPlayer;
    v.displayImageCurrentX = v.displayImageX = t.x;
    v.displayImageCurrentY = v.displayImageY = t.y;
    v.displayImageCurrentScale = v.displayImageScale = t.scale;
    v.currentTime = v.v.currentTime = t.time;
    v.updateRange();
    v.updateDisplayImage(t.x, t.y, t.scale);
  }

  chooseBase(id) {
    this.beelogService.setColor(id);
    this.possibleAbdomens = this.beelogService.getAbdomens();
    this.possibleHeads = this.beelogService.getHeader();
  }

  partSelected(event) {
    this.chosenBeePartIds[event.name.toLowerCase()] = event.selectedId;
  }

  private identifyBee() {
    const bee = _.find(beeCharacteristics, b => {
      return b.color === this.beeLog.beeCharacteristic.color &&
        b.abdomen === this.beeLog.beeCharacteristic.abdomen &&
        b.thorus === this.beeLog.beeCharacteristic.thorus &&
        b.head === this.beeLog.beeCharacteristic.head;
    });

    const beeName = bee ? bee.beeName : undefined;
    this.beePackage = beeName ? BeePicturePackageData[beeName] : undefined;

    if (this.beePackage) {
      this.beeLog.beeCharacteristic.beeName = this.beePackage.name;
      this.beeLog.gender = this.beePackage.gender;
      this.beeLog.cartoon = this.beePackage.cartoon;
    } else {
      this.beeLog.beeCharacteristic.beeName = 'Unknown';
      this.beeLog.gender = 'Unknown';
    }
  }

  saveFrame(value) {
    this.imageData[this.beeStepId - 1] = value;
    this.videoTransforms[this.beeStepId - 1] = this.getCurrentTransform();
  }
}
