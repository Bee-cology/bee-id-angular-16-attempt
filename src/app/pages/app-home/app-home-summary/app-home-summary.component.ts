import { ButterflyLog } from './../../../models/butterfly-log.model';
import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { BeeLog } from '../../../models/bee-log.model';
import { BeelogService } from '../../../services/beelog.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DatabaseService } from '../../../services/database.service';
import { Router } from '@angular/router';
import { SavedLocationsService } from '../../../services/saved-locations.service';
import { SnackBarInputBasic } from '../../../models/app-snackbars';
import { DialogVideoUploadComponent } from '../app-home.component';

@Component({
  selector: 'app-app-home-summary',
  templateUrl: './app-home-summary.component.html',
  styleUrls: ['./app-home-summary.component.scss']
})

export class AppHomeSummaryComponent implements OnInit {
  @Output() backEmitter = new EventEmitter<any>();
  @Output() choiceEmitter = new EventEmitter<any>();
  @Input() images;
  @Input() beePackage;
  @Input() newLocation;
  @Input() uploadVideo;
  @Input() source;

  beeLog: BeeLog;
  butterflyLog: ButterflyLog;
  beeButterflyLog: BeeLog | ButterflyLog;

  maxVideoSize = 41943040; // 40 MB = 40 * 1024 * 1024 Bytes

  constructor(
    private beelogService: BeelogService,
    private db: DatabaseService,
    private savedLocationsService: SavedLocationsService,
    public snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog) {
    this.beeLog = beelogService.beeLog;
    this.butterflyLog = beelogService.butterflyLog;
    if (this.beeLog.beeCharacteristic.beeName === 'Butterfly') {
      this.beeButterflyLog = beelogService.butterflyLog;
    } else {
      this.beeButterflyLog = beelogService.beeLog;
    }
  }

  ngOnInit() {
    if (this.source.type === 'video' && !this.uploadVideo && this.source.file.size <= this.maxVideoSize) {
      this.openDialog();
    }
  }

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  })

  openDialog() {
    const dialogRef = this.dialog.open(DialogVideoUploadComponent, {autoFocus: false, data: {uploadVideo: this.uploadVideo}});
    document.body.classList.add('dialog-open');
    dialogRef.afterClosed().subscribe(result => {
      document.body.classList.remove('dialog-open');
      this.choiceEmitter.emit(result);
    });
  }

  back() {
    this.backEmitter.emit();
  }

  async saveLog() {
    if (this.beelogService.getIsBee()) {
      if (this.uploadVideo) {//
        this.beelogService.setVideoSrc(<string>await this.toBase64(this.source.file));
      }
      await this.db.addLogs(this.beeLog);
      const snackBarRef = this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: { 'messageKey': 'success-log-saved', 'icon': 'success' },
        panelClass: ['sb-success']
      });
      if (this.newLocation) {
        snackBarRef.afterDismissed().subscribe(() => {
          this.snackBar.openFromComponent(SnackBarInputBasic, {
            duration: 2000,
            data: { 'messageKey': 'info-log-location-saved', 'icon': 'info' },
            panelClass: ['sb-info']
          });
        });
        const lat = parseFloat(this.beeLog.location.split(',')[0]);
        const lng = parseFloat(this.beeLog.location.split(',')[1]);
        this.savedLocationsService.saveLocation(lat, lng).then();
      }
      this.router.navigate(['/app/logs']).then();
    } else {// if butterfly
      if (this.uploadVideo) {//
        this.beelogService.setVideoSrcButterfly(<string>await this.toBase64(this.source.file));
      }
      await this.db.addLogsButterfly(this.butterflyLog);
      const snackBarRef = this.snackBar.openFromComponent(SnackBarInputBasic, {
        duration: 2000,
        data: { 'messageKey': 'success-log-saved', 'icon': 'success' },
        panelClass: ['sb-success']
      });
      if (this.newLocation) {
        snackBarRef.afterDismissed().subscribe(() => {
          this.snackBar.openFromComponent(SnackBarInputBasic, {
            duration: 2000,
            data: { 'messageKey': 'info-log-location-saved', 'icon': 'info' },
            panelClass: ['sb-info']
          });
        });
        const lat = parseFloat(this.butterflyLog.location.split(',')[0]);
        const lng = parseFloat(this.butterflyLog.location.split(',')[1]);
        this.savedLocationsService.saveLocation(lat, lng).then();
      }
      this.router.navigate(['/app/logs']).then();
    }
  }
}
