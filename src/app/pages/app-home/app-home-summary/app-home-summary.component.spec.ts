import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHomeSummaryComponent } from './app-home-summary.component';

describe('AppHomeSummaryComponent', () => {
  let component: AppHomeSummaryComponent;
  let fixture: ComponentFixture<AppHomeSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHomeSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHomeSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
