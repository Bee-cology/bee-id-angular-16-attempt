import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BeelogService } from '../../services/beelog.service';
import { BeePicturePackage } from '../../models/bee-picture-package.model';
import { CheckUpdateService } from '../../services/check-update.service';

@Component({
  selector: 'app-app-home',
  templateUrl: './app-home.component.html',
  styleUrls: ['./app-home.component.scss'],
  providers: [BeelogService]
})
export class AppHomeComponent implements OnInit {
  source;
  beeImageData;
  // butterflyImageData;
  imageData = [];
  imageIndex;
  images = null;

  stepId = 0;
  stepIcons = ['cameraIcon.png', 'beeIcon.png', 'flowerIcon.png', 'mapIcon.png', 'summaryIcon.png', 'summaryIcon.png', '', '', 'butterflyIcon.png']; // currently using summary icon in two places
  stepStrings = ['Step 1: Upload Your Bee or Butterfly', 'Step 2: Identify Your Bee', 'Step 3: Identify the Flower', 'Step 4: Date and Locate', 'Step 5: Additional Comments', 'Step 6: Complete Log', '', '', 'Step 2: Identify Your Butterfly'];

  beePackage: BeePicturePackage = null;
  newLocation: boolean;
  flowerVideoInit = false; // used to initiate flower ID video only when that step is first reached (resolves problem with panning)
  uploadVideo: boolean; // whether to also save the raw video to the log

  constructor(private checkUpdateService: CheckUpdateService,
              private beelogService: BeelogService) {
  }

  ngOnInit() {
    this.checkUpdateService.checkForUpdate();
    console.log('stepId' + this.stepId);
  }

  next(value) {
    switch (this.stepId) {
      // source upload
      case 0:
        this.source = value;
        this.beelogService.setIsBee(this.source.specimen !== 'butterfly');
        if (!this.beelogService.getIsBee()) {
          this.beelogService.beeLog.beeCharacteristic.beeName = 'Butterfly';
          this.beelogService.setIsBee(false);
          this.stepId = 7; // temporary -- lol probably not
        }
        break;
      // bee identification
      case 1:
        this.beePackage = value.beePackage;
        if (this.beelogService.getIsBee()) {
          this.beeImageData = value.imageData;
        } else {

          this.beeImageData = value.imageData;
        }
        this.flowerVideoInit = true;
        break;
      // flower identification
      case 2:
        if (this.beeImageData) {
          this.imageData = this.beeImageData.slice(0);
        } // build imageData from beeImageData
        if (value) {
          this.imageData.push(value);
        } // add flower image data to imageData
        if (this.source && this.source.type === 'video') {
          this.compileImages();
        } else if (this.source && this.source.type === 'image') {
          this.images = [this.source.file];
          this.imageIndex = [['abdomen', 'thorax', 'head', 'flower']];
        }
        if (this.beelogService.getIsBee()) {
        this.beelogService.setImgSrc(this.images);
        this.beelogService.setImgIndex(this.imageIndex);
        } else { // if it butterfly
          this.beelogService.setImgSrcButterfly(this.images);
          this.beelogService.setImgIndexButterfly(this.imageIndex);
        }
        break;
      // date and location
      case 3:
        this.newLocation = value;
        break;
      case 8: // temporary
        this.stepId = 1; // temporary, go to flowers
        if (value) {
          this.imageData.push(value);
        }
        this.flowerVideoInit = true;
      break;
    }
    this.stepId++;
  }

  back() {
    if (this.stepId !== 0) {
      if (this.stepId === 2 && this.source.specimen === 'butterfly') {
        this.stepId = 8;
        return;
      }
      if (this.stepId === 8) {
        this.beelogService.beeLog.beeCharacteristic.beeName = null;
        this.beelogService.setIsBee(null);
        this.stepId = 0;
        return;
      }
      this.stepId--;
    }
    if (this.stepId === 0) {
      location.reload();
    } // reset app when returning to step 0 (fixes video re-upload issue) ))
    if(this.stepId === 6 && this.source.specimen === 'butterfly'){
      location.reload();
    }//same as above, but for butterfly
  }

  // removes duplicate images and creates an image index
  compileImages() {
    const indices = ['abdomen', 'thorax', 'head', 'flower'];
    this.imageIndex = [];
    this.images = [];
    for (let i = 0; i < this.imageData.length; i++) {
      if (this.imageData[i] != null) {
        this.images.push(this.imageData[i].image);
        this.imageIndex.push([indices[i]]);
        for (let j = i + 1; j < this.imageData.length; j++) {
          if (this.imageData[j] != null && (JSON.stringify(this.imageData[j].transform) === JSON.stringify(this.imageData[i].transform))) {
            this.imageIndex[this.imageIndex.length - 1].push(indices[j]);
            this.imageData[j] = null;
          }
        }
      }
    }
    this.imageIndex.filter((e) => e != null);
  }

}

@Component({
  selector: 'app-dialog-bee-behavior-dialog',
  templateUrl: 'dialog-bee-behavior-dialog.html',
})

export class DialogBeeBehaviorDialogComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-butterfly-behavior-dialog',
  templateUrl: 'dialog-butterfly-behavior-dialog.html',
})

export class DialogButterflyBehaviorDialogComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-guided-bee-identification',
  templateUrl: 'dialog-guided-bee-identification.html',
})

export class DialogGuidedBeeIdentificationComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-guided-butterfly-tails-identification',
  templateUrl: 'dialog-butterfly-tails.html',
})

export class DialogTailsGuideComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-guided-butterfly-shape-identification',
  templateUrl: 'dialog-butterfly-shape.html',
})

export class DialogShapeGuideComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-guided-butterfly-antennae-identification',
  templateUrl: 'dialog-butterfly-antennae.html',
})

export class DialogAntennaeGuideComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-guided-butterfly-pattern-identification',
  templateUrl: 'dialog-butterfly-pattern.html',
})

export class DialogPatternGuideComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-guided-flower-identification',
  templateUrl: 'dialog-guided-flower-identification.html',
})

export class DialogGuidedFlowerIdentificationComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-guided-butterfly-identification',
  templateUrl: 'dialog-guided-butterfly-identification.html',
})

export class DialogGuidedButterflyIdentificationComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
  selector: 'app-dialog-video-preference',
  templateUrl: 'dialog-video-preference.html',
})

export class DialogVideoPreferenceComponent {
  constructor(public dialogRef: MatDialogRef<DialogVideoPreferenceComponent>) {
  }
}

@Component({
  selector: 'app-dialog-video-upload',
  templateUrl: 'dialog-video-upload.html',
})

export class DialogVideoUploadComponent {
  constructor(public dialogRef: MatDialogRef<DialogVideoUploadComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    dialogRef.disableClose = true;
  }
}

@Component({
  selector: 'app-dialog-video-too-large',
  templateUrl: 'dialog-video-too-large.html',
})

export class DialogVideoTooLargeComponent {
  constructor(public dialogRef: MatDialogRef<DialogVideoTooLargeComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    dialogRef.disableClose = true;
  }
}

@Component({
  selector: 'app-dialog-butterfly-upload',
  templateUrl: 'dialog-butterfly-upload.html',
})

export class DialogButterflyUploadComponent {
  constructor(public dialogRef: MatDialogRef<DialogButterflyUploadComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {}
}
