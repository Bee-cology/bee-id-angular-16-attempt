import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BeeLog } from "../../../models/bee-log.model";
import { BeelogService } from "../../../services/beelog.service";
import { BeePicturePackage } from "../../../models/bee-picture-package.model";
import { BeePicturePackageData } from "../../../models/bee-picture-package-data.model";
import { beeCharacteristics } from "../../../models/bee-checker.model";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SnackBarInputBasic } from '../../../models/app-snackbars';
import * as _ from "lodash";

@Component({
  selector: 'app-app-home-bee-id-expert',
  templateUrl: './app-home-bee-id-expert.component.html',
  styleUrls: ['./app-home-bee-id-expert.component.scss']
})
export class AppHomeBeeIdExpertComponent implements OnInit {
  @Output() nextEmitter = new EventEmitter<any>();
  @Output() backEmitter = new EventEmitter<any>();

  beeLog: BeeLog;
  beePackage: BeePicturePackage;

  beeSpecies: string[] = [];
  options: string[] = [];
  beeSexes = ['Male', 'Female', 'Unknown'];

  constructor(
    private beelogService: BeelogService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog) {
    this.beeLog = beelogService.beeLog;
  }

  ngOnInit() {
    _.forOwn(BeePicturePackageData, (value, key) => {
      if (! _.includes(this.beeSpecies, value.name)) this.beeSpecies.push(value.name);
    });
  }

  next() {
    if (this.validateForm()) {
      this.nextEmitter.emit({'beePackage': this.beePackage, 'images': null});
    }
  }

  back() {
    this.backEmitter.emit();
  }

  validateForm() {
    if (this.beeLog.gender && this.beeLog.beeCharacteristic.beeName) {
      this.beePackage = _.find(BeePicturePackageData, o => {
        return o.name === this.beeLog.beeCharacteristic.beeName && o.gender === this.beeLog.gender;
      });
      if (this.beePackage) {
        const beeCharacteristic = _.find(beeCharacteristics, b => b.beeName === this.beePackage.packageName);
        this.beelogService.setAbdomen(beeCharacteristic.abdomen);
        this.beelogService.setHead(beeCharacteristic.head);
        this.beelogService.setThorus(beeCharacteristic.thorus);
        this.beeLog.cartoon = this.beePackage.cartoon;
      }
      else {
        this.beelogService.setAbdomen('');
        this.beelogService.setHead('');
        this.beelogService.setThorus('');
      }
      return true
    }
    else {
      this.snackBar.openFromComponent(SnackBarInputBasic, { duration: 2000, data: {'message': "Fill out all available fields.", 'icon': 'error'}, panelClass: ['sb-error']});
      return false;
    }
  }

}


