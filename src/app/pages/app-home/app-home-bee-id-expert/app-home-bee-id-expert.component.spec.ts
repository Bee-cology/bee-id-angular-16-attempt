import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHomeBeeIdExpertComponent } from './app-home-bee-id-expert.component';

describe('AppHomeBeeIdExpertComponent', () => {
  let component: AppHomeBeeIdExpertComponent;
  let fixture: ComponentFixture<AppHomeBeeIdExpertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppHomeBeeIdExpertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppHomeBeeIdExpertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
