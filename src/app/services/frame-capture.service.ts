import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class FrameCaptureService {
  // Observable File sources
  private videoSource = new Subject<File>();
  private imageSource = new Subject<File>();
  private santinizedImageSrc = new Subject<string>();

  videoSource$ = this.videoSource.asObservable();
  imageSource$ = this.imageSource.asObservable();
  santinizedImageSrc$ = this.santinizedImageSrc.asObservable();

  constructor() { }

  public getVideoSource(video: File) {
    this.videoSource.next(video);
  }

  public getImageSource(image: File) {
    this.imageSource.next(image);
  }

  public getSantinizedImageSrc(imageSrc: string) {
    this.santinizedImageSrc.next(imageSrc);
  }
}
