import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import * as _ from 'lodash';

import { Marker } from '../models/marker.model';
import { environment } from '../../environments/environment';

interface AddressResponse {
  results: Array<any>;
  status: string;
}

/**
 * Service that handles determining the current location, as well as storing a list of location markers
 */
@Injectable()
export class LocationService {
  private _lat: number;
  private _lng: number;
  private _markers: Array<Marker> = [];

  set lat(newLat: number) {
    this._lat = newLat;
  }

  get lat(): number {
    return this._lat;
  }

  set lng(newLng: number) {
    this._lng = newLng;
  }

  get lng(): number {
    return this._lng;
  }

  get markers(): Array<Marker> {
    return this._markers;
  }

  constructor(private http: HttpClient) {
  }

  /**
   * Get elevation for a location
   * @param loc Location
   */
  async getElevation(loc): Promise<string> {
    const locationUrl = `${environment.virtualEarth.baseURL}?points=${loc}&key=${environment.virtualEarth.key}`;
    return this.http.get<any>(locationUrl)
      .pipe(
        map(res => {
            console.log(res);
            if (res.statusDescription === 'OK') {
              return res.resourceSets[0].resources[0].elevations[0].toString();
            } else {
              return '';
            }
          }
        ),
        catchError(
          err => {
            return '';
          }
        )
      ).toPromise();
  }

  /**
   * Remove all stored location markers.
   */
  public clearMarker() {
    this._markers = [];
  }

  /**
   * Converts lat and lng into a string representation
   *
   * @param lat Latitude
   * @param lng Longitude
   * @returns A string representation of the latitude and longitude
   */
  public coordsToString(lat: number, lng: number) {
    if (!lat || !lng) {
      return null;
    }
    return `${lat.toFixed(6)}, ${lng.toFixed(6)}`;
  }

  /**
   * Get the city name from Google geocode address components
   *
   * @param addressComponents The address components to get the city name from
   * @returns The city name
   */
  public cityFromAddressComponents(addressComponents) {
    return _.find(addressComponents, o => _.includes(o.types, 'locality')).long_name;
  }

  /**
   * Add a marker at the given latitude and longitude
   *
   * @param lat The latitude to place the marker at
   * @param lng The longitude to place the marker at
   * @param label The color of the marker
   * @param draggable Whether or not the marker should be draggable
   * @param opacity Opacity for the marker
   * @returns The updated array of markers, with the new marker at the end.
   */
  public addMarker(lat, lng, label = '', draggable = false, opacity = 0.5, iconUrl = 'assets/offline-assets/icons/marker_red.png'): Promise<Array<Marker>> {
    return new Promise((resolve, reject) => {
      this.getLocationAddress(lat, lng)
        .subscribe(
          res => {
            const address = res[0].formatted_address;
            const marker: Marker = {
              id: this._markers.length,
              lat,
              lng,
              label,
              draggable,
              opacity,
              iconUrl,
              address,
              addressComponents: res[0].address_components
            };
            this._markers.push(marker);
            resolve(this.markers);
          },
          e => reject(e)
        );
    });
  }

  /**
   * Remove markers with the given ID
   *
   * @param id The ID of markers to remove
   */
  public removeMarker(id: number) {
    _.remove(this._markers, (m => m.id === id));
  }

  /**
   * Get the current longitude and latitude
   * Convert callback to Observable
   *
   * @returns The current position
   */
  public getCurrentLocation(): Observable<GeolocationPosition> {
    return Observable.create((sub: Observer<GeolocationPosition>) => {
      navigator.geolocation.getCurrentPosition(v => {
        sub.next(v);
        sub.complete();
      });
    });
  }

  /**
   * Get the address associated with a latitude and longitude
   * Uses the Google Maps API geocode service
   *
   * @param lat Latitude
   * @param lng Longitude
   * @param detailLevel Detail level of the address
   */
  public getLocationAddress(lat, lng, detailLevel = 0): Observable<any> {
    const locationUrl = `${environment.googleMaps.baseURL}?latlng=${lat},${lng}&sensor=false&key=${environment.googleMaps.key}`;
    return this.http.get<AddressResponse>(locationUrl)
      .pipe(
        map(res => res.results)
      );
  }

  /**
   * Get the address associated with a query string
   * Uses the Google Maps API geocode service
   *
   * @param searchString text query string
   */
  public getLocationLatLng(searchString): Observable<any> {
    const locationUrl = `${environment.googleMaps.baseURL}?address=${searchString}&key=${environment.googleMaps.key}`;
    return this.http.get<AddressResponse>(locationUrl)
      .pipe(
        map(res => res.results)
      );
  }
}
