import { Injectable } from '@angular/core';
import { BeeLog } from '../models/bee-log.model';
import { ButterflyLog } from '../models/butterfly-log.model';
import { BeeCharacteristic } from '../models/bee-characteristic.model';
import { ButterflyCharacteristic } from '../models/butterfly-characteristic.model';

enum Color {
  MOST_BLACK, HALF_BLACK, MOST_NOT_BLACK
}

/**
 * Essentially just acts like a global variable that lets different components access information about the information that the user
 * provides during the bee id process
 */
@Injectable()
export class BeelogService {
  private _beeLog: BeeLog;
  private _butterflyLog: ButterflyLog;
  private _isBee: boolean;

  set beeLog(b: BeeLog) {
    this._beeLog = b;
  }

  get beeLog(): BeeLog {
    return this._beeLog;
  }

  constructor() {
    this._beeLog = new BeeLog();
    this._beeLog.beeCharacteristic = new BeeCharacteristic();

    this._butterflyLog = new ButterflyLog();
    this._butterflyLog.butterflyCharacteristic = new ButterflyCharacteristic();
  }

  public getIsBee() {
    return this._isBee;
  }

  public setIsBee(isBee: boolean) {
    this._isBee = isBee;
  }

  public setNearestTown(city: string) {
    this._beeLog.city = city;
  }

  public getAbdomen() {
    return this._beeLog.beeCharacteristic.abdomen;
  }

  public setColor(color: Color) {
    this._beeLog.beeCharacteristic.color = color;
  }

  public setAbdomen(abdomen) {
    this._beeLog.beeCharacteristic.abdomen = abdomen;
  }

  public setThorus(thorus) {
    this._beeLog.beeCharacteristic.thorus = thorus;
  }

  public setHead(head) {
    this._beeLog.beeCharacteristic.head = head;
  }

  public setImgSrc(imgs: string[]) {
    this._beeLog.beeCharacteristic.imgSrc = imgs;
  }

  public setVideoSrc(video: string) {
    this._beeLog.videoPath = video;
  }

  public setImgIndex(imgIndex: string[][]) {
    this._beeLog.beeCharacteristic.imgIndex = imgIndex;
  }

  set butterflyLog(b: ButterflyLog) {
    this._butterflyLog = b;
  }

  get butterflyLog(): ButterflyLog {
    return this._butterflyLog;
  }


  public setNearestTownButterfly(city: string) {
    this._butterflyLog.city = city;
  }

  /*public getAbdomen() {
    return this._beeLog.beeCharacteristic.abdomen;
  }

  public setColor(color: Color) {
    this._beeLog.beeCharacteristic.color = color;
  }

  public setAbdomen(abdomen) {
    this._beeLog.beeCharacteristic.abdomen = abdomen;
  }

  public setThorus(thorus) {
    this._beeLog.beeCharacteristic.thorus = thorus;
  }

  public setHead(head) {
    this._beeLog.beeCharacteristic.head = head;
  }*/

  public setButterflyName(name: string) {
    this._butterflyLog.butterflyCharacteristic.butterflyName = name;
  }

  public setImgSrcButterfly(imgs: string[]) {
    this._butterflyLog.butterflyCharacteristic.imgSrc = imgs;
  }

  public setVideoSrcButterfly(video: string) {
    this._butterflyLog.videoPath = video;
  }

  public setImgIndexButterfly(imgIndex: string[][]) {
    this._butterflyLog.butterflyCharacteristic.imgIndex = imgIndex;
  }

  /**
   * Get the abdomen options available given the currently selected bee color
   * @returns The available abdomen options
   */
  public getAbdomens(): string[] {
    switch (this._beeLog.beeCharacteristic.color) {
      case Color.MOST_BLACK:
        return this._beeLog.beeCharacteristic.ABDOMEN_MOST_BLACK;
      case Color.HALF_BLACK:
        return this._beeLog.beeCharacteristic.ABDOMEN_HALF_BLACK;
      case Color.MOST_NOT_BLACK:
        return this._beeLog.beeCharacteristic.ABDOMEN_MOST_NOT_BLACK;
    }
  }

  /**
   * Get the head options available
   * @returns The available head options
   */
  public getHeader(): string[] {
    return this._beeLog.beeCharacteristic.HEADER;
  }

  /**
   * Get the thorus options available given the currently selected bee color and abdomen kind
   * @returns The available thorus options
   */
  public getThorus(): string[] {
    switch (this._beeLog.beeCharacteristic.color) {
      case Color.MOST_BLACK:
        if (this.getAbdomen() === 'a1') {
          return ['t2'];
        }
        return this._beeLog.beeCharacteristic.THORUS_MOST_BLACK;
      case Color.HALF_BLACK:
        return this._beeLog.beeCharacteristic.THORUS_HALF_BLACK;
      case Color.MOST_NOT_BLACK:
        return this._beeLog.beeCharacteristic.THORUS_MOST_NOT_BLACK;
    }
  }
}
