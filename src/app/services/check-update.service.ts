import { Component, Inject, Injectable } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SwUpdate } from '@angular/service-worker';

@Injectable({
  providedIn: 'root'
})

export class CheckUpdateService {
  constructor(private swUpdate: SwUpdate, private snackbar: MatSnackBar, private dialog: MatDialog) {
	// ERROR: swUpdate.available is deprecated, need a new way to do this
	// todo: FOR NOW, JUST COMMENTING IT OUT
	console.log('checkUpdateService needs repairs. old version used deprecated stuff.');
    // swUpdate.available.subscribe((event) => {
    //   console.log('Update found!');
    //   const dialogRef = this.dialog.open(DialogAppUpdateComponent);
    //   document.body.classList.add('dialog-open');
    //   dialogRef.afterClosed().subscribe(() => {
    //     document.body.classList.remove('dialog-open');
    //     window.location.reload();
    //   });
    // });

      /*
      const snack = this.snackbar.open('There is a new version of webapp available.', 'Reload');

      snack
        .onAction()
        .subscribe(() => {
          window.location.reload();
        });
    });
       */
  }

  checkForUpdate() {
    console.log('Checking for update...');
    this.swUpdate.checkForUpdate().then(r => console.log(r));
  }
}

@Component({
  selector: 'app-dialog-app-update',
  templateUrl: 'dialog-app-update.html',
})

export class DialogAppUpdateComponent {
  constructor(private dialogRef: MatDialogRef<DialogAppUpdateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    dialogRef.disableClose = true;
  }
}
