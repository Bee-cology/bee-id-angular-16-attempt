import { Injectable } from '@angular/core';
// @ts-ignore
import PouchDB from 'pouchdb';
import { Subject} from 'rxjs';
import { BeeLog } from '../models/bee-log.model';
import { BeecologyAuthService } from './authentication/beecology-auth.service';
import { BeeLogRecord } from '../models/bee-log-record.model';
import { ButterflyLog } from '../models/butterfly-log.model';
import { ButterflyLogRecord } from '../models/butterfly-log-record.model';
import { LocationService } from './location.service';
import {
  BeeRecord, BeeSpecies, ButterflyRecord, ButterflySpecies, DefaultService,
  FlowerSpecies, MediaInformation, ReferenceService } from '../api-module';

import WingShapeEnum = ButterflyRecord.WingShapeEnum;
import TailsEnum = ButterflyRecord.TailsEnum;
import AntennaeEnum = ButterflyRecord.AntennaeEnum;

import GenderEnum = BeeRecord.GenderEnum;
import { environment } from '../../environments/environment';

@Injectable()
export class DatabaseService {
  private readonly db: PouchDB.Database<BeeLogRecord>;
  private readonly dbButterfly: PouchDB.Database<ButterflyLogRecord>;

  logSource = new Subject<any>();
  logSource$ = this.logSource.asObservable();

  logSourceButterfly = new Subject<any>();
  logSourceButterfly$ = this.logSourceButterfly.asObservable();

  constructor(
    private auth: BeecologyAuthService,
    private locationService: LocationService,
    private referenceService: ReferenceService,
    private apiService: DefaultService
  ) {
    this.db = new PouchDB('logs', {
      size: 50,
      auto_compaction: true
    });

    // purges deleted logs created before the update enabling "auto_compaction"
    // https://pouchdb.com/guides/compact-and-destroy.html
    this.db.compact();

    this.dbButterfly = new PouchDB('butterflylogs', {
      size: 50,
      auto_compaction: true
    });

    this.dbButterfly.compact();
  }

  /**
   * Upload a single bee log to the server
   * @param log The BeeLog to upload
   * @returns An observable that emits the log after uploading it to the server and deleting it from the local database
   */
  async postLog(log: PouchDB.Core.Document<BeeLogRecord>): Promise<PouchDB.Core.Response> {
    const isButterfly = (log.beename === 'Butterfly');
    const elevation: number = parseFloat(await this.locationService.getElevation(log.loc));

    // Convert some fields from old-style API fields to new-style API fields. Namely this means species IDs and elevations.
    const beeSpecies: BeeSpecies = isButterfly ? null : (await this.referenceService.getBees([log.beename.split(' ')[1]]).toPromise())[0];
    const flowerSpecies: FlowerSpecies = log.fname ? (await this.referenceService.getFlowers([log.fname.split(' ')[0]],
      [log.fname.split(' ')[1]]).toPromise())[0] : null;
    if (!beeSpecies || !flowerSpecies) {
      console.log('Failed to match bee or flower species with server records', log);
    }
    const location = log.loc.split(' ').map(parseFloat);

    const behavior = Object.values(BeeRecord.BehaviorEnum)[log.beebehavior];

    const log_upload: BeeRecord = {
      bee_species_id: beeSpecies ? beeSpecies.id : null,
      flower_species_id: flowerSpecies ? flowerSpecies.id : null,
      behavior: behavior ? behavior : null,
      gender:
        (!isButterfly && log.gender !== null && log.gender.toLowerCase() !== 'unknown') ? log.gender.toLowerCase() as GenderEnum : null,
      head_coloration: log.chead,
      thorax_coloration: log.cthorax,
      abdomen_coloration: log.cabdomen,
      closest_city: log.cityname,
      location: {
        latitude: location[0],
        longitude: location[1]
      },
      elevation: elevation,
      time: log.time as any,
      how_submitted: 'webapp',
      media: [],
      user_comments: log.userComments,
      flower_species_guess_array: log.flowerSpeciesGuessArray
    };
    // Delete anything null before upload
    Object.keys(log_upload).forEach((key) => (log_upload[key] == null) && delete log_upload[key]);

    // If the log has images, upload them
    if (log.recordpicpath) {
      const image_urls = [];
      for (const url of log.recordpicpath) {

        const info: MediaInformation = await this.apiService.postMedia({data: url.split(',')[1]}).toPromise();
        image_urls.push(info.path);
      }
      log_upload.media = image_urls;
    }

    // If the log has a video, upload it
    if (log.recordvidpath) {
      const url = log.recordvidpath;
      const info: MediaInformation = await this.apiService.postMedia({data: url.split(',')[1]}).toPromise();
      if (isButterfly) {
        log_upload.media.push(info.id);
      } else {
        log_upload.media.push(info.path);
      }
    }

    if(log.recordvidpath && (log.flowerSpeciesGuessArray == null || log.flowerSpeciesGuessArray.length < 1)){
      await this.resizeSendImage(log.recordpicpath[3],true);
      await waitTime(500); // wait needed

       log_upload.flower_species_guess_array = flowersResponseSpecies;
       log.flowerSpeciesGuessArray = flowersResponseSpecies;
    }
   else if((log.flowerSpeciesGuessArray == null )){//if is image, not video
      log_upload.flower_species_guess_array = [];
    }

    console.log('log_upload.media');
    console.log(log_upload.media);
    try {
      /*if (isButterfly) {
        console.log('bb');
        console.log(log_upload);
        const butterfly_log_upload: ButterflyRecord = {
          closest_city: log.cityname,
          location: {
            latitude: location[0],
            longitude: location[1]
          },
          elevation: elevation,
          time: log.time as any,
          submitted: log.time as any,
          media_id: log_upload.media[0]
        };
        const response: string = await this.apiService.postButterflyRecord(butterfly_log_upload).toPromise();
        // If the log was successfully uploaded to the server, delete it from the local db
        if (response) {
          const doc = await this.db.remove(await this.db.get(log._id));
          this.logSource.next(await this.getLocalLogs());
          return doc;
        }
      } else {*/
        const response: string = await this.apiService.postRecord(log_upload).toPromise();
        // If the log was successfully uploaded to the server, delete it from the local db
        if (response) {
          const doc = await this.db.remove(await this.db.get(log._id));
          this.logSource.next(await this.getLocalLogs());
          return doc;
        }
      // }////
    } catch (e) {
      console.log('Failed to submit bee log to server!', e);
    }
  }

  /**
   * Upload the given log or logs to the server
   * @param logs The logs to upload to the server. Can be a single log or an array of logs.
   * @returns A Promise resolving to the uploaded documents.
   */
  postLogs(logs: Array<PouchDB.Core.ExistingDocument<BeeLogRecord>> | PouchDB.Core.ExistingDocument<BeeLogRecord>):
    Promise<Array<PouchDB.Core.Response>> {
    if (Array.isArray(logs)) {
      return Promise.all(logs.map((log) => this.postLog(log)));
    } else {
      return this.postLog(logs).then((res) => [res]);
    }
  }

  /**
   * Add a log to the local database
   * @param log The log to add to the database
   * @returns A Promise resolving to a success string if successful, or an error otherwise
   */
  addLogs(log: BeeLog): Promise<PouchDB.Core.Response> {
    const entry = {
      _id: new Date().toISOString(),
      chead: log.beeCharacteristic.head,
      cabdomen: log.beeCharacteristic.abdomen,
      cthorax: log.beeCharacteristic.thorus,
      gender: log.gender,
      loc: log.location,
      cityname: log.city,
      fname: log.flowerName,
      ffname: log.flowerFamilyName,
      fcname: log.flowerCommonName,
      fshape: log.flowerShape,
      lifecycle: log.flowerLifeCycle,
      fcolor: log.flowerColor,
      beename: log.beeCharacteristic.beeName,
      time: log.date,
      beedictid: new Date().toISOString(),
      beebehavior: log.beeBehavior,
      recordpicpath: log.beeCharacteristic.imgSrc,
      recordvidpath: log.videoPath,
      recordpicindex: log.beeCharacteristic.imgIndex,
      cartoon: log.cartoon,
      hasLocation: log.hasLocation,
      appVersion: environment.currentAppVersion,
      userComments: log.userComments,
      flowerSpeciesGuessArray: log.flowerSpeciesGuessArray
    };
    return this.db.put(entry);
  }

  updateLocation(log: PouchDB.Core.ExistingDocument<BeeLogRecord>,
                 hasLocation: boolean,
                 loc: string,
                 cityname: string): Promise<PouchDB.Core.Response> {
    const entry = {
      _id: log._id,
      chead: log.chead,
      cabdomen: log.cabdomen,
      cthorax: log.cthorax,
      gender: log.gender,
      loc: loc,
      cityname: cityname,
      fname: log.fname,
      ffname: log.ffname,
      fcname: log.fcname,
      fshape: log.fshape,
      lifecycle: log.lifecycle,
      fcolor: log.fcolor,
      beename: log.beename,
      time: log.time,
      beedictid: log.beedictid,
      beebehavior: log.beebehavior,
      recordpicpath: log.recordpicpath,
      recordvidpath: log.recordvidpath,
      recordpicindex: log.recordpicindex,
      cartoon: log.cartoon,
      appVersion: log.appVersion,
      hasLocation: hasLocation,
      _rev: log._rev,
      userComments: log.userComments,
      flowerSpeciesGuessArray: log.flowerSpeciesGuessArray
    };
    return this.db.put(entry);
  }

  updateFlower(log: PouchDB.Core.ExistingDocument<BeeLogRecord>,
               fname: string,
               ffname: string,
               fcname: string,
               fshape: string,
               lifecycle: string,
               fcolor: string): Promise<PouchDB.Core.Response> {
      const entry = {
        _id: log._id,
        chead: log.chead,
        cabdomen: log.cabdomen,
        cthorax: log.cthorax,
        gender: log.gender,
        loc: log.loc,
        cityname: log.cityname,
        fname: fname,
        ffname: ffname,
        fcname: fcname,
        fshape: fshape,
        lifecycle: lifecycle,
        fcolor: fcolor,
        beename: log.beename,
        time: log.time,
        beedictid: log.beedictid,
        beebehavior: log.beebehavior,
        recordpicpath: log.recordpicpath,
        recordvidpath: log.recordvidpath,
        recordpicindex: log.recordpicindex,
        cartoon: log.cartoon,
        appVersion: log.appVersion,
        hasLocation: log.hasLocation,
        _rev: log._rev,
        userComments: log.userComments,
        flowerSpeciesGuessArray: log.flowerSpeciesGuessArray
      };
      return this.db.put(entry);
    }

  /**
   * Get all the logs currently in the database
   * @returns A Promise resolving to a list of the logs in the local database if successful, or an error otherwise
   */
  getLocalLogs(): Promise<Array<PouchDB.Core.ExistingDocument<BeeLogRecord>>> {
    return this.db.allDocs({include_docs: true, descending: true}).then((doc) => {
      return doc.rows.map((row) => row.doc);
    });
  }

  /**
   * Delete a log from the database
   * @param log The log to delete from the database
   * @returns A Promise resolving to void if successful, or an error otherwise
   */
  deleteLog(log: PouchDB.Core.ExistingDocument<BeeLogRecord>): Promise<void> {
    return new Promise((resolve, reject) => {
      this.db.remove(log).then(() => {
          resolve();
          this.getLocalLogs().then(data => {
            this.logSource.next(data);
          });
        })
        .catch(err => reject(err));
    });
  }
  // ------------------------------------
  /**
   * Upload a single butterfly log to the server
   * @param log The ButterflyLog to upload
   * @returns An observable that emits the log after uploading it to the server and deleting it from the local database
   */

   async postLogButterfly(log: PouchDB.Core.Document<ButterflyLogRecord>): Promise<PouchDB.Core.Response> {

    const elevation: number = parseFloat(await this.locationService.getElevation(log.loc));

    // Convert some fields from old-style API fields to new-style API fields. Namely this means species IDs and elevations.
    const butterflySpecies: ButterflySpecies = (await this.referenceService.getButterflies([log.butterflyname]
    ).toPromise())[0];
    const flowerSpecies: FlowerSpecies = log.fname ? (await this.referenceService.getFlowers([log.fname.split(' ')[0]],
      [log.fname.split(' ')[1]]).toPromise())[0] : null;

      // console.log((await this.referenceService.getFlowers([log.fname.split(' ')[0]],
      // [log.fname.split(' ')[1]]).toPromise())[0]);//remove this when done TODO
    if (!butterflySpecies || !flowerSpecies) {
      console.log('Failed to match butterfly or flower species with server records', log);
    }
    const location = log.loc.split(' ').map(parseFloat);

    const behavior = Object.values(ButterflyRecord.BehaviorEnum)[log.butterflybehavior];

    console.log('Behavior: ');
    console.log(behavior);
    const log_upload: ButterflyRecord = {
      // id: butterflySpecies ? butterflySpecies.id : null,
      //butterfly_species: butterflySpecies ? butterflySpecies : null,
      butterfly_species_id: butterflySpecies ? butterflySpecies.id : null,
      //flower_species: flowerSpecies ? flowerSpecies : null,//BIG PROBLEM, WE NEED TO REMOVE THIS LINE
      flower_species_id: flowerSpecies ? flowerSpecies.id : null,

      butterfly_behavior: behavior ? behavior : null,

      closest_city: log.cityname,
      location: {
        latitude: location[0],
        longitude: location[1]
      },
      elevation: elevation,
      time: log.time as any,
      submitted: 'webapp',
      how_submitted: 'webapp',
      species_top_guess: log.speciesTopGuess,
      media: [], // check functionality
      user_comments: log.userComments,
      species_guess_array: log.speciesGuessArray,
      flower_species_guess_array: log.flowerSpeciesGuessArray,
      dorsalPattern: log.dorsalPattern,
      ventralPattern: log.ventralPattern,
      gender: null, // log.gender, // we don't know how to record this

      tails: (log.tails) ? (TailsEnum[log.tails]) : null,
      wing_size: (log.wingSize) ? (TailsEnum[log.wingSize]) : null,
      wing_shape: (log.wingShape) ? (WingShapeEnum[log.wingShape]) : null,
      antennae: (log.antennae) ? (AntennaeEnum[log.antennae]) : null,
      dorsalPrimaryColor: log.dorsalPrimaryColor,
      dorsalSecondaryColor: log.dorsalSecondaryColor,
      ventralPrimaryColor: log.ventralPrimaryColor,
      ventralSecondaryColor: log.ventralSecondaryColor,



    };
    // Delete anything null before upload
    Object.keys(log_upload).forEach((key) => (log_upload[key] == null) && delete log_upload[key]);

    // If the log has images, upload them
    if (log.recordpicpath) {
      const image_urls = [];
      for (const url of log.recordpicpath) {

        const info: MediaInformation = await this.apiService.postMedia({data: url.toString().split(',')[1]}).toPromise();
        image_urls.push(info.path);
      }

      if (log.speciesTopGuess == null) { // if no response has already been received
       await this.resizeSendImage(log.recordpicpath[0],false);
       await waitTime(500); // wait needed
       console.log('iNaturalistFirstGuess: ' + iNaturalistFirstGuess);
       console.log('butterfliesResponseSpecies: ' + butterfliesResponseSpecies);
        log_upload.species_top_guess = iNaturalistFirstGuess;
        log_upload.species_guess_array = butterfliesResponseSpecies;//
        log.speciesTopGuess = iNaturalistFirstGuess;
        log.speciesGuessArray = butterfliesResponseSpecies;

      }
      if(log.flowerSpeciesGuessArray == null || log.flowerSpeciesGuessArray.length < 1) {
        await this.resizeSendImage(log.recordpicpath[1],true);
        await waitTime(500); // wait needed

         log_upload.flower_species_guess_array = flowersResponseSpecies;
         log.flowerSpeciesGuessArray = flowersResponseSpecies;
      }
      log_upload.media = image_urls; // media must be of type string hack fix (original without [0]) NEEDED
    }

 // media must be of type string hack fix
    // If the log has a video, upload it
    if (log.recordvidpath) {
      const url = log.recordvidpath;
      await this.apiService.postMedia({data: url.split(',')[1]}).toPromise();
      /*
      if (isButterfly) {
       log_upload.media.push(info.id);
      } else {
        log_upload.media.push(info.path);//needed
      }
       */
    }

    try {
        const response: string = await this.apiService.postButterflyRecord(log_upload).toPromise();

        // If the log was successfully uploaded to the server, delete it from the local db
        if (response) {
          const doc = await this.dbButterfly.remove(await this.dbButterfly.get(log._id));
          this.logSourceButterfly.next(await this.getLocalLogsButterfly()); // this is the fix but when i went to commit already here
          return doc;
        }

    } catch (e) {
      console.log('Failed to submit butterfly log to server! (oops)', e);
    }
  }

  /** Butterfly
   * Upload the given log or logs to the server
   * @param logs The logs to upload to the server. Can be a single log or an array of logs.
   * @returns A Promise resolving to the uploaded documents.
   */
  postLogsButterfly(logs: Array<PouchDB.Core.ExistingDocument<ButterflyLogRecord>> | PouchDB.Core.ExistingDocument<ButterflyLogRecord>):
    Promise<Array<PouchDB.Core.Response>> {
    if (Array.isArray(logs)) {
      return Promise.all(logs.map((log) => this.postLogButterfly(log)));
    } else {
      return this.postLogButterfly(logs).then((res) => [res]);
    }
  }

  /** butterfly
   * Add a log to the local database
   * @param log The log to add to the database
   * @returns A Promise resolving to a success string if successful, or an error otherwise
   */
  addLogsButterfly(log: ButterflyLog): Promise<PouchDB.Core.Response> {

    const entry = {
      _id: new Date().toISOString(),

      loc: log.location,
      cityname: log.city,
      fname: log.flowerName,
      ffname: log.flowerFamilyName,
      fcname: log.flowerCommonName,
      fshape: log.flowerShape,
      lifecycle: log.flowerLifeCycle,
      fcolor: log.flowerColor,
      butterflyname: (log.butterflyCharacteristic.butterflyName) ? log.butterflyCharacteristic.butterflyName : 'Unknown',
      time: log.date,
      butterflyid: new Date().toISOString(),
      butterflybehavior: log.butterflyBehavior,
      recordpicpath: log.butterflyCharacteristic.imgSrc,
      recordvidpath: log.videoPath,
      recordpicindex: log.butterflyCharacteristic.imgIndex,
      // cartoon: log.cartoon,
      hasLocation: log.hasLocation,
      appVersion: environment.currentAppVersion,
      userComments: log.userComments,
      wingSize: log.wingSize,
      wingShape: log.wingShape,
      antennae: log.antennae,
      dorsalPattern: log.dorsalPattern,
      ventralPattern: log.ventralPattern,
      dorsalPrimaryColor: log.dorsalPrimaryColor,
      dorsalSecondaryColor: log.dorsalSecondaryColor,
      ventralPrimaryColor: log.ventralPrimaryColor,
      ventralSecondaryColor: log.ventralSecondaryColor,
      tails: log.tails,
      speciesGuessArray: log.speciesGuessArray,
      flowerSpeciesGuessArray: log.flowerSpeciesGuessArray
    };
    return this.dbButterfly.put(entry);
  }

  // butterfly
  updateLocationButterfly(log: PouchDB.Core.ExistingDocument<ButterflyLogRecord>,
                 hasLocation: boolean,
                 loc: string,
                 cityname: string): Promise<PouchDB.Core.Response> {
    const entry = {
      _id: log._id,
      loc: loc,
      cityname: cityname,
      fname: log.fname,
      ffname: log.ffname,
      fcname: log.fcname,
      fshape: log.fshape,
      lifecycle: log.lifecycle,
      fcolor: log.fcolor,
      butterflyname: log.butterflyname,
      time: log.time,
      butterflyid: log.butterflyid,
      butterflybehavior: log.butterflybehavior,
      recordpicpath: log.recordpicpath,
      recordvidpath: log.recordvidpath,
      recordpicindex: log.recordpicindex,
      // cartoon: log.cartoon,
      appVersion: log.appVersion,
      hasLocation: hasLocation,
      _rev: log._rev,
      userComments: log.userComments,
      wingSize: log.wingSize,
      wingShape: log.wingShape,
      antennae: log.antennae,
      dorsalPattern: log.dorsalPattern,
      ventralPattern: log.ventralPattern,
      dorsalPrimaryColor: log.dorsalPrimaryColor,
      dorsalSecondaryColor: log.dorsalSecondaryColor,
      ventralPrimaryColor: log.ventralPrimaryColor,
      ventralSecondaryColor: log.ventralSecondaryColor,
      tails: log.tails,
      speciesGuessArray: log.speciesGuessArray,
      flowerSpeciesGuessArray: log.flowerSpeciesGuessArray

    };
    return this.dbButterfly.put(entry);
  }

  // butterfly
  updateFlowerButterfly(log: PouchDB.Core.ExistingDocument<ButterflyLogRecord>,
               fname: string,
               ffname: string,
               fcname: string,
               fshape: string,
               lifecycle: string,
               fcolor: string): Promise<PouchDB.Core.Response> {
      const entry = {
        _id: log._id,

        loc: log.loc,
        cityname: log.cityname,
        fname: fname,
        ffname: ffname,
        fcname: fcname,
        fshape: fshape,
        lifecycle: lifecycle,
        fcolor: fcolor,
        butterflyname: log.butterflyname,
        time: log.time,
        butterflyid: log.butterflyid,
        butterflybehavior: log.butterflybehavior,
        recordpicpath: log.recordpicpath,
        recordvidpath: log.recordvidpath,
        recordpicindex: log.recordpicindex,
        // cartoon: log.cartoon,
        appVersion: log.appVersion,
        hasLocation: log.hasLocation,
        _rev: log._rev,
        userComments: log.userComments,
        wingSize: log.wingSize,
        wingShape: log.wingShape,
        antennae: log.antennae,
        dorsalPattern: log.dorsalPattern,
        ventralPattern: log.ventralPattern,
        dorsalPrimaryColor: log.dorsalPrimaryColor,
        dorsalSecondaryColor: log.dorsalSecondaryColor,
        ventralPrimaryColor: log.ventralPrimaryColor,
        ventralSecondaryColor: log.ventralSecondaryColor,
        tails: log.tails,
        speciesGuessArray: log.speciesGuessArray,
        flowerSpeciesGuessArray: log.flowerSpeciesGuessArray
      };
      return this.dbButterfly.put(entry);
    }

  /** Butterfly
   * Get all the logs currently in the database
   * @returns A Promise resolving to a list of the logs in the local database if successful, or an error otherwise
   */
  getLocalLogsButterfly(): Promise<Array<PouchDB.Core.ExistingDocument<ButterflyLogRecord>>> {
    return this.dbButterfly.allDocs({include_docs: true, descending: true}).then((doc) => {
      return doc.rows.map((row) => row.doc);
    });
  }

  /** Butterfly
   * Delete a log from the database
   * @param log The log to delete from the database
   * @returns A Promise resolving to void if successful, or an error otherwise
   */
  deleteLogButterfly(log: PouchDB.Core.ExistingDocument<ButterflyLogRecord>): Promise<void> {
    return new Promise((resolve, reject) => {
      this.dbButterfly.get(log._id).then(doc => {
        return this.dbButterfly.remove(doc);
      })
        .then(doc => {
          resolve();
          this.getLocalLogsButterfly().then(data => {
            this.logSourceButterfly.next(data);
          });
        })
        .catch(err => reject(err));
    });
  }


  resizeSendImage(resizesendsrc, isFlower) {
    const i = new Image();

    i.onload = function() {
      console.log( ' ' );
    };

    // i.src = this.source.file;
    i.src = resizesendsrc; // using image rather than video
    let newWidth = 300;
    let newHeight = 300;

    if (i.width > i.height) {
      newHeight = Math.round(300 * (i.height / i.width));
    } else if (i.width < i.height) {
      newWidth = Math.round(300 * (i.width / i.height));
    }

    console.log( 'Modified image size: ' + newWidth + ', ' + newHeight );
    console.log( 'Modified src: ' + resizesendsrc);
    // compressImage( this.source.file, newWidth, newHeight ).then( (blob:Blob )=> {
    compressImage( resizesendsrc, newWidth, newHeight ).then( (blob: Blob ) => {
      console.log( 'compressed');
    const data = new FormData( );

    data.append( 'image', blob);

    // data.append( 'taxon_id', '47126'); // looking for any plant
    if(isFlower){
      data.append( 'taxon_id', '47125'); // looking for a flower
    }
    else{
      data.append( 'taxon_id', '47157'); // looking for a butterfly
    }
      // tslint:disable-next-line:max-line-length
    // taxon_id can be found at https://www.inaturalist.org/taxa by searching for the taxa of interest. The id is a five digit number in the URL

    const xhr = new XMLHttpRequest( );
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {

        console.log( this.responseText);
        // tslint:disable-next-line:max-line-length
        // {"results":[{"taxon":{"id":48662,"name":"Danaus plexippus","preferred_common_name":"Monarch"},"visually_similar":true},{"taxon":{"id":146923,"name":"Danaus genutia","preferred_common_name":"Common Tiger"},"visually_similar":true},{"taxon":{"id":126299,"name":"Danaus erippus","preferred_common_name":"South American Monarch"},"visually_similar":true},{"taxon":{"id":58586,"name":"Limenitis archippus","preferred_common_name":"Viceroy"},"visually_similar":true},{"taxon":{"id":49150,"name":"Dione vanillae","preferred_common_name":"Gulf Fritillary"},"visually_similar":true},{"taxon":{"id":67835,"name":"Danaus chrysippus","preferred_common_name":"Plain Tiger Butterfly"},"visually_similar":true},{"taxon":{"id":180386,"name":"Acraea terpsicore","preferred_common_name":"Tawny Coster"},"visually_similar":true},{"taxon":{"id":319651,"name":"Acraea serena","preferred_common_name":"Small Orange Acraea"},"visually_similar":true},{"taxon":{"id":546406,"name":"Acraea rahira","preferred_common_name":"Marsh Acraea"},"visually_similar":true},{"taxon":{"id":83834,"name":"Eueides isabella","preferred_common_name":"Isabella's Longwing"},"visually_similar":true}],"suggested_ancestor":{"id":48663,"name":"Danaus","preferred_common_name":"Tiger Milkweed Butterflies"}}


        readResponseToVars(this.responseText, isFlower);
        console.log('iNaturalist response received: ' + responseRecieved);
      }
    });

    xhr.open('POST', 'https://visionapi.p.rapidapi.com/v1/rapidapi/score_image', false);
    xhr.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    xhr.setRequestHeader('x-rapidapi-host', 'visionapi.p.rapidapi.com');
    xhr.setRequestHeader('x-rapidapi-key', 'b7ddc20751msh765abd135dc9500p125e28jsnbbcf6a8da233');

    xhr.send(data); // temp
    responseRecieved = false;
    });
  }
}


// copied from elsewhere
// these are globals

const butterfliesResponseSpecies = [];
const flowersResponseSpecies = [];

let iNaturalistFirstGuess = null;
let responseRecieved = false;

function compressImage( src, newX, newY ) {
  return new Promise( ( res, rej ) => {
    const img = new Image();
    img.src = src;
    img.onload = ( ) => {
      const elem = document.createElement( 'canvas' );
      elem.width = newX;
      elem.height = newY;
      const ctx = elem.getContext( '2d' );
      ctx.drawImage( img, 0, 0, newX, newY );
      const data = ctx.canvas.toBlob(res);
    };
    img.onerror = error => rej( error );
  } );
}

function blobFromURI(inputURI) {

  let binaryVal;

  // mime extension extraction
  const inputMIME = inputURI.split(',')[0].split(':')[1].split(';')[0];

  // Extract remaining part of URL and convert it to binary value
  if (inputURI.split(',')[0].indexOf('base64') >= 0) {
      binaryVal = atob(inputURI.split(',')[1]);
  } else {
      binaryVal = unescape(inputURI.split(',')[1]);
  }

  // Computation of new string in which hexadecimal
  // escape sequences are replaced by the character
  // it represents

  // Store the bytes of the string to a typed array
  const blobArray = [];
  for (let index = 0; index < binaryVal.length; index++) {
      blobArray.push(binaryVal.charCodeAt(index));
  }

  return new Blob(blobArray, {
      type: inputMIME
  });
}

  function readResponseToVars(input, isFlower) {

    const json = JSON.parse(input);

    const results = [];

    for (let i = 0; i < json.results.length; i++) { // 1?

      results.push(json.results[i].taxon.name);
    }

      console.log(results);
      if(!isFlower){
      butterfliesResponseSpecies.push.apply([], results);
      if (butterfliesResponseSpecies[0] != null) {

      iNaturalistFirstGuess = butterfliesResponseSpecies[0];
      } else {

        iNaturalistFirstGuess = 'Unknown';
      }
    }
    else{
      flowersResponseSpecies.push.apply([], results);
    }
      responseRecieved = true;

    return results;
    }

    function waitTime(ms: number) {
      return new Promise( resolve => setTimeout(resolve, ms) );
  }
