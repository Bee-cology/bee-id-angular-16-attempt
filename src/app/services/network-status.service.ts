import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { switchMap, filter } from 'rxjs/operators';
import { fromEvent } from 'rxjs-compat/observable/fromEvent';
import { ReferenceService } from '../api-module';

@Injectable()
export class NetworkStatusService {
  private offline$: Observable<boolean>;
  private online$: Observable<boolean>;

  constructor(private referenceService: ReferenceService) {
      this.online$ = fromEvent(window, 'online').pipe(switchMap(this.pingServer), filter((e) => e));
      this.offline$ = fromEvent(window, 'offline').pipe(switchMap(this.pingServer), filter((e) => !e));
  }

  /**
   * Attempt a get request to the isConnected endpoint of the Beecology API
   *
   * @returns True if connected, false otherwise
   */
  async pingServer(): Promise<boolean> {
    try {
      // Request a non-existent bee from the server. The server will immediately 404 in response if we're connected, otherwise the
      // request will simply fail.
      await this.referenceService.getBee('beeeeeeeeee');
      return true;
    } catch (e) {
      console.log(e);
      return true;
    }
  }

  /**
   * An Observable that reports network connectivity any time the browser reports a connection change
   * Reports true if we can access the beecology API, false otherwise
   */
  public get isOnline() {
      return this.online$;
  }

  /**
   * An Observable that reports network connectivity any time the browser reports a connection change
   * Reports false if we can access the beecology API, true otherwise
   */
  public get isOffline() {
      return this.offline$;
  }

  /**
   * Check if we can access the beecology API
   *
   * @returns True if online, false otherwise
   */
  public async checkOnline(): Promise<boolean> {
      return this.pingServer();
  }
}
