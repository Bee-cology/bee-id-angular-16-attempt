import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { FirebaseAuthService } from './firebaseauth.service';
import { Router } from '@angular/router';
// import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/compat/app';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Subject } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { DefaultService, JWT } from '../../api-module';

export const STORAGE_JWT = 'beeAuthJWT';
const EXPIRY_THRESHOLD_IN_MILLIS = 30000;

@Injectable()
export class BeecologyAuthService {
  private firebaseAuth: FirebaseAuthService;
  private persist = false;
  private isLoggedInSubject: Subject<boolean>;
  $isLoggedIn: Observable<boolean>;

  constructor(
    _firebaseAuth: AngularFireAuth,
    router: Router,
    private authApi: DefaultService,
    private storage: LocalStorageService,
    private session: SessionStorageService) {

    this.isLoggedInSubject = new Subject<boolean>();
    this.$isLoggedIn = this.isLoggedInSubject.asObservable();
    this.firebaseAuth = new FirebaseAuthService(_firebaseAuth, router);
    this.firebaseAuth.user.subscribe(
      (user) => {
        if (user) {
          this.firebaseAuth.userDetails = user;
          this.enrollDevice().then(() => {
            this.isLoggedInSubject.next(true);
          });
        } else {
          this.firebaseAuth.userDetails = null;
        }
      }
    );
  }

  private async enrollDevice(): Promise<void> {
    // Obtain a firebase ID token and set its fields to be the username+password fields of basic auth
    const firebaseToken: string = await this.firebaseAuth.getIdToken();

    // Acquire JWT, configure the API to use it if the request was successful
    this.authApi.configuration.accessToken = btoa(firebaseToken);
    const jwt: JWT = await this.authApi.getToken().toPromise();
    if (!jwt) {
      throw new Error('Unable to sign in. Try again later');
    }

    this.authApi.configuration.accessToken = jwt.access_token;
    (this.persist ? this.storage : this.session).store(STORAGE_JWT, jwt);
  }

  async resetPassword(email: string): Promise<void> {
    return this.firebaseAuth.resetPassword(email);
  }

  private flowAndClearTokens() {
    (this.persist ? this.storage : this.session).clear(STORAGE_JWT);
  }

  private getJWT(): JWT {
    return (this.persist ? this.storage : this.session).retrieve(STORAGE_JWT);
  }

  private async shouldNegotiateNewAccessToken(): Promise<boolean> {
    const jwt: JWT = await this.getJWT();
    return (Date.now() - new Date(jwt.expires).getTime()) <= EXPIRY_THRESHOLD_IN_MILLIS;
  }

  private async refreshDeviceToken(): Promise<void> {
    // Assume we already have a refresh token saved; swap it into the API's config so it uses the refresh token
    let jwt: JWT = this.getJWT();
    this.authApi.configuration.accessToken = jwt.refresh_token;
    jwt = await this.authApi.getRefresh().toPromise();

    this.authApi.configuration.accessToken = jwt.access_token;
    (this.persist ? this.storage : this.session).store(STORAGE_JWT, jwt);
  }

  createAccount(email: string, password: string) {
    return this.firebaseAuth.createAccount(email, password);
  }

  signInWith(method: Method,
	// ERROR
             options?: { email?: string, password?: string, phone?: number, persist?: boolean }): Promise<firebase.auth.UserCredential> {
    if (options.persist !== undefined) {
      this.persist = options.persist;
    }
    switch (method) {
      case Method.Email:
        return this.firebaseAuth.signInRegular(options.email, options.password, this.persist);
      case Method.Facebook:
        return this.firebaseAuth.signInWithFacebook(this.persist);
      case Method.Google:
        return this.firebaseAuth.signInWithGoogle(this.persist);
      default:
        return this.firebaseAuth.signInRegular(options.email, options.password, this.persist);
    }
  }

  logout(): void {
    this.isLoggedInSubject.next(false);
    this.flowAndClearTokens();
    this.firebaseAuth.logout();
  }

  getUsername(): string {
    return this.firebaseAuth.getUsername();
  }

  getUserID(): string {
    return this.firebaseAuth.getUserID();
  }


  getEmail(): string {
    return this.firebaseAuth.userDetails ? this.firebaseAuth.userDetails.email : null;
  }

  getAvatar(): string {
    return this.firebaseAuth.userDetails ? this.firebaseAuth.userDetails.photoURL : null;
  }

  isLoggedIn(): boolean {
    return this.firebaseAuth.isLoggedIn();
  }
}

export enum Method {
  Email,
  Google,
  Facebook,
  PhoneNumber
}
