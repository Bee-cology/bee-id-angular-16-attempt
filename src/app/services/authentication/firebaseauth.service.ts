import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import { Observable } from 'rxjs';

export class FirebaseAuthService {
  user: Observable<firebase.User>;
  userDetails: firebase.User = null;

  constructor(private _firebaseAuth: AngularFireAuth, private router: Router) {
    this.user = _firebaseAuth.authState;
  }

  getUsername(): string {
    if (!this.userDetails) {
      return '';
    }
    return this.userDetails.displayName
    || this.userDetails.email.substr(0, this.userDetails.email.indexOf('@'));
  }

  getUserID(): string {
    if (!this.userDetails) {
      return '';
    }
    return this.userDetails.uid;
  }

  async getIdToken(): Promise<string> {
    if (!this.userDetails) {
      return Promise.reject(new Error('auth.service.ts: User hasn\'t login'));
    }
    return this.userDetails.getIdToken(false);
  }

  async signInRegular(email: string, password: string, persist: boolean): Promise<firebase.auth.UserCredential> {
    return this._firebaseAuth.setPersistence(persist ? firebase.auth.Auth.Persistence.LOCAL : firebase.auth.Auth.Persistence.SESSION)
      .then(v => this._firebaseAuth.signInWithEmailAndPassword(email, password));
  }

  async signInWithFacebook(persist: boolean): Promise<firebase.auth.UserCredential> {
    return this._firebaseAuth.setPersistence(persist ? firebase.auth.Auth.Persistence.LOCAL : firebase.auth.Auth.Persistence.SESSION)
      .then(v => this._firebaseAuth.signInWithPopup(
        new firebase.auth.FacebookAuthProvider()
      ));
  }

  async signInWithGoogle(persist: boolean): Promise<firebase.auth.UserCredential> {
    return this._firebaseAuth.setPersistence(persist ? firebase.auth.Auth.Persistence.LOCAL : firebase.auth.Auth.Persistence.SESSION)
      .then (v => this._firebaseAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()));
  }

  isLoggedIn(): boolean {
    return this.userDetails != null;
  }

  async resetPassword(email: string): Promise<void> {
    return this._firebaseAuth.sendPasswordResetEmail(email);
  }

  async logout(): Promise<void> {
    return this._firebaseAuth.signOut()
      .then(res => {
        this.userDetails = null;
        this.router.navigate(['/app']);
    });
  }

  createAccount(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this._firebaseAuth.createUserWithEmailAndPassword(email, password);
  }
}
