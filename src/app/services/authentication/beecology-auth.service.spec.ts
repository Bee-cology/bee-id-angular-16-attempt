import { TestBed, inject } from '@angular/core/testing';

import { BeecologyAuthService } from './beecology-auth.service';

describe('BeecologyAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeecologyAuthService]
    });
  });

  it('should be created', inject([BeecologyAuthService], (service: BeecologyAuthService) => {
    expect(service).toBeTruthy();
  }));
});
