import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';

@Injectable({
  providedIn: 'root'
})
export class SavedLocationsService {
  private locations: PouchDB.Database<any>;

  constructor() {
    this.locations = new PouchDB('savedLocations', {size: 5});
  }

  getLocations(): Promise<Array<PouchDB.Core.ExistingDocument<any>>> {
    return this.locations.allDocs({include_docs: true, descending: true}).then((doc) => {
      return doc.rows.map((row) => row.doc);
    });
  }

  // Deletes oldest saved location if location.length > 5
  deleteOldestLocation() {
    this.locations.allDocs({include_docs: true, descending: true}).then(res => {
      var logs = res.rows;
      if (logs.length < 5) return null;
      var log = logs[logs.length-1].doc;
      console.log('Oldest saved location removed.');
      return this.locations.remove(log);
    });
  }

  removeLocation(lat: number, lng: number): Promise<PouchDB.Core.Response> {
    return this.locations.allDocs({include_docs: true, descending: true}).then(res => {
      var logs = res.rows;
      for (let log of logs) {
        var doc = log.doc;
        if (lat == doc.lat && lng == doc.lng) return this.locations.remove(doc);
      }
    });
  }

  saveLocation(lat: number, lng: number): Promise<PouchDB.Core.Response> {
    this.deleteOldestLocation();
    const entry = {
      _id: new Date().toISOString(),
      'lat': lat,
      'lng': lng
    };
    return this.locations.put(entry);
  }

}

