import { Pipe, PipeTransform } from '@angular/core';
import { BeeSpecies, ButterflySpecies, FlowerSpecies } from '../api-module';
import { TitleCasePipe } from '@angular/common';

/**
 * Pipe to easily convert any bee species or butterfly species or flower species object into a single string in title case. Implemented as a pipe
 * because this function is used so incredibly often.
 */
@Pipe({
  name: 'speciesName'
})
export class SpeciesNamePipe implements PipeTransform {

  constructor(private titleCasePipe: TitleCasePipe) { }

  transform(value, ...args: any[]): string {
    // If it has no genus, it is most certainly a bumblebee. That's... just how biology works, right?
    // If it has no genus but has a family its butterfly right?
    if (value) {
      const str = value.genus === undefined && value.family === undefined ? `Bombus ${value.species}` : value.subfamily !== undefined ? `${value.species}` : `${(value as FlowerSpecies).genus} ${value.species}`;
      return str[0].toUpperCase() + str.slice(1).toLowerCase();
    } else {
      return '';
    }
  }
}
//
