/**
 * Class representing a local bee log.
 * Log entries are stored in the local PouchDB in this format.
 */
export class BeeLogRecord {
    chead: string;
    cabdomen: string;
    cthorax: string;
    gender: string;
    loc: string;
    cityname: string;
    fname: string;
    ffname: string;
    fcname: string;
    fshape: string;
    lifecycle: string;
    fcolor: string;
    beename: string;
    time: Date;
    beedictid: string;
    beebehavior: number;
    recordpicpath: string[];
    recordvidpath: string;
    recordpicindex: string[][];
    cartoon: string;
    hasLocation;
    appVersion: string;
    userComments: string;
    flowerSpeciesGuessArray?: string[];
    flowerSpeciesTopGuess?: string;

}
