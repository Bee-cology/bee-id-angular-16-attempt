/**
 * Class representing a local butterfly log.
 * Log entries are stored in the local PouchDB in this format.
 */
 export class ButterflyLogRecord {
  loc: string;
  cityname: string;
  fname: string;
  ffname: string;
  fcname: string;
  fshape: string;
  lifecycle: string;
  fcolor: string;
  butterflyname: string;
  time: Date;
  butterflyid: string;
  butterflybehavior: number;
  recordpicpath: string[];
  recordvidpath: string;
  recordpicindex: string[][];
  // cartoon: string;
  hasLocation;
  appVersion: string;
  userComments: string;
  wingSize?: string;
  wingShape?: string;
  antennae?: string;
  dorsalPattern?: string[];
  ventralPattern?: string[];
  dorsalPrimaryColor?: string[];
  dorsalSecondaryColor?: string[];
  ventralPrimaryColor?: string[];
  ventralSecondaryColor?: string[];
  tails?: string;
  speciesGuessArray?: string[];
  speciesTopGuess?: string;
  flowerSpeciesGuessArray?: string[];
  flowerSpeciesTopGuess?: string;
}

