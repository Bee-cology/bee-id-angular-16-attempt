import { ButterflyCharacteristic } from './butterfly-characteristic.model';

export class ButterflyLog {
    private _id: number;
    private _picturesPath: string[];
    private _isVideo: boolean;
    private _butterflyCharacteristic: ButterflyCharacteristic; // removed, may need to re create in the future
    private _date: Date;
    private _flowerName: string;
    private _flowerFamilyName: string;
    private _flowerCommonName: string;
    private _flowerShape: string;
    private _flowerLifeCycle: string;
    private _flowerColor: string;
    private _butterflyBehavior: number;
    private _time: number; // epoch time
    private _videoPath: string;
    private _city: string;
    private _location: string;
    private _gender: string;
    // private _cartoon: string;
    private _hasLocation: boolean;
    private _appVersion: string;

    private _userComments: string;
    private _wingSize: string;
    private _wingShape: string;
    private _antennae: string;
    private _dorsalPattern: string[];
    private _ventralPattern: string[];
    private _dorsalPrimaryColor: string[];
    private _dorsalSecondaryColor: string[];
    private _ventralPrimaryColor: string[];
    private _ventralSecondaryColor: string[];
    private _tailsLog: string;
    private _speciesGuessArrayLog: string[];
    private _speciesTopGuess: string;
    private _flowerSpeciesGuessArrayLog: string[];
    private _flowerSpeciesTopGuess: string;

    public get speciesTopGuess(): string {
      return this._speciesTopGuess;
    }
    public set speciesTopGuess(value: string) {
      this._speciesTopGuess = value;
    }

    public get flowerSpeciesTopGuess(): string {
      return this._flowerSpeciesTopGuess;
    }
    public set flowerSpeciesTopGuess(value: string) {
      this._flowerSpeciesTopGuess = value;
    }

    public get dorsalPattern(): string[] {
      return this._dorsalPattern;
    }

    public set dorsalPattern(value: string[]) {
      this._dorsalPattern = value;
    }

    public get ventralPattern(): string[] {
      return this._ventralPattern;
    }

    public set ventralPattern(value: string[]) {
      this._ventralPattern = value;
    }

    public get dorsalPrimaryColor(): string[] {
      return this._dorsalPrimaryColor;
    }

    public set dorsalPrimaryColor(value: string[]) {
      this._dorsalPrimaryColor = value;
    }

    public get dorsalSecondaryColor(): string[] {
      return this._dorsalSecondaryColor;
    }

    public set dorsalSecondaryColor(value: string[]) {
      this._dorsalSecondaryColor = value;
    }

    public get ventralPrimaryColor(): string[] {
      return this._ventralPrimaryColor;
    }

    public set ventralPrimaryColor(value: string[]) {
      this._ventralPrimaryColor = value;
    }

    public get ventralSecondaryColor(): string[] {
      return this._ventralSecondaryColor;
    }

    public set ventralSecondaryColor(value: string[]) {
      this._ventralSecondaryColor = value;
    }

    public get tails(): string {
      return this._tailsLog;
    }

    public set tails(value: string) {
      this._tailsLog = value;
    }

    public get speciesGuessArray(): string[] {
      return this._speciesGuessArrayLog;
    }

    public set speciesGuessArray(value: string[]) {
      this._speciesGuessArrayLog = value;
    }

    public get flowerSpeciesGuessArray(): string[] {
      return this._flowerSpeciesGuessArrayLog;
    }

    public set flowerSpeciesGuessArray(value: string[]) {
      this._flowerSpeciesGuessArrayLog = value;
    }



    public get wingSize(): string {
      return this._wingSize;
    }
    public set wingSize(value: string) {
      this._wingSize = value;
    }

    public get wingShape(): string {
      return this._wingShape;
    }
    public set wingShape(value: string) {
     this._wingShape = value;
    }

    public get antennae(): string {
      return this._antennae;
    }
    public set antennae(value: string) {
      this._antennae = value;
    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
    // public get cartoon(): string {
    //     return this._cartoon;
    // }
    // public set cartoon(value: string) {
    //     this._cartoon = value;
    // }
    public get picturesPath(): string[] {
        return this._picturesPath;
    }
    public set picturesPath(value: string[]) {
        this._picturesPath = value;
    }
    public get date(): Date {
        return this._date;
    }
    public set date(value: Date) {
        this._date = value;
    }
    public get isVideo(): boolean {
        return this._isVideo;
    }
    public set isVideo(value: boolean) {
        this._isVideo = value;
    }
    public get butterflyCharacteristic(): ButterflyCharacteristic {
        return this._butterflyCharacteristic;
    }
    public set butterflyCharacteristic(value: ButterflyCharacteristic) {
        this._butterflyCharacteristic = value;
    }
    public get flowerShape(): string {
        return this._flowerShape;
    }
    public set flowerShape(value: string) {
        this._flowerShape = value;
    }
    public get flowerColor(): string {
        return this._flowerColor;
    }
    public set flowerColor(value: string) {
        this._flowerColor = value;
    }
    public get flowerName(): string {
        return this._flowerName;
    }
    public set flowerName(value: string) {
        this._flowerName = value;
    }
    public get flowerFamilyName(): string {
        return this._flowerFamilyName;
    }
    public set flowerFamilyName(value: string) {
        this._flowerFamilyName = value;
    }
    public get flowerLifeCycle(): string {
        return this._flowerLifeCycle;
    }
    public set flowerLifeCycle(value: string) {
        this._flowerLifeCycle = value;
    }
    public get flowerCommonName(): string {
      return this._flowerCommonName;
    }
    public set flowerCommonName(value: string) {
    this._flowerCommonName = value;
    }
    public get butterflyBehavior(): number {
        return this._butterflyBehavior;
    }
    public set butterflyBehavior(value: number) {
        this._butterflyBehavior = value;
    }
    public get time(): number {
        return this._time;
    }
    public set time(value: number) {
        this._time = value;
    }
    public get videoPath(): string {
        return this._videoPath;
    }
    public set videoPath(value: string) {
        this._videoPath = value;
    }
    public get city(): string {
        return this._city;
    }
    public set city(value: string) {
        this._city = value;
    }
    public get location(): string {
        return this._location;
    }
    public set location(value: string) {
        this._location = value;
    }
   public get gender(): string {
        return this._gender;
    }
    public set gender(value: string) {
        this._gender = value;
    }
    public get hasLocation(): boolean {
        return this._hasLocation;
    }
    public set hasLocation(value: boolean) {
        this._hasLocation = value;
    }
    public get appVersion(): string {
      return this._appVersion;
    }
    public set appVersion(value: string) {
      this._appVersion = value;
    }

    public get userComments(): string {
      return this._userComments;
    }
    public set userComments(value: string) {
      this._userComments = value;
    }
}
