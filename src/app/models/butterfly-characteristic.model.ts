export enum Color {
    MOST_BLACK, HALF_BLACK, MOST_NOT_BLACK
}

export class ButterflyCharacteristic {// currently just for images and video storage
    private _butterflyName: string;
    private _butterflyFamily: string;
    private _imgSrc: string[];
    private _imgIndex: string[][];

    constructor(
        butterflyName: string = '',
        butterflyFamily: string = ''
    ) {
        this._butterflyName = butterflyName;
        this._butterflyFamily = butterflyFamily;
    }

    public get butterflyName(): string {
        return this._butterflyName;
    }

    public set butterflyName(value: string) {
        this._butterflyName = value;
    }
    public get butterflyFamily(): string {
      return this._butterflyFamily;
    }

    public set butterflyFamily(value: string) {
      this._butterflyFamily = value;
    }
    public get imgSrc(): string[] {
        return this._imgSrc;
    }

    public set imgSrc(value: string[]) {
        this._imgSrc = value;
    }

    public get imgIndex(): string[][] {
        return this._imgIndex;
    }

    public set imgIndex(value: string[][]) {
        this._imgIndex = value;
    }
}
