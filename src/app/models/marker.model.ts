/**
 * Class representing a map marker in a Google Maps view
 */
export class Marker {
    id: number;
    lat: number;
    lng: number;
    label: string;
    draggable: boolean;
    opacity: number;
    iconUrl: string;
    address: string;
    addressComponents: any;
}
