import { BeeCharacteristic } from './bee-characteristic.model';

export class BeeLog {
    private _id: number;
    private _picturesPath: string[];
    private _isVideo: boolean;
    private _beeCharacteristic: BeeCharacteristic;
    private _date: Date;
    private _flowerName: string;
    private _flowerFamilyName: string;
    private _flowerCommonName: string;
    private _flowerShape: string;
    private _flowerLifeCycle: string;
    private _flowerColor: string;
    private _beeBehavior: number;
    private _time: number; // epoch time
    private _videoPath: string;
    private _city: string;
    private _location: string;
    private _gender: string;
    private _cartoon: string;
    private _hasLocation: boolean;
    private _appVersion: string;

    private _userComments: string;


    private _flowerSpeciesGuessArrayLog: string[];
    private _flowerSpeciesTopGuess: string;

    public get flowerSpeciesGuessArray(): string[] {
      return this._flowerSpeciesGuessArrayLog;
    }

    public set flowerSpeciesGuessArray(value: string[]) {
      this._flowerSpeciesGuessArrayLog = value;
    }

    public get flowerSpeciesTopGuess(): string {
      return this._flowerSpeciesTopGuess;
    }
    public set flowerSpeciesTopGuess(value: string) {
      this._flowerSpeciesTopGuess = value;
    }
    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
    public get cartoon(): string {
        return this._cartoon;
    }
    public set cartoon(value: string) {
        this._cartoon = value;
    }
    public get picturesPath(): string[] {
        return this._picturesPath;
    }
    public set picturesPath(value: string[]) {
        this._picturesPath = value;
    }
    public get date(): Date {
        return this._date;
    }
    public set date(value: Date) {
        this._date = value;
    }
    public get isVideo(): boolean {
        return this._isVideo;
    }
    public set isVideo(value: boolean) {
        this._isVideo = value;
    }
    public get beeCharacteristic(): BeeCharacteristic {
        return this._beeCharacteristic;
    }
    public set beeCharacteristic(value: BeeCharacteristic) {
        this._beeCharacteristic = value;
    }
    public get flowerShape(): string {
        return this._flowerShape;
    }
    public set flowerShape(value: string) {
        this._flowerShape = value;
    }
    public get flowerColor(): string {
        return this._flowerColor;
    }
    public set flowerColor(value: string) {
        this._flowerColor = value;
    }
    public get flowerName(): string {
        return this._flowerName;
    }
    public set flowerName(value: string) {
        this._flowerName = value;
    }
    public get flowerFamilyName(): string {
        return this._flowerFamilyName;
    }
    public set flowerFamilyName(value: string) {
        this._flowerFamilyName = value;
    }
    public get flowerLifeCycle(): string {
        return this._flowerLifeCycle;
    }
    public set flowerLifeCycle(value: string) {
        this._flowerLifeCycle = value;
    }
    public get flowerCommonName(): string {
      return this._flowerCommonName;
    }
    public set flowerCommonName(value: string) {
    this._flowerCommonName = value;
    }
    public get beeBehavior(): number {
        return this._beeBehavior;
    }
    public set beeBehavior(value: number) {
        this._beeBehavior = value;
    }
    public get time(): number {
        return this._time;
    }
    public set time(value: number) {
        this._time = value;
    }
    public get videoPath(): string {
        return this._videoPath;
    }
    public set videoPath(value: string) {
        this._videoPath = value;
    }
    public get city(): string {
        return this._city;
    }
    public set city(value: string) {
        this._city = value;
    }
    public get location(): string {
        return this._location;
    }
    public set location(value: string) {
        this._location = value;
    }
    public get gender(): string {
        return this._gender;
    }
    public set gender(value: string) {
        this._gender = value;
    }
    public get hasLocation(): boolean {
        return this._hasLocation;
    }
    public set hasLocation(value: boolean) {
        this._hasLocation = value;
    }
    public get appVersion(): string {
      return this._appVersion;
    }
    public set appVersion(value: string) {
      this._appVersion = value;
    }

    public get userComments(): string {
      return this._userComments;
    }
    public set userComments(value: string) {
      this._userComments = value;
    }
}
