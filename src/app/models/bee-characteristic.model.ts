export enum Color {
    MOST_BLACK, HALF_BLACK, MOST_NOT_BLACK
}

export class BeeCharacteristic {
    private _head: string;
    private _thorus: string;
    private _abdomen: string;
    private _color: Color;
    private _beeName: string;
    private _imgSrc: string[];
    private _imgIndex: string[][];

    COLOR = ['abdomen_1', 'abdomen_2', 'abdomen_3'];
    HEADER = ['h2', 'h3'];
    THORUS_MOST_BLACK = ['t1', 't2'];
    THORUS_HALF_BLACK = ['t1', 't3', 't4', 't5'];
    THORUS_MOST_NOT_BLACK = ['t5', 't6', 't7', 't8', 't9'];
    ABDOMEN_MOST_BLACK = ['a1', 'a2', 'a3'];
    ABDOMEN_HALF_BLACK = ['a5', 'a4', 'a6', 'a7'];
    ABDOMEN_MOST_NOT_BLACK = ['a8', 'a9', 'a10', 'a11', 'a13'];

    constructor(
        color: Color = 0,
        header: string = '',
        thorus: string = '',
        abdomen: string = '',
        beeName: string = ''
    ) {
        this._color = color;
        this._head = header;
        this._thorus = thorus;
        this._abdomen = abdomen;
        this._beeName = beeName;
    }

    public get color(): Color {
        return this._color;
    }

    public set color(value: Color) {
        this._color = value;
    }

    public get head(): string {
        return this._head;
    }

    public set head(value: string) {
        this._head = value;
    }

    public get thorus(): string {
        return this._thorus;
    }

    public set thorus(value: string) {
        this._thorus = value;
    }

    public get abdomen(): string {
        return this._abdomen;
    }

    public set abdomen(value: string) {
        this._abdomen = value;
    }

    public get beeName(): string {
        return this._beeName;
    }

    public set beeName(value: string) {
        this._beeName = value;
    }

    public get imgSrc(): string[] {
        return this._imgSrc;
    }

    public set imgSrc(value: string[]) {
        this._imgSrc = value;
    }

    public get imgIndex(): string[][] {
        return this._imgIndex;
    }

    public set imgIndex(value: string[][]) {
        this._imgIndex = value;
    }
}
