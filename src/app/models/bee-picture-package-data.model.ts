import { BeePicturePackage } from './bee-picture-package.model';

export const BeePicturePackageData = {
  impatiens_male: new BeePicturePackage('Bombus impatiens', 'helper_3', 'impatiens', 'impatiens', 'male', '4c1f54d6-ce43-4337-8d61-e840c38b1da1', 'impatiens_male'),
  impatiens_female: new BeePicturePackage('Bombus impatiens', 'helper_3_2', 'impatiens', 'impatiens', 'female', '4c1f54d6-ce43-4337-8d61-e840c38b1da1', 'impatiens_female'),
  griseocollis_male: new BeePicturePackage(
    'Bombus griseocollis', 'helper_1_2', 'griseocollis', 'griseocollis', 'male', '6e03096f-166f-49ee-8c3e-6e451d6db4a3', 'griseocollis_male'
  ),
  griseocollis_female: new BeePicturePackage(
    'Bombus griseocollis', 'helper_1', 'griseocollis', 'griseocollis', 'female', '6e03096f-166f-49ee-8c3e-6e451d6db4a3', 'griseocollis_female'
  ),
  bimaculatus_female: new BeePicturePackage(
    'Bombus bimaculatus', 'helper_2', 'bimaculatus', 'bimaculatus', 'female', 'afeb20ea-4426-447c-a6cf-3cf49b6b55b3', 'bimaculatus_female'
  ),
  bimaculatus_male_1: new BeePicturePackage(
    'Bombus bimaculatus', 'helper_2_2', 'bimaculatus', 'bimaculatus', 'male', 'afeb20ea-4426-447c-a6cf-3cf49b6b55b3', 'bimaculatus_male_1'
  ),
  bimaculatus_male_2: new BeePicturePackage(
    'Bombus bimaculatus', 'helper_2_3', 'bimaculatus_male2', 'bimaculatus_male2', 'male', 'afeb20ea-4426-447c-a6cf-3cf49b6b55b3', 'bimaculatus_male_2'
  ),
  affinis: new BeePicturePackage('Bombus affinis', 'helper_11', 'affinis', 'affinis', null, '6aa7cef1-858e-43ab-9e4d-54fae61c279d', 'affinis'),
  terricola_male: new BeePicturePackage('Bombus terricola', 'helper_10_2', 'terricola', 'terricola', 'male', '152be09f-445a-4dc0-9223-42db678cb42b', 'terricola_male'),
  terricola_female: new BeePicturePackage('Bombus terricola', 'helper_10', 'terricola', 'terricola', 'female', '152be09f-445a-4dc0-9223-42db678cb42b', 'terricola_female'),
  perplexus_female_1: new BeePicturePackage('Bombus perplexus', 'helper_7', 'perplexus', 'perplexus', 'female', '92c66708-ada8-4fcb-b507-c0a89a9f54f3', 'perplexus_female_1'),
  perplexus_male_1: new BeePicturePackage('Bombus perplexus', 'helper_7_2', 'perplexus', 'perplexus', 'male', '92c66708-ada8-4fcb-b507-c0a89a9f54f3', 'perplexus_male_1'),
  perplexus_female_2: new BeePicturePackage('Bombus perplexus', 'helper_9', 'perplexus', 'perplexus', 'female', '92c66708-ada8-4fcb-b507-c0a89a9f54f3', 'perplexus_female_2'),
  perplexus_male_2: new BeePicturePackage('Bombus perplexus', 'helper_9_2', 'perplexus', 'perplexus', 'male', '92c66708-ada8-4fcb-b507-c0a89a9f54f3', 'perplexus_male_2'),
  perplexus_male_3: new BeePicturePackage(
    'Bombus perplexus', 'helper_9_3', 'perplexus_male_3', 'perplexus_male_3', 'male', '92c66708-ada8-4fcb-b507-c0a89a9f54f3', 'perplexus_male_3'
  ),
  vagans_female: new BeePicturePackage('Bombus vagans', 'helper_6', 'vagans', 'vagans', 'female', 'ab68260c-f40d-424a-8ac9-13210feec38f', 'vagans_female'),
  vagans_male: new BeePicturePackage('Bombus vagans', 'helper_6_2', 'vagans', 'vagans', 'male', 'ab68260c-f40d-424a-8ac9-13210feec38f', 'vagans_male'),
  pensylvanicus_female: new BeePicturePackage(
    'Bombus pensylvanicus', 'helper_8', 'pensylvanicus', 'pensylvanicus', 'female', '32101a07-4655-486c-bfc5-e1d612beef45', 'pensylvanicus_female'
  ),
  pensylvanicus_male: new BeePicturePackage(
    'Bombus pensylvanicus', 'helper_8_2', 'pensylvanicus_male', 'pensylvanicus_male', 'male', '32101a07-4655-486c-bfc5-e1d612beef45', 'pensylvanicus_male'
  ),
  ternarius: new BeePicturePackage('Bombus ternarius', 'helper_4', 'ternarius', 'ternarius', null, '27c21859-ede7-435e-b582-488c4e58b2ae', 'ternarius'),
  fervidus: new BeePicturePackage('Bombus fervidus', 'helper_5', 'fervidus', 'fervidus', null, '48393177-8044-4c0f-aa43-8aff867ef041', 'fervidus'),
  borealis: new BeePicturePackage('Bombus borealis', 'helper_12', 'borealis', 'borealis', null, 'd4f72810-b4a8-4e6c-86e3-7f225488b861', 'borealis'),
};
