import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-snack-bar',
  template: `
    <div class="div-snackbar" fxLayout fxLayoutAlign="start center">
      <div class="div-text">{{ data.message }}{{ messages[data['messageKey']] }}</div>
      <mat-icon *ngIf="data.icon == 'error'">error_outline</mat-icon>
      <mat-icon *ngIf="data.icon == 'warning'">warning</mat-icon>
      <mat-icon *ngIf="data.icon == 'success'">check_circle</mat-icon>
      <mat-icon *ngIf="data.icon == 'info'">info</mat-icon>
    </div>
  `,
  styleUrls: ['./app-snackbars.scss']
})
export class SnackBarInputBasic {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) { }
  messages = {
    'info-log-location-saved': 'Log location saved for future use.', // app-home-summary, app-logs
    'info-select-logs-deletion': 'Select the logs you would like to delete.', // app-logs
    'info-select-logs-submission': 'Select the logs you would like to submit.', // app-logs
    'success-log-saved': 'Log successfully saved to your device.', // app-home-summary
    'success-log-location-update': 'Log location successfully updated.', // app-logs
    'success-log-flower-update': 'Log flower successfully updated.', // app-logs
    'success-log-submission': ' logs successfully submitted to the Beecology Database. Thank you for your contribution!', // app-logs
    'success-log-deletion': ' logs successfully delete from your device.', // app-logs
    'success-password-reset': 'A password reset email has been sent to you.', // app-resetpassword
    'success-account-creation': 'Account successfully created!', // create-account
    'success-database-compact': 'Old deleted logs successfully purged. You shouldn\'t have to do this again.', // login-page
    'error-field-input': 'Fill out all available fields.', // app-home-flower-id-expert
    'error-abdomen-base-color': 'Select an abdomen color!', // app-home-bee-id-guided
    'error-flower-name': 'Choose a flower from our list.', // app-home-flower-id-expert
    'error-flower-frame': 'Capture a video frame of the flower.', // app-home-flower-id-expert
    'error-butterfly-frame': 'Capture a video frame of the butterfly.', // app-home-butterfly-if-manual
    'capture-image-for-fxn': 'Capture an image for added page functionality.', // app-home-butterfly-id-manual
    'capture-image-for-iNat': 'Capture an image to use for iNaturalist suggestion.', // app-home-butterfly-id-manual
    'out-of-iNat-queries': 'You have reached the maximum number of iNaturalist queries for this submission.', // app-home-butterfly-id-manual
    'capture-new-image': 'Capture a new image before reusing iNaturalist. Careful, you only have 1 attempt left.', // app-home-butterfly-id-manual
    'no-species-returned': 'Sorry, iNaturalist didn\'t return any species present in our database. Either try using a new capture or select unknown species.', // app-home-butterfly-id-manual
    'not-connected-to-internet': 'Sorry, please connect to internet to utilize iNaturalist identification.', // app-home-butterfly-id
    'error-log-submission': 'An error occurred during log submission.', // app-logs
    'error-log-deletion': 'An error occurred during log deletion.', // app-logs
    'error-log-location-update': 'An error occurred updating the log location.', // app-logs
    'error-log-flower-update': 'An error occurred updating the log flower.', // app-logs
    'error-log-location-missing-submission': 'This log is missing the required location field. Please scroll down to add a location.', // app-logs
    'error-log-flower-missing-submission': 'This log is missing the required flower field. Please scroll down to identify the flower.', // app-logs
    'error-log-location-missing-confirmation': 'Confirm the location your bee was spotted.', // app-logs
    'error-account-field-input': 'Fill out all credential fields.', // create-account, login-page
    'error-account-email-invalid': 'Enter a valid email address.', // create-account, login-page
    'error-account-email-taken': 'Email address already in use by another account.', // create-account
    'error-account-email-unknown': 'Email does not match any user on record.', // login-page
    'error-account-password-weak': 'Password must be at least 6 characters long.', // create-account
    'error-account-password-incorrect': 'Password does not match our account records.', // login-page
    'error-account-too-many-attempts': 'Too many failed login attempts on the same account. Please try again later.', // login-page
    'error-database-compact': 'Failed to purge old deleted logs. Please try again.' // login-page
  };
}
