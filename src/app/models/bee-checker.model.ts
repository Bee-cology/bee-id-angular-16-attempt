import { BeeCharacteristic, Color } from './bee-characteristic.model';

export const beeCharacteristics = [
  new BeeCharacteristic(Color.MOST_BLACK, 'h3', 't2', 'a1', 'impatiens_male'),
  new BeeCharacteristic(Color.MOST_BLACK, 'h2', 't2', 'a1', 'impatiens_female'),
  new BeeCharacteristic(Color.MOST_BLACK, 'h3', 't1', 'a2', 'griseocollis_male'),
  new BeeCharacteristic(Color.MOST_BLACK, 'h2', 't1', 'a2', 'griseocollis_female'),
  new BeeCharacteristic(Color.MOST_BLACK, 'h2', 't1', 'a3', 'bimaculatus_female'),
  new BeeCharacteristic(Color.MOST_BLACK, 'h3', 't1', 'a3', 'bimaculatus_male_1'),
  new BeeCharacteristic(Color.HALF_BLACK, 'h2', 't4', 'a4', 'affinis'),
  new BeeCharacteristic(Color.HALF_BLACK, 'h2', 't3', 'a6', 'terricola_female'),
  new BeeCharacteristic(Color.HALF_BLACK, 'h3', 't3', 'a6', 'terricola_male'),
  new BeeCharacteristic(Color.HALF_BLACK, 'h2', 't5', 'a5', 'perplexus_female_1'),
  new BeeCharacteristic(Color.HALF_BLACK, 'h3', 't5', 'a5', 'perplexus_male_1'),
  new BeeCharacteristic(Color.HALF_BLACK, 'h2', 't1', 'a5', 'vagans_female'),
  new BeeCharacteristic(Color.HALF_BLACK, 'h3', 't1', 'a5', 'vagans_male'),
  new BeeCharacteristic(Color.HALF_BLACK, 'h2', 't3', 'a7', 'pensylvanicus_female'),
  new BeeCharacteristic(Color.MOST_NOT_BLACK, 'h3', 't7', 'a9', 'ternarius'),
  new BeeCharacteristic(Color.MOST_NOT_BLACK, 'h2', 't5', 'a10', 'perplexus_female_2'),
  new BeeCharacteristic(Color.MOST_NOT_BLACK, 'h3', 't5', 'a10', 'perplexus_male_2'),
  new BeeCharacteristic(Color.MOST_NOT_BLACK, 'h2', 't6', 'a8', 'fervidus'),
  new BeeCharacteristic(Color.MOST_NOT_BLACK, 'h3', 't8', 'a11', 'borealis'),
  new BeeCharacteristic(Color.MOST_NOT_BLACK, 'h3', 't9', 'a13', 'bimaculatus_male_2'),
  new BeeCharacteristic(Color.MOST_NOT_BLACK, 'h3', 't9', 'a8', 'perplexus_male_3'),
  new BeeCharacteristic(Color.MOST_NOT_BLACK, 'h2', 't8', 'a8', 'pensylvanicus_male')
];
