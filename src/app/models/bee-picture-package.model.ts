export class BeePicturePackage {
    name: string;
    cartoon: string;
    field: string;
    icon: string;
    gender: string;
    packageName: string;
    beeID: string;

    constructor(
        name: string,
        cartoon: string,
        field: string,
        icon: string,
        gender: string,
        beeID: string,
        packageName: string
    ) {
        this.name = name;
        this.cartoon = cartoon;
        this.field = field;
        this.icon = icon;
        this.gender = gender;
        this.packageName = packageName;
        this.beeID = beeID;
    }
}
