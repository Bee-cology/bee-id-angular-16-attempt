# Beecology API Client 2.1.0

This client is an auto-generated client (and associated generation script) in Typescript (Angular) for the Beecology
API. Don't modify the code, any changes you make will be overwritten the next time the generator is run.

## Installation & Configuration

To add the API client to your project, run `git submodule add https://arcgit.wpi.edu/Bee-cology/Beecology-API-Angular-Client.git path/of/your/choosing`
This will clone the client repo into the path of your choosing and configure your repository to track the version that
the cloned repo is on. This does not add the client's files to your repository (it leaves only a marker that the submodule
should be there) but does allow you to pull future updates from GitLab.

To change the API base path you need to pass in a configuration to the API module when importing it in your project's
root module. For instance:

```angular2
@NgModule({
  declarations: [
    // ...
  ],
  imports: [
    ApiModule.forRoot(() => new Configuration({basePath: '/api'})),
  ],
  providers: [
    // ..
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
```

The above will configure the API client to use `/api` as the base path. It is **strongly recommended** that you use your
project's `environment.ts` file to specify the base path.

## Versioning

This project tracks the version of the parent Beecology API project, following
[semantic versioning conventions](semver.org):

* If your patch version differs from the server's, you may assume that only bugfixes have been applied and that an update
is not necessary.
* If your minor version differs from the server's, you may assume that the server has new features that your client version
does not yet have.
* If your major version differs from the server's, you may assume that a breaking change was made and your client is
incompatible with the server. Furthermore it is logical that upgrading will not be a drop-in-place operation.

## Issue tracking

Please direct issues regarding the API client **specifically** to this project's issue tracker. Issues concerning the
API/server itself should be directed towards that project's issue tracker.
