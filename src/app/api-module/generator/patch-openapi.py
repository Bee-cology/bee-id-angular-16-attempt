#!/usr/bin/env python3
import argparse
import json
import sys
from typing import Dict

parser = argparse.ArgumentParser()
parser.add_argument("file", type=str, help="Open API file (converted from Swagger) to patch")
args = parser.parse_args()

spec: Dict
try:
	with open(args.file, "r") as file:
		spec = json.load(file)

	# Patch security schemes to use bearer tokens, which are not available in Swagger 2.0
	security_schemes = {}
	for name, scheme in spec["components"]["securitySchemes"].items():
		security_schemes[name] = {
			"type": "http",
			"scheme": "bearer",
			"bearerFormat": "JWT"
		}
	spec["components"]["securitySchemes"] = security_schemes

	# Fix CSV endpoint return type
	spec["paths"]["/csv"]["get"]["responses"]["200"] = {
		"description": "CSV enclosed",
		"content": {
			"text/csv": {
				"schema": {
					"type": "string"
				}
			}
		}
	}

	with open(args.file, "w") as file:
		json.dump(spec, file, indent=4)
except IOError:
	print("Failed to open {}, check permissions or file name".format(args.file), file=sys.stderr)
	exit(1)


