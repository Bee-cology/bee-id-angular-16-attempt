#!/usr/bin/env bash

# Ensure dependencies for script are installed
if ! [[ -x "$(command -v swagger2openapi)" ]]; then
    echo Missing swagger2openapi, install it before proceeding 1>&2
    exit 1
fi
if ! [[ -x "$(command -v openapi-generator)" ]]; then
    echo Missing openapi-generator, install it before proceeding 1>&2
    exit 1
fi

if [[ -z "$1" ]]; then
    echo Input Swagger JSON file not provided, downloading from dev server...
    rm -f swagger.json
    if ! wget http://localhost:5000/swagger.json; then
        echo Failed to download swagger.json, make sure the Flask dev server is running at localhost:5000 1>&2
        exit 2
    fi
    file="swagger.json"
else
    file=$1
fi

echo Converting $file to Open API 3.0 format and applying patches...
swagger2openapi -p $file > openapi3.json
./patch-openapi.py openapi3.json
openapi-generator generate -i openapi3.json -g typescript-angular -o .. -p modelPropertyNaming=original
