import { FlowerSpecies } from './flowerSpecies';
import { GISCoordinate } from './gISCoordinate';
import { ButterflySpecies } from './butterflySpecies';
import { MediaInformation } from './mediaInformation';



export interface ButterflyRecord {
  /**
   * UUID (v4) of the butterfly record
   */
  id?: string;
  /**
   * If known, the species ID of the observed butterfly
   */
  butterfly_species_id?: string;
  /**
   * Butterfly species details
   */
  butterfly_species?: ButterflySpecies; // Added for coverage... though may need?
  /**
   * If known, the species ID of the observed flower
   */
  flower_species_id?: string;
  /**
   * Flower species details
   */
  flower_species?: FlowerSpecies;
  /**
   * media path for video associated with this butterfly record
   */

  media_id?: string;
  media?: Array<string>; // added for extra coverage
  /**
   * Butterfly Dorsal Primary Color
   */
  dorsalPrimaryColor?: Array<string>;
  /**
   * Butterfly Dorsal Secondary Coloration
   */
  dorsalSecondaryColor?: Array<string>;
  /**
   * Butterfly Ventral Primary Color
   */
  ventralPrimaryColor?: Array<string>;
  /**
   * Butterfly Ventral Secondary Coloration
   */
  ventralSecondaryColor?: Array<string>;
  /**
   * Butterfly Dorsal Pattern
   */
  dorsalPattern?: Array<string>;
  /**
   * Butterfly Ventral Pattern
   */
  ventralPattern?: Array<string>;
  /**
   * Butterfly gender
   */
  gender?: ButterflyRecord.GenderEnum;
  /**
   * The butterfly\'s behavior
   */
  butterfly_behavior?: ButterflyRecord.BehaviorEnum;
  /**
   * Butterfly's antennae shape
   */
  antennae?: ButterflyRecord.AntennaeEnum;
  /**
   * Butterfly's wing shape
   */
  wing_shape?: ButterflyRecord.WingShapeEnum;
  /**
   * Butterfly's tail present
   */
  tails?: ButterflyRecord.TailsEnum;
  /**
   * Butterfly's size
   */
  wing_size?: ButterflyRecord.SizeEnum;
  /**
   * Time the butterfly record was logged in ISO 8601 format
   */
  time: string;
  submitted?: string;
  /**
   * Point of observation
   */
  location: GISCoordinate;
  /**
   * Elevation at the point the butterfly was observed at
   */
  elevation?: number;
  /**
   * Where the butterfly was observed
   */
  closest_city?: string;
  /**
   * How the record was submitted
   */
  how_submitted: ButterflyRecord.HowSubmittedEnum;
  /**
   *  Whether flower is native to the observed location
   */
  // flower_native_classification?: string;
  user_comments?: string;
  /**
   *  user comments on this record
   */
  species_top_guess?: string;
  species_guess_array?: Array<string>;
  flower_species_guess_array?: Array<string>;

}
export namespace ButterflyRecord {
  export type GenderEnum = 'male' | 'female';
  export const GenderEnum = {
    Male: 'male' as GenderEnum,
    Female: 'female' as GenderEnum,
  };

  export type BehaviorEnum =  'nectaring' | 'puddling' | 'patrolling' | 'perching' | 'ovipositing' | 'mating';
  export const BehaviorEnum = {
    Nectaring: 'nectaring' as BehaviorEnum,
    Puddling: 'puddling' as BehaviorEnum,
    Patrolling: 'patrolling' as BehaviorEnum,
    Perching: 'perching' as BehaviorEnum,
    Ovipositing: 'ovipositing' as BehaviorEnum,
    Mating: 'mating' as BehaviorEnum
  };
  export type AntennaeEnum = 'hooked' | 'clubbed' | 'combed';
  export const AntennaeEnum = {
    Hooked: 'hooked' as AntennaeEnum,
    Clubbed: 'clubbed' as AntennaeEnum,
    Combed: 'combed' as AntennaeEnum
  };
  export type SizeEnum = 'Small' | 'Medium' | 'Large';
  export const SizeEnum = {
    Small: 'Small' as SizeEnum,
    Medium: 'Medium' as SizeEnum,
    Large: 'Large' as SizeEnum
  };
  export type WingShapeEnum = 'Common' | 'Unique' | 'Skipper' | 'Hairstreak' | 'Longtail';
  export const WingShapeEnum = {
    Common: 'Common' as WingShapeEnum,
    Unique: 'Unique' as WingShapeEnum,
    Skipper: 'Skipper' as WingShapeEnum,
    Hairstreak: 'Hairstreak' as WingShapeEnum,
    Longtail: 'Longtail' as WingShapeEnum
  };
  export type TailsEnum = 'Yes' | 'No';
  export const TailsEnum = {
    Yes: 'Yes' as TailsEnum,
    No: 'No' as TailsEnum
  };
  export type HowSubmittedEnum = 'webapp' | 'androidapp' | 'museum' | 'expert';
  export const HowSubmittedEnum = {
    Webapp: 'webapp' as HowSubmittedEnum,
    Androidapp: 'androidapp' as HowSubmittedEnum,
    Museum: 'museum' as HowSubmittedEnum,
    Expert: 'expert' as HowSubmittedEnum
  };
}
