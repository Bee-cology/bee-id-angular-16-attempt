<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="colorPrimary">#FFC107</color>
    <color name="colorPrimaryDark">#FFA000</color>
    <color name="colorPrimaryLight">#FFECB3</color>
    <color name="colorAccent">#8e24aa</color>
    <color name="colorAccentLight">#c158dc</color>
    <color name="colorPrimaryText">#212121</color>
    <color name="colorSecondaryText">#757575</color>
    <color name="colorIcons">#212121</color>
    <color name="colorDivider">#BDBDBD</color>
    <color name="colorWhite">#ffffff</color>
    <color name="colorBackdrop">#fffafafa</color>
</resources>

