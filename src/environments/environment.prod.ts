export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyDXvJq6tJ0595dQVly486DICgeEvV0tYiw',
    authDomain: 'beecology-f7460.firebaseapp.com',
    databaseURL: 'https://beecology-f7460.firebaseio.com',
    projectId: 'beecology-f7460',
    storageBucket: 'beecology-f7460.appspot.com',
    messagingSenderId: '834616427421'
  },
  virtualEarth: {
    baseURL: 'https://dev.virtualearth.net/REST/v1/Elevation/List',
    key: 'Ath0FVMO9-ASJFLh6lErHaCgtmGTTgf87zjeit7lNjWyfKmz--hZh_GWUG5U8iQd'
  },
  googleMaps: {
    baseURL: 'https://maps.googleapis.com/maps/api/geocode/json',
    'key': 'AIzaSyDPdeu0S4CWpem-3KrcgVNuAQ8TJHQtZDs'
  },
  currentAppVersion: '2.5.3',
  basePath: '/api',
  images: '/images'
};
