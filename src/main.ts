import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import 'hammerjs';


if (environment.production) {
  enableProdMode();
}

var cookieEnabled = (navigator.cookieEnabled) ? true : false;

if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled)
{
  document.cookie="testcookie";
  cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
}

if (!cookieEnabled) {
  window.location.replace("http://beecology.wpi.edu/website_dev/enable-cookies-javascript");
}

platformBrowserDynamic().bootstrapModule(AppModule).then(() => {
  // r.e. sean-nicholas' suggestion in https://github.com/angular/angular/issues/20970
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.getRegistration()
      .then(active => !active && navigator.serviceWorker.register('/webapp/ngsw-worker.js'))
      .catch(console.error);
  }
}).catch(err => console.log(err));
