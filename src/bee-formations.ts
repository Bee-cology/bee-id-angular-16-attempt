export const BEE_FORMATIONS = [
  {
    id: 0,
    abdomen: 'Mostly-Black',
    imgUrl: 'assets/offline-assets/bee-cartoons/abdomen/abdomen_1.png',
    abdomenPattern: [
      {
        id: 0,
        imgUrl: 'assets/offline-assets/bee-cartoons/abdomen/a1.png',
        throax: [
          {
            id: 0,
            imgUrl: 'assets/offline-assets/bee-cartoons/thorax/t2.png',
            head: [
              {
                id: 0,
                imgUrl: 'assets/offline-assets/bee-cartoons/head/h2.png',
              },
              {
                id: 1,
                imgUrl: 'assets/offline-assets/bee-cartoons/head/h3.png',
              }
            ]
          }
        ]
      },
      {
        id: 1,
        imgUrl: 'assets/offline-assets/bee-cartoons/abdomen/a2.png',
        throax: [
          {
            imgUrl: 'assets/offline-assets/bee-cartoons/thorax/t1.png'
          },
          {
            imgUrl: 'assets/offline-assets/bee-cartoons/thorax/t2.png'
          }
        ]
      },
      {
        id: 2,
        imgUrl: 'assets/offline-assets/bee-cartoons/abdomen/a3.png',
        throax: [
          {
            imgUrl: 'assets/offline-assets/bee-cartoons/thorax/t1.png'
          },
          {
            imgUrl: 'assets/offline-assets/bee-cartoons/thorax/t2.png'
          }
        ]
      },
    ]
  }
];
